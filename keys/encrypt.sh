#!/bin/bash

cd $(dirname "$BASH_SOURCE")

shopt -s nullglob
if (($#)); then
  files=( "$@" )
else
  files=( *.h *.xml *.jks *.json )
fi
set -- "${files[@]}"

if (( !$# )); then
  echo >&2 no files
  exit 1
fi
set --
devs=( $(sed 's,#.*,,'  devs/list) "$@" )

for file in ${files[@]}; do
  file="${file%.asc}"

  if test -e $file.asc ; then
    if test -e $file.old.asc; then
      rm -f $file.asc
    else
      mv -f $file.asc $file.old.asc
    fi
  fi

  set -- $(printf ' -r %s ' "${devs[@]}" ) 
  set -- gpg -sea  "$@" --output ${file}.asc ${file} 
  printf '%s\n' "$@"
  if "$@"; then
      shred $file
      rm -f $file
  fi
done
