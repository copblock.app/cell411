--
-- PostgreSQL database dump
--

-- Dumped from database version 14.8 (Ubuntu 14.8-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.8 (Ubuntu 14.8-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE empty;
--
-- Name: empty; Type: DATABASE; Schema: -; Owner: nn
--

CREATE DATABASE empty WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';


ALTER DATABASE empty OWNER TO nn;

\connect empty

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: array_add(jsonb, jsonb); Type: FUNCTION; Schema: public; Owner: nn
--

CREATE FUNCTION public.array_add("array" jsonb, "values" jsonb) RETURNS jsonb
    LANGUAGE sql IMMUTABLE STRICT
    AS $$ SELECT array_to_json(ARRAY(SELECT unnest(ARRAY(SELECT DISTINCT jsonb_array_elements("array")) || ARRAY(SELECT jsonb_array_elements("values")))))::jsonb; $$;


ALTER FUNCTION public.array_add("array" jsonb, "values" jsonb) OWNER TO nn;

--
-- Name: array_add_unique(jsonb, jsonb); Type: FUNCTION; Schema: public; Owner: nn
--

CREATE FUNCTION public.array_add_unique("array" jsonb, "values" jsonb) RETURNS jsonb
    LANGUAGE sql IMMUTABLE STRICT
    AS $$ SELECT array_to_json(ARRAY(SELECT DISTINCT unnest(ARRAY(SELECT DISTINCT jsonb_array_elements("array")) || ARRAY(SELECT DISTINCT jsonb_array_elements("values")))))::jsonb; $$;


ALTER FUNCTION public.array_add_unique("array" jsonb, "values" jsonb) OWNER TO nn;

--
-- Name: array_contains(jsonb, jsonb); Type: FUNCTION; Schema: public; Owner: nn
--

CREATE FUNCTION public.array_contains("array" jsonb, "values" jsonb) RETURNS boolean
    LANGUAGE sql IMMUTABLE STRICT
    AS $$ SELECT RES.CNT >= 1 FROM (SELECT COUNT(*) as CNT FROM jsonb_array_elements("array") as elt WHERE elt IN (SELECT jsonb_array_elements("values"))) as RES; $$;


ALTER FUNCTION public.array_contains("array" jsonb, "values" jsonb) OWNER TO nn;

--
-- Name: array_contains_all(jsonb, jsonb); Type: FUNCTION; Schema: public; Owner: nn
--

CREATE FUNCTION public.array_contains_all("array" jsonb, "values" jsonb) RETURNS boolean
    LANGUAGE sql IMMUTABLE STRICT
    AS $$ SELECT CASE WHEN 0 = jsonb_array_length("values") THEN true = false ELSE (SELECT RES.CNT = jsonb_array_length("values") FROM (SELECT COUNT(*) as CNT FROM jsonb_array_elements_text("array") as elt WHERE elt IN (SELECT jsonb_array_elements_text("values"))) as RES) END; $$;


ALTER FUNCTION public.array_contains_all("array" jsonb, "values" jsonb) OWNER TO nn;

--
-- Name: array_contains_all_regex(jsonb, jsonb); Type: FUNCTION; Schema: public; Owner: nn
--

CREATE FUNCTION public.array_contains_all_regex("array" jsonb, "values" jsonb) RETURNS boolean
    LANGUAGE sql IMMUTABLE STRICT
    AS $$ SELECT CASE WHEN 0 = jsonb_array_length("values") THEN true = false ELSE (SELECT RES.CNT = jsonb_array_length("values") FROM (SELECT COUNT(*) as CNT FROM jsonb_array_elements_text("array") as elt WHERE elt LIKE ANY (SELECT jsonb_array_elements_text("values"))) as RES) END; $$;


ALTER FUNCTION public.array_contains_all_regex("array" jsonb, "values" jsonb) OWNER TO nn;

--
-- Name: array_remove(jsonb, jsonb); Type: FUNCTION; Schema: public; Owner: nn
--

CREATE FUNCTION public.array_remove("array" jsonb, "values" jsonb) RETURNS jsonb
    LANGUAGE sql IMMUTABLE STRICT
    AS $$ SELECT array_to_json(ARRAY(SELECT * FROM jsonb_array_elements("array") as elt WHERE elt NOT IN (SELECT * FROM (SELECT jsonb_array_elements("values")) AS sub)))::jsonb; $$;


ALTER FUNCTION public.array_remove("array" jsonb, "values" jsonb) OWNER TO nn;

--
-- Name: idempotency_delete_expired_records(); Type: FUNCTION; Schema: public; Owner: nn
--

CREATE FUNCTION public.idempotency_delete_expired_records() RETURNS void
    LANGUAGE plpgsql
    AS $$ BEGIN DELETE FROM "_Idempotency" WHERE expire < NOW() - INTERVAL '300 seconds'; END; $$;


ALTER FUNCTION public.idempotency_delete_expired_records() OWNER TO nn;

--
-- Name: json_object_set_key(jsonb, text, anyelement); Type: FUNCTION; Schema: public; Owner: nn
--

CREATE FUNCTION public.json_object_set_key(json jsonb, key_to_set text, value_to_set anyelement) RETURNS jsonb
    LANGUAGE sql IMMUTABLE STRICT
    AS $$ SELECT concat('{', string_agg(to_json("key") || ':' || "value", ','), '}')::jsonb FROM (SELECT * FROM jsonb_each("json") WHERE key <> key_to_set UNION ALL SELECT key_to_set, to_json("value_to_set")::jsonb) AS fields $$;


ALTER FUNCTION public.json_object_set_key(json jsonb, key_to_set text, value_to_set anyelement) OWNER TO nn;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Alert; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."Alert" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    note text,
    media text,
    owner text,
    photo text,
    status text,
    address text,
    "chatRoom" text,
    "isGlobal" boolean,
    location point,
    "problemType" text,
    "totalPatrolUsers" double precision,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."Alert" OWNER TO nn;

--
-- Name: ChatMsg; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."ChatMsg" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    text text,
    image text,
    owner text,
    "chatRoom" text,
    location point,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."ChatMsg" OWNER TO nn;

--
-- Name: ChatRoom; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."ChatRoom" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."ChatRoom" OWNER TO nn;

--
-- Name: PrivacyPolicy; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."PrivacyPolicy" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    version text,
    "versionCode" double precision,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."PrivacyPolicy" OWNER TO nn;

--
-- Name: PrivateCell; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."PrivateCell" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    name text,
    type double precision,
    owner text,
    "chatRoom" text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."PrivateCell" OWNER TO nn;

--
-- Name: PublicCell; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."PublicCell" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    name text,
    owner text,
    category text,
    "cellType" double precision,
    "chatRoom" text,
    location point,
    "isVerified" boolean,
    description text,
    "verificationStatus" double precision,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."PublicCell" OWNER TO nn;

--
-- Name: RelationshipUpdate; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."RelationshipUpdate" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    op text,
    owner text,
    "owningId" text,
    "relatedId" text,
    "owningClass" text,
    "relatedClass" text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."RelationshipUpdate" OWNER TO nn;

--
-- Name: Request; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."Request" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    cell text,
    owner text,
    "sentTo" text,
    status text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."Request" OWNER TO nn;

--
-- Name: Response; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."Response" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    note text,
    seen boolean,
    alert text,
    owner text,
    "canHelp" boolean,
    received boolean,
    "travelTime" text,
    "forwardedBy" text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."Response" OWNER TO nn;

--
-- Name: _Audience; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_Audience" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    name text,
    query text,
    "lastUsed" timestamp with time zone,
    "timesUsed" double precision,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."_Audience" OWNER TO nn;

--
-- Name: _GlobalConfig; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_GlobalConfig" (
    "objectId" text NOT NULL,
    params jsonb,
    "masterKeyOnly" jsonb
);


ALTER TABLE public."_GlobalConfig" OWNER TO nn;

--
-- Name: _Hooks; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_Hooks" (
    "functionName" text,
    "className" text,
    "triggerName" text,
    url text
);


ALTER TABLE public."_Hooks" OWNER TO nn;

--
-- Name: _Idempotency; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_Idempotency" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "reqId" text,
    expire timestamp with time zone,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."_Idempotency" OWNER TO nn;

--
-- Name: _JobSchedule; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_JobSchedule" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "jobName" text,
    description text,
    params text,
    "startAfter" text,
    "daysOfWeek" jsonb,
    "timeOfDay" text,
    "lastRun" double precision,
    "repeatMinutes" double precision,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."_JobSchedule" OWNER TO nn;

--
-- Name: _JobStatus; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_JobStatus" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "jobName" text,
    source text,
    status text,
    message text,
    params jsonb,
    "finishedAt" timestamp with time zone,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."_JobStatus" OWNER TO nn;

--
-- Name: _Join:members:PrivateCell; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_Join:members:PrivateCell" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE public."_Join:members:PrivateCell" OWNER TO nn;

--
-- Name: _Join:members:PublicCell; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_Join:members:PublicCell" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE public."_Join:members:PublicCell" OWNER TO nn;

--
-- Name: _Join:roles:_Role; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_Join:roles:_Role" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE public."_Join:roles:_Role" OWNER TO nn;

--
-- Name: _Join:users:_Role; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_Join:users:_Role" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE public."_Join:users:_Role" OWNER TO nn;

--
-- Name: _PushStatus; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_PushStatus" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "pushTime" text,
    source text,
    query text,
    payload text,
    title text,
    expiry double precision,
    expiration_interval double precision,
    status text,
    "numSent" double precision,
    "numFailed" double precision,
    "pushHash" text,
    "errorMessage" jsonb,
    "sentPerType" jsonb,
    "failedPerType" jsonb,
    "sentPerUTCOffset" jsonb,
    "failedPerUTCOffset" jsonb,
    count double precision,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."_PushStatus" OWNER TO nn;

--
-- Name: _Role; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_Role" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    name text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."_Role" OWNER TO nn;

--
-- Name: _SCHEMA; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_SCHEMA" (
    "className" character varying(120) NOT NULL,
    schema jsonb,
    "isParseClass" boolean
);


ALTER TABLE public."_SCHEMA" OWNER TO nn;

--
-- Name: _Session; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_Session" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "user" text,
    "installationId" text,
    "sessionToken" text,
    "expiresAt" timestamp with time zone,
    "createdWith" jsonb,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE public."_Session" OWNER TO nn;

--
-- Name: _User; Type: TABLE; Schema: public; Owner: nn
--

CREATE TABLE public."_User" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    username text,
    email text,
    "emailVerified" boolean,
    "authData" jsonb,
    _rperm text[],
    _wperm text[],
    _hashed_password text,
    _email_verify_token_expires_at timestamp with time zone,
    _email_verify_token text,
    _account_lockout_expires_at timestamp with time zone,
    _failed_login_count double precision,
    _perishable_token text,
    _perishable_token_expires_at timestamp with time zone,
    _password_changed_at timestamp with time zone,
    _password_history jsonb
);


ALTER TABLE public."_User" OWNER TO nn;

--
-- Data for Name: Alert; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."Alert" ("objectId", "createdAt", "updatedAt", note, media, owner, photo, status, address, "chatRoom", "isGlobal", location, "problemType", "totalPatrolUsers", _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: ChatMsg; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."ChatMsg" ("objectId", "createdAt", "updatedAt", text, image, owner, "chatRoom", location, _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: ChatRoom; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."ChatRoom" ("objectId", "createdAt", "updatedAt", _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: PrivacyPolicy; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."PrivacyPolicy" ("objectId", "createdAt", "updatedAt", version, "versionCode", _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: PrivateCell; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."PrivateCell" ("objectId", "createdAt", "updatedAt", name, type, owner, "chatRoom", _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: PublicCell; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."PublicCell" ("objectId", "createdAt", "updatedAt", name, owner, category, "cellType", "chatRoom", location, "isVerified", description, "verificationStatus", _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: RelationshipUpdate; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."RelationshipUpdate" ("objectId", "createdAt", "updatedAt", op, owner, "owningId", "relatedId", "owningClass", "relatedClass", _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: Request; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."Request" ("objectId", "createdAt", "updatedAt", cell, owner, "sentTo", status, _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: Response; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."Response" ("objectId", "createdAt", "updatedAt", note, seen, alert, owner, "canHelp", received, "travelTime", "forwardedBy", _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: _Audience; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_Audience" ("objectId", "createdAt", "updatedAt", name, query, "lastUsed", "timesUsed", _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: _GlobalConfig; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_GlobalConfig" ("objectId", params, "masterKeyOnly") FROM stdin;
\.


--
-- Data for Name: _Hooks; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_Hooks" ("functionName", "className", "triggerName", url) FROM stdin;
\.


--
-- Data for Name: _Idempotency; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_Idempotency" ("objectId", "createdAt", "updatedAt", "reqId", expire, _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: _JobSchedule; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_JobSchedule" ("objectId", "createdAt", "updatedAt", "jobName", description, params, "startAfter", "daysOfWeek", "timeOfDay", "lastRun", "repeatMinutes", _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: _JobStatus; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_JobStatus" ("objectId", "createdAt", "updatedAt", "jobName", source, status, message, params, "finishedAt", _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: _Join:members:PrivateCell; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_Join:members:PrivateCell" ("relatedId", "owningId") FROM stdin;
\.


--
-- Data for Name: _Join:members:PublicCell; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_Join:members:PublicCell" ("relatedId", "owningId") FROM stdin;
\.


--
-- Data for Name: _Join:roles:_Role; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_Join:roles:_Role" ("relatedId", "owningId") FROM stdin;
\.


--
-- Data for Name: _Join:users:_Role; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_Join:users:_Role" ("relatedId", "owningId") FROM stdin;
\.


--
-- Data for Name: _PushStatus; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_PushStatus" ("objectId", "createdAt", "updatedAt", "pushTime", source, query, payload, title, expiry, expiration_interval, status, "numSent", "numFailed", "pushHash", "errorMessage", "sentPerType", "failedPerType", "sentPerUTCOffset", "failedPerUTCOffset", count, _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: _Role; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_Role" ("objectId", "createdAt", "updatedAt", name, _rperm, _wperm) FROM stdin;
\.


--
-- Data for Name: _SCHEMA; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_SCHEMA" ("className", schema, "isParseClass") FROM stdin;
_User	{"fields": {"email": {"type": "String"}, "_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "authData": {"type": "Object"}, "objectId": {"type": "String"}, "username": {"type": "String"}, "createdAt": {"type": "Date"}, "updatedAt": {"type": "Date"}, "emailVerified": {"type": "Boolean"}, "_hashed_password": {"type": "String"}}, "className": "_User"}	t
_Role	{"fields": {"name": {"type": "String"}, "roles": {"type": "Relation", "targetClass": "_Role"}, "users": {"type": "Relation", "targetClass": "_User"}, "_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "objectId": {"type": "String"}, "createdAt": {"type": "Date"}, "updatedAt": {"type": "Date"}}, "className": "_Role"}	t
_Session	{"fields": {"user": {"type": "Pointer", "targetClass": "_User"}, "_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "objectId": {"type": "String"}, "createdAt": {"type": "Date"}, "expiresAt": {"type": "Date"}, "updatedAt": {"type": "Date"}, "createdWith": {"type": "Object"}, "sessionToken": {"type": "String"}, "installationId": {"type": "String"}}, "className": "_Session"}	t
Alert	{"fields": {"note": {"type": "String"}, "media": {"type": "String"}, "owner": {"type": "Pointer", "required": true, "targetClass": "_User"}, "photo": {"type": "File"}, "_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "status": {"type": "String", "required": true, "defaultValue": "ACTIVE"}, "address": {"type": "String"}, "chatRoom": {"type": "Pointer", "targetClass": "ChatRoom"}, "isGlobal": {"type": "Boolean", "required": true, "defaultValue": false}, "location": {"type": "GeoPoint"}, "objectId": {"type": "String"}, "createdAt": {"type": "Date"}, "updatedAt": {"type": "Date"}, "problemType": {"type": "String"}, "totalPatrolUsers": {"type": "Number"}}, "className": "Alert"}	t
ChatMsg	{"fields": {"text": {"type": "String"}, "image": {"type": "File"}, "owner": {"type": "Pointer", "targetClass": "_User"}, "_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "chatRoom": {"type": "Pointer", "targetClass": "ChatRoom"}, "location": {"type": "GeoPoint"}, "objectId": {"type": "String"}, "createdAt": {"type": "Date"}, "updatedAt": {"type": "Date"}}, "className": "ChatMsg"}	t
PrivacyPolicy	{"fields": {"_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "version": {"type": "String"}, "objectId": {"type": "String"}, "createdAt": {"type": "Date"}, "updatedAt": {"type": "Date"}, "versionCode": {"type": "Number"}}, "className": "PrivacyPolicy"}	t
PrivateCell	{"fields": {"name": {"type": "String"}, "type": {"type": "Number"}, "owner": {"type": "Pointer", "required": true, "targetClass": "_User"}, "_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "members": {"type": "Relation", "targetClass": "_User"}, "chatRoom": {"type": "Pointer", "targetClass": "ChatRoom"}, "objectId": {"type": "String"}, "createdAt": {"type": "Date"}, "updatedAt": {"type": "Date"}}, "className": "PrivateCell"}	t
Request	{"fields": {"cell": {"type": "Pointer", "targetClass": "PublicCell"}, "owner": {"type": "Pointer", "required": true, "targetClass": "_User"}, "_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "sentTo": {"type": "Pointer", "targetClass": "_User"}, "status": {"type": "String"}, "objectId": {"type": "String"}, "createdAt": {"type": "Date"}, "updatedAt": {"type": "Date"}}, "className": "Request"}	t
PublicCell	{"fields": {"name": {"type": "String"}, "owner": {"type": "Pointer", "required": true, "targetClass": "_User"}, "_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "members": {"type": "Relation", "targetClass": "_User"}, "category": {"type": "String"}, "cellType": {"type": "Number"}, "chatRoom": {"type": "Pointer", "targetClass": "ChatRoom"}, "location": {"type": "GeoPoint"}, "objectId": {"type": "String"}, "createdAt": {"type": "Date"}, "updatedAt": {"type": "Date"}, "isVerified": {"type": "Boolean", "required": true, "defaultValue": false}, "description": {"type": "String"}, "verificationStatus": {"type": "Number"}}, "className": "PublicCell"}	t
RelationshipUpdate	{"fields": {"op": {"type": "String"}, "owner": {"type": "Pointer", "targetClass": "_User"}, "_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "objectId": {"type": "String"}, "owningId": {"type": "String"}, "createdAt": {"type": "Date"}, "relatedId": {"type": "String"}, "updatedAt": {"type": "Date"}, "owningClass": {"type": "String"}, "relatedClass": {"type": "String"}}, "className": "RelationshipUpdate"}	t
Response	{"fields": {"note": {"type": "String"}, "seen": {"type": "Boolean", "required": true, "defaultValue": false}, "alert": {"type": "Pointer", "targetClass": "Alert"}, "owner": {"type": "Pointer", "targetClass": "_User"}, "_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "canHelp": {"type": "Boolean", "required": false}, "objectId": {"type": "String"}, "received": {"type": "Boolean", "required": true, "defaultValue": false}, "createdAt": {"type": "Date"}, "updatedAt": {"type": "Date"}, "travelTime": {"type": "String"}, "forwardedBy": {"type": "Pointer", "targetClass": "_User"}}, "className": "Response"}	t
ChatRoom	{"fields": {"_rperm": {"type": "Array", "contents": {"type": "String"}}, "_wperm": {"type": "Array", "contents": {"type": "String"}}, "objectId": {"type": "String"}, "createdAt": {"type": "Date"}, "updatedAt": {"type": "Date"}}, "className": "ChatRoom"}	t
\.


--
-- Data for Name: _Session; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_Session" ("objectId", "createdAt", "updatedAt", "user", "installationId", "sessionToken", "expiresAt", "createdWith", _rperm, _wperm) FROM stdin;
Om2KHoX83E	2023-07-24 09:29:43.201+00	2023-07-24 09:29:43.201+00	bfJWXYT1Ql	22904551-7c46-40e3-9cc4-2c97b25f0405	r:e3a28874feed48725f5a4ee2bbfd6a63	2024-07-23 09:29:43.201+00	{"action": "signup", "authProvider": "password"}	\N	\N
MNiegbp5nt	2023-07-24 09:29:43.203+00	2023-07-24 09:29:43.203+00	rlmcKRen2b	07876cbe-da81-4feb-b02a-28c3b69c4ac5	r:766720ac5fd8002302913dc57fba8529	2024-07-23 09:29:43.203+00	{"action": "signup", "authProvider": "password"}	\N	\N
1A59VlKsKA	2023-07-24 09:29:43.143+00	2023-07-24 09:29:43.143+00	MC764BPYdh	f10d559e-1e7e-4f5f-a031-56dfb5339a34	r:556e4e2d64cd3bffe9aeca622a9913f1	2024-07-23 09:29:43.143+00	{"action": "signup", "authProvider": "password"}	\N	\N
L0g3Hu0iBJ	2023-07-24 09:29:43.195+00	2023-07-24 09:29:43.195+00	aoGEm8TNnd	de5bc493-0306-48b9-aea2-0f8fa42de4da	r:f13d4638c5572181d0ce29f7ac53e189	2024-07-23 09:29:43.195+00	{"action": "signup", "authProvider": "password"}	\N	\N
W4dHigAQON	2023-07-24 09:29:43.2+00	2023-07-24 09:29:43.2+00	t7G22l4upu	e3d5511a-c294-4724-8cab-98f574102d25	r:0b2becaf559c2f1cf6234bca09732246	2024-07-23 09:29:43.2+00	{"action": "signup", "authProvider": "password"}	\N	\N
Omghz61XBh	2023-07-24 09:29:43.206+00	2023-07-24 09:29:43.206+00	xDxJWsCaPE	22e0e668-760a-4320-801d-87cce3cdd4d7	r:754408dcb5654b1428fbd34e9393d0f4	2024-07-23 09:29:43.206+00	{"action": "signup", "authProvider": "password"}	\N	\N
Bp7Sghs80e	2023-07-24 09:29:43.142+00	2023-07-24 09:29:43.142+00	crNCBcvfNn	d983c73c-9957-419f-862b-0f49adc9bf39	r:f205cf45ce2a598fdcc7a64d678640fb	2024-07-23 09:29:43.141+00	{"action": "signup", "authProvider": "password"}	\N	\N
4fxFhZl39s	2023-07-24 09:29:43.144+00	2023-07-24 09:29:43.144+00	FG9NC3v0gH	20e4e21e-6497-4a32-ba8f-17a225bfe673	r:116d1674c94c8a47433b93039992de50	2024-07-23 09:29:43.144+00	{"action": "signup", "authProvider": "password"}	\N	\N
\.


--
-- Data for Name: _User; Type: TABLE DATA; Schema: public; Owner: nn
--

COPY public."_User" ("objectId", "createdAt", "updatedAt", username, email, "emailVerified", "authData", _rperm, _wperm, _hashed_password, _email_verify_token_expires_at, _email_verify_token, _account_lockout_expires_at, _failed_login_count, _perishable_token, _perishable_token_expires_at, _password_changed_at, _password_history) FROM stdin;
crNCBcvfNn	2023-07-24 09:29:42.832+00	2023-07-24 09:29:42.832+00	dev1@copblock.app	dev1@copblock.app	\N	\N	{*,crNCBcvfNn}	{crNCBcvfNn}	$2y$10$otzuHQj0oVxImq6aLne6H.U0Kay.uno0OLvvxhpQLv0X2YvQSwoI2	\N	\N	\N	\N	\N	\N	\N	\N
MC764BPYdh	2023-07-24 09:29:42.828+00	2023-07-24 09:29:42.828+00	dev0@copblock.app	dev0@copblock.app	\N	\N	{*,MC764BPYdh}	{MC764BPYdh}	$2y$10$qleHRhGTNQXDIcVvnPZf4uNc24mKRM7jBHl2/al7DT14WD2tfDyGe	\N	\N	\N	\N	\N	\N	\N	\N
FG9NC3v0gH	2023-07-24 09:29:42.837+00	2023-07-24 09:29:42.837+00	dev3@copblock.app	dev3@copblock.app	\N	\N	{*,FG9NC3v0gH}	{FG9NC3v0gH}	$2y$10$2a4kNxdBbde.VKUJ1Y9fmeTNMEVMn7u4OXhY9tpu.2iE2sFw3d/5W	\N	\N	\N	\N	\N	\N	\N	\N
aoGEm8TNnd	2023-07-24 09:29:42.834+00	2023-07-24 09:29:42.834+00	dev2@copblock.app	dev2@copblock.app	\N	\N	{*,aoGEm8TNnd}	{aoGEm8TNnd}	$2y$10$Ofatnrih388vAIa9wBvG5enOAlPbov.gxhx53Pv3/w7SQ1K8hNcf6	\N	\N	\N	\N	\N	\N	\N	\N
t7G22l4upu	2023-07-24 09:29:42.839+00	2023-07-24 09:29:42.839+00	dev4@copblock.app	dev4@copblock.app	\N	\N	{*,t7G22l4upu}	{t7G22l4upu}	$2y$10$m8Wj9GM5xNRnqpQcNrDc8epYh60CP3ZKrPGxM/AWrbfI2FjGSsG7i	\N	\N	\N	\N	\N	\N	\N	\N
bfJWXYT1Ql	2023-07-24 09:29:42.84+00	2023-07-24 09:29:42.84+00	dev5@copblock.app	dev5@copblock.app	\N	\N	{*,bfJWXYT1Ql}	{bfJWXYT1Ql}	$2y$10$5238Sl9vU.dSDKCz09Hnt.WcfmR8o2G7TWAzocEw/KJW.D/UtCpIS	\N	\N	\N	\N	\N	\N	\N	\N
rlmcKRen2b	2023-07-24 09:29:42.843+00	2023-07-24 09:29:42.843+00	dev7@copblock.app	dev7@copblock.app	\N	\N	{*,rlmcKRen2b}	{rlmcKRen2b}	$2y$10$jMK5WZN400kLRy5H1889j.UICwRHiK8P418eom2JXXzEzUSCf4wxO	\N	\N	\N	\N	\N	\N	\N	\N
xDxJWsCaPE	2023-07-24 09:29:42.842+00	2023-07-24 09:29:42.842+00	dev6@copblock.app	dev6@copblock.app	\N	\N	{*,xDxJWsCaPE}	{xDxJWsCaPE}	$2y$10$VQhUcFEGwIQ6vgghzoIWUOl0Y5tQXCE36lBElSd9T374rXknBTnc.	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Name: Alert Alert_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."Alert"
    ADD CONSTRAINT "Alert_pkey" PRIMARY KEY ("objectId");


--
-- Name: ChatMsg ChatMsg_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."ChatMsg"
    ADD CONSTRAINT "ChatMsg_pkey" PRIMARY KEY ("objectId");


--
-- Name: ChatRoom ChatRoom_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."ChatRoom"
    ADD CONSTRAINT "ChatRoom_pkey" PRIMARY KEY ("objectId");


--
-- Name: PrivacyPolicy PrivacyPolicy_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."PrivacyPolicy"
    ADD CONSTRAINT "PrivacyPolicy_pkey" PRIMARY KEY ("objectId");


--
-- Name: PrivateCell PrivateCell_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."PrivateCell"
    ADD CONSTRAINT "PrivateCell_pkey" PRIMARY KEY ("objectId");


--
-- Name: PublicCell PublicCell_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."PublicCell"
    ADD CONSTRAINT "PublicCell_pkey" PRIMARY KEY ("objectId");


--
-- Name: RelationshipUpdate RelationshipUpdate_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."RelationshipUpdate"
    ADD CONSTRAINT "RelationshipUpdate_pkey" PRIMARY KEY ("objectId");


--
-- Name: Request Request_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."Request"
    ADD CONSTRAINT "Request_pkey" PRIMARY KEY ("objectId");


--
-- Name: Response Response_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."Response"
    ADD CONSTRAINT "Response_pkey" PRIMARY KEY ("objectId");


--
-- Name: _Audience _Audience_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_Audience"
    ADD CONSTRAINT "_Audience_pkey" PRIMARY KEY ("objectId");


--
-- Name: _GlobalConfig _GlobalConfig_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_GlobalConfig"
    ADD CONSTRAINT "_GlobalConfig_pkey" PRIMARY KEY ("objectId");


--
-- Name: _Idempotency _Idempotency_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_Idempotency"
    ADD CONSTRAINT "_Idempotency_pkey" PRIMARY KEY ("objectId");


--
-- Name: _JobSchedule _JobSchedule_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_JobSchedule"
    ADD CONSTRAINT "_JobSchedule_pkey" PRIMARY KEY ("objectId");


--
-- Name: _JobStatus _JobStatus_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_JobStatus"
    ADD CONSTRAINT "_JobStatus_pkey" PRIMARY KEY ("objectId");


--
-- Name: _Join:members:PrivateCell _Join:members:PrivateCell_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_Join:members:PrivateCell"
    ADD CONSTRAINT "_Join:members:PrivateCell_pkey" PRIMARY KEY ("relatedId", "owningId");


--
-- Name: _Join:members:PublicCell _Join:members:PublicCell_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_Join:members:PublicCell"
    ADD CONSTRAINT "_Join:members:PublicCell_pkey" PRIMARY KEY ("relatedId", "owningId");


--
-- Name: _Join:roles:_Role _Join:roles:_Role_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_Join:roles:_Role"
    ADD CONSTRAINT "_Join:roles:_Role_pkey" PRIMARY KEY ("relatedId", "owningId");


--
-- Name: _Join:users:_Role _Join:users:_Role_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_Join:users:_Role"
    ADD CONSTRAINT "_Join:users:_Role_pkey" PRIMARY KEY ("relatedId", "owningId");


--
-- Name: _PushStatus _PushStatus_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_PushStatus"
    ADD CONSTRAINT "_PushStatus_pkey" PRIMARY KEY ("objectId");


--
-- Name: _Role _Role_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_Role"
    ADD CONSTRAINT "_Role_pkey" PRIMARY KEY ("objectId");


--
-- Name: _SCHEMA _SCHEMA_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_SCHEMA"
    ADD CONSTRAINT "_SCHEMA_pkey" PRIMARY KEY ("className");


--
-- Name: _Session _Session_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_Session"
    ADD CONSTRAINT "_Session_pkey" PRIMARY KEY ("objectId");


--
-- Name: _User _User_pkey; Type: CONSTRAINT; Schema: public; Owner: nn
--

ALTER TABLE ONLY public."_User"
    ADD CONSTRAINT "_User_pkey" PRIMARY KEY ("objectId");


--
-- Name: _Idempotency_unique_reqId; Type: INDEX; Schema: public; Owner: nn
--

CREATE UNIQUE INDEX "_Idempotency_unique_reqId" ON public."_Idempotency" USING btree ("reqId");


--
-- Name: _Role_unique_name; Type: INDEX; Schema: public; Owner: nn
--

CREATE UNIQUE INDEX "_Role_unique_name" ON public."_Role" USING btree (name);


--
-- Name: _User_unique_email; Type: INDEX; Schema: public; Owner: nn
--

CREATE UNIQUE INDEX "_User_unique_email" ON public."_User" USING btree (email);


--
-- Name: _User_unique_username; Type: INDEX; Schema: public; Owner: nn
--

CREATE UNIQUE INDEX "_User_unique_username" ON public."_User" USING btree (username);


--
-- Name: case_insensitive_email; Type: INDEX; Schema: public; Owner: nn
--

CREATE INDEX case_insensitive_email ON public."_User" USING btree (lower(email) varchar_pattern_ops);


--
-- Name: case_insensitive_username; Type: INDEX; Schema: public; Owner: nn
--

CREATE INDEX case_insensitive_username ON public."_User" USING btree (lower(username) varchar_pattern_ops);


--
-- Name: ttl; Type: INDEX; Schema: public; Owner: nn
--

CREATE INDEX ttl ON public."_Idempotency" USING btree (expire);


--
-- PostgreSQL database dump complete
--

