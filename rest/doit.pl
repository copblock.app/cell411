#!/usr/bin/perl
# vim: ts=2 sw=2 ft=perl
eval 'exec perl -x -wS $0 ${1+"$@"}'
  if 0;

use strict;
use warnings;

BEGIN {
  use FindBin qw($Bin $Script);
  use lib "$Bin/../lib/perl";
  $|++; $\="\n"; $,=" "; $"=" "; $\="\n";
};
use autodie qw(:all);
use Data::Dump;
use LWP;
require LWP;

use URI;
use URI::Heuristic qw(uf_uri);
use Encode;
use Encode::Locale;
use HTTP::Status qw(status_message);
use HTTP::Date qw(time2str str2time);
use LWP::UserAgent;


my $url=URI->new( "https://localhost:1338/empty" );

my $ur = new LWP::UserAgent;

