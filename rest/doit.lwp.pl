#!/usr/bin/perl
# vim: ts=2 sw=2 ft=perl
eval 'exec perl -x -wS $0 ${1+"$@"}'
  if 0;

use strict;
use warnings;

BEGIN {
  use FindBin qw($Bin $Script);
  use lib "$Bin/../lib/perl";
  $|++; $\="\n"; $,=" "; $"=" "; $\="\n";
};
use autodie qw(:all);
use Data::Dump;
use LWP;

require LWP;

use URI;
use URI::Heuristic qw(uf_uri);
use Encode;
use Encode::Locale;
use HTTP::Status qw(status_message);
use HTTP::Date qw(time2str str2time);
use LWP::UserAgent;


my $method="GET";
my $url="https://dev.copblock.app:443/empty";
$url=URI->new($url);
print "URL: $url";
my $rq = HTTP::Request->new($method);
$rq->url($url);

$rq->header(
  'Accept',
  'application/json'
);
$rq->header(
  'X-Parse-Application-Id',
  'empty'
);
$rq->header(
  'X-Parse-REST-API-Key',
  'empty'
);

my $ua = LWP::UserAgent->new;
my $rs = $ua->request($rq);
if($rs->is_success){
  print STDERR "Success!";
  print $rs->content;
} else {
  print STDERR $rs->status_line;
};

