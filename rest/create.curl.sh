#!/bin/bash

set -xv
source ~/.parse/config-empty.sh
#printf '%-40s %-40s\n'
curl -X POST \
  -H "X-Parse-Application-Id: $PARSE_ID" \
  -H "X-Parse-REST-API-Key: $PARSE_CLIENT" \
  -H "Content-Type: application/json" \
  -d '{}' \
  "$PARSE_PUBLIC_URL/$PARSE_FLAVOR/classes/PrivateCell"
