#!/bin/bash

set -xv
source ~/.parse/config-empty.sh

#test -z "$*" || eval "$*"

set -- curl -D headers.out -X GET \
  -H "X-Parse-Application-Id: $PARSE_ID" \
  -H "X-Parse-REST-API-Key: $PARSE_CLIENT" \
  -H "Content-Type: application/json" \
  -d '{}' \
  "$PARSE_PUBLIC_URL/$PARSE_FLAVOR/health"
