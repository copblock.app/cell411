#include "system.hh"
#include "checkret.hh"
#include "util.hh"
#include "fixed_buf.hh"
#include "md5.h"
#include "md5.hh"
#include "unixpp.hh"


using namespace checkret;
using unixpp::range_t;
using unixpp::xmmap_file;
using std::string;
extern "C" {
  int dprintf(int, const char *, ...);
  int atoi(const char *);
}


static std::array<char,1024*1024> buf;

int ifd=-1;

int wait_nohang(int *stat) {
  int res = waitpid(-1,stat,WNOHANG);
  return res;
}
void sigchild(int arg){
  dprintf(1,"sigchild(%d)\n",arg);
  int wstat;
  pid_t pid = wait(&wstat);
  if(pid<0)
    pexit("wait");
  dprintf(1,"wait_nohang(&wstat)=>%d\n", wait_nohang(&wstat));
  dprintf(1,"             wstat =>%d\n", wstat);
};
const string gen_tmpfile(){
  static char buf[128];
  size_t len=snprintf(buf,sizeof(buf),"%s-%08d.bin",fmt_now(),getpid())>=sizeof(buf);
  if(len>sizeof(buf)){
    dprintf(2,"overflow(%lu>%lu)\n",len,sizeof(buf));
    exit(20);
  };
  return string(buf);
};
int main(int argc, char**argv){
  int port=0;
  if(argc!=4) {
    dprintf(2,"usage: %s <purpose> <oid> <port>\n",argv[0]);
    exit(1);
  }

  const string purpose=argv[1];
  const string oid=argv[2];
  port=atoi(argv[3]);
  signal(SIGCHLD, &sigchild);
  alarm(5);
  ifd=bind_accept_nofork("0.0.0.0",port);
  alarm(0);
  const string tmpfile=gen_tmpfile();
  mkdirat(AT_FDCWD,"upload",0777);
  xchdir("upload");
  int ofd=xopenat(AT_FDCWD,tmpfile.c_str(),O_CREAT|O_WRONLY|O_EXCL,0666);
  // calling the normal one, if it fails, it fails.
  int tfd=openat(AT_FDCWD,"/dev/tty",O_WRONLY);
  size_t rtot=0,wtot=0,wlen=0,rlen=0;

#define speak()    dprintf(tfd,"%16lu %16lu %16lu %16lu\r",rlen,rtot,wlen,wtot);
  while(true){
    size_t rlen=xread(ifd,buf.begin(),sizeof(buf));
    rtot+=rlen;
    speak();
    const char *beg(buf.begin());
    const char *end=beg+rlen;
    if(beg==end)
      break;
    while(beg<end) {
      wlen=xwrite(ofd,beg,end-beg);
      wtot+=wlen;
      speak();
      beg+=wlen;
    }
    speak();
  };

  range_t file=xmmap_file(tmpfile.c_str());
  string md5sum=unixpp::md5sum(file);
  string ext = unixpp::magic_ext(file);
  dprintf(2,"ext: %s\n",ext.c_str());
  auto pos=ext.begin();
  while(pos!=ext.end()){
    if(*pos=='/')
      break;
    else
      pos++;
  };
  if(pos!=ext.end())
    ext=string(ext.begin(),pos);
  dprintf(2,"ext: %s\n",ext.c_str());

  string filename=purpose+"."+oid+"."+md5sum+ext;
  string json="\n\n{\n  \"file\": \"";
  json += filename;
  json += "\",\n  \"md5\": \"";
  json += md5sum;
  json += "\",\n  \"mime\": \"";
  json += unixpp::magic_mime(file);
  json += "\",\n  \"ext\": \"";
  json += ext;
  json += "\"\n}\n\n";
  write(1,json.c_str(),json.length());
  xclose(ifd);
  unlink(filename.c_str());
  xlink(tmpfile.c_str(),filename.c_str());
  xunlink(tmpfile.c_str());
  return 0;
}
