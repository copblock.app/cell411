import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.TreeMap;
import java.util.function.Supplier;

class Scratch {
  static class NewInvoker implements InvocationHandler {
    final Class<?> mType;

    NewInvoker(Class<?> type) {
      mType = type;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
      switch(method.getName()) {
        case "toString":
          if(method.getParameterCount()==0)
            return "Supplier Of "+mType;
          break;
        case "hashCode":
          if(method.getParameterCount()==0)
            return proxy.toString().hashCode();
        case "equals":
          if(method.getParameterCount()==1)
            return args[0]==this;
        case "get":
          if(method.getParameterCount()==0)
            return mType.newInstance();
        default:
          break;
      }
      throw new NoSuchMethodError("No method for "+method);
    }
  }
  static Class<?>[] smTypes = new Class<?>[]{ Supplier.class } ;
  static TreeMap<String, Supplier<?>> smCache = new TreeMap<>();

  static <X> Supplier<X> forClass(Class<X> type)
  {
    String name=type.getName();
    Supplier<?> supplier = smCache.computeIfAbsent(name,
            (n)-> makeNewInvoker(type));

    //noinspection unchecked
    return (Supplier<X>)supplier;
  }

  private static Supplier<?> makeNewInvoker(Class<?> type) {
    NewInvoker invoker = new NewInvoker(type);
    Supplier<?> supplier = (Supplier<?>)
            Proxy.newProxyInstance(type.getClassLoader(),smTypes,invoker);
    return supplier;
  }

  public static void main(String[] args) {
    
  }
}