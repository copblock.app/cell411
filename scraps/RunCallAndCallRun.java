  static class CallableRunnable
    extends Adapter
  {
    final Runnable mRunnable;
    final Object[] mArgs;
    final Method mMethod;

    Throwable mThrowable;

    CallableRunnable(Runnable runnable, Object[] args, Method method)
    {
      mRunnable = runnable;
      mArgs = args;
      mMethod = method;
    }

    public void run() {
      try {
        mRunnable.run();
      } catch ( Throwable throwable) {
        mThrowable = throwable;
      }
    }

    @Override
    public Void call()
    {
      run();
      return null;
    }
  }
  static class RunnableCallable<X>
    extends Adapter<X>
    implements Runnable, Callable<X>
  {
    final Callable<X> mCallable;
    final Class<X> mClass;
    X mResult;
    Throwable mThrowable;

    RunnableCallable(Class<X> aClass, Callable<X> callable)
    {
      mCallable = callable;
      mClass = aClass;
    }

    @Override
    public void run()
    {
      try {
        call();
      } catch( Throwable throwable ) {
        mThrowable = throwable;
      }
    }

    @Override
    public X call()
      throws Exception
    {
      mResult=mCallable.call();
      return mResult;
    }
  }

