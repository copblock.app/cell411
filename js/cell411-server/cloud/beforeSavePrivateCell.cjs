async function beforeSavePrivateCell(req) {
  const cell = req.object;
  const user = req.user;
  await storeUpdates(req,cell.id,"PrivateCell","members");
};
Parse.Cloud.beforeSave("PrivateCell",beforeSavePrivateCell);
