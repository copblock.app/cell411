async function signUp(req) {
  try {
    const params = req.params;
    const username=params.username;
    if(username==null || !username.length){
      return {
        success: false,
        message: "username must be non-mull"
      };
    } else {
      const parts = username.split("@");
      if(parts.length!=2){
        return {
          success: false,
          message: "username/email must be valid address"
        };
      }
    };
    const password=params.password;
    if(password==null || !password.length){
      return {
        success: false,
        message: "password must be non-mull"
      };
    };
    var user = new Parse.User();
    const keys = Object.keys(params);
    for(var i=0;i<keys.length;i++){
      if(keys[i]==="base64")
        continue;
      user.set(keys[i],params[keys[i]]);
    };
    user=await user.signUp(null,{useMasterKey: true});
    user.set("password",password);
    user=await user.logIn({useMasterKey: true});
    const priv1 = new PrivateCell();
    priv1.set("owner", user);
    priv1.set("type",1);
    priv1.set("name","Family");
    priv1.save(null,{useMasterKey: true});
    const priv2 = new PrivateCell();
    priv2.set("owner", user);
    priv2.set("type",2);
    priv2.set("name","Coworkers");
    priv2.save(null,{useMasterKey: true});
    const priv3 = new PrivateCell();
    priv3.set("owner", user);
    priv3.set("type",3);
    priv3.set("name","School Friends");
    priv3.save(null,{useMasterKey: true});
    const priv4 = new PrivateCell();
    priv4.set("owner", user);
    priv4.set("type",4);
    priv4.set("name","Neighborhood Watch");
    priv4.save(null,{useMasterKey: true});
    const priv5 = new PrivateCell();
    priv5.set("owner", user);
    priv5.set("type",5);
    priv5.set("name","Friends");
    priv4.save(null,{useMasterKey: true});
    return {
      success: true,
      user
    };
  } catch ( err ) {
    console.log(err);
    throw err;
  };
}
Parse.Cloud.define("signUp", signUp);
