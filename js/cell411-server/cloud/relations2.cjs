async function gatherRelations(req)
{
  var user = req.user;
  if(user==null)
    throw new Error("You must be logged in to call relations");
  
  const chatRelId =  ["_User","connectedTo","ChatRoom","objectId" ];
  var chatRel = [];

//     const req2 = JSON.parse(JSON.stringify(req));
//     const keys = Object.keys(req);
//     for(var i=0;i<keys.length;i++) {
//       const key=keys[i];
//       const disp={};
//       disp.key=key;
//       disp[key]=req[key];
//       console.log(disp);
//     };
  
  const dates = {};
  function updateDate(obj){
    var date=obj.get("updatedAt");
    if(date==null)
      return;
    date=date.getTime();
    if(date==null)
      return;
    dates[obj.id]=date;
  }
  const rels = [ dates ];
  user.fetch();
  chatRelId.push(user.id);
  updateDate(user);
  await loadFriends();
//     await loadBlocks(false);
//     await loadBlocks(true);
  console.log("Private Cells");
  await loadCells("PrivateCell","owner");
  await loadCells("PrivateCell","members");

  console.log("Public Cells");
  await loadCells("PublicCell","owner");
  await loadCells("PublicCell","members");

  console.log("Requests");
  await loadRequests("owner");
  await loadRequests("sentTo");

  await loadRelatedAlerts();

  {
    console.log(chatRel);
    const chatObj={};
    chatRel.forEach((id)=>{chatObj[id]=1});
    chatRel=Object.keys(chatObj);
    console.log(chatRel);
  }
  chatRel.unshift(chatRel.length);
  while(chatRelId.length>0){
    chatRel.unshift(chatRelId.pop());
  }
  rels.push(chatRel);
  async function loadRelatedAlerts() {
    console.log("loadRelatedAlerts");
    var relatedAlertRel = [ "_User", "audienceOf", "Alert", "objectId", user.id];
    rels.push(relatedAlertRel);
    const time = new Date().getTime();
    const minTime = time-86400*1000*3;
    //const query = new Parse.Query("Alert").matchesKeyInQuery("objectId","objectId",query);
    const query = Parse.Query.or(
      new Parse.Query("Alert").containedBy("audience",[user.id]),
      new Parse.Query("Alert").equalTo("owner",user.id)
    );
    query.greaterThan("createdAt", new Date(minTime));
    const alerts = await findFully(query);
    console.log({query,alerts});
    relatedAlertRel.push(alerts.length);
    for(var i=0;i<alerts.length;i++){
      const alert=alerts[i];
      updateDate(alert);
      relatedAlertRel.push(alert.id);
      const chatRoom = alert.get("chatRoom");
      if(chatRoom!=null) {
        updateDate(chatRoom);
        chatRel.push(chatRoom.id);
      };
    };
  };
  async function loadRequests(rel) {
    console.log("loadRequests");
    var requestRel = [ "_User", null, "Request", "objectId", user.id];
    if(rel=="owner"){
      requestRel[1]="ownerOf";
    } else {
      requestRel[1]="sentToOf";
    }
    const query = new Parse.Query("Request").equalTo(rel,user.id);
    query.containedIn("status",["PENDING","RESENT"]);
    console.log(query);
    const requests=await findFully(query);
    requestRel.push(requests.length);
    for(var i=0;i<requests.length;i++){
      const request=requests[i];
      updateDate(request);
      requestRel.push(request.id);
    };
    rels.push(requestRel);
    console.log(requestRel);
  };
  async function loadFriends() {
    console.log("loadFriends");
    var friendRel = [ "_User", "friends", "_User","objectId", user.id];
    var friends = await findFully(user.relation("friends").query());
    rels.push(friendRel);
    var i;
    friendRel.push(friends.length);
    for(i=0;i<friends.length;i++) {
      const friend = friends[i]; 
      updateDate(friend);
      friendRel.push(friend.id);
    };
    console.log({i,friend: null,rels});
  }
  async function loadCells(className, relname) {
    console.log("loadCells");
    var query = new Parse.Query(className);
    query.equalTo(relname,user);
    console.log(query);
    var result;
    if(relname=="owner"){
      result = [ "_User", "ownerOf", className, "objectId", user.id ];
    } else {
      result = [ "_User", "memberOf",className, "objectId", user.id ];
    };
    rels.push(result);
    const cells  = await findFully(query);
    console.log(`got ${cells.length} cells`);
    result.push(cells.length);
    for(var i=0;i<cells.length;i++) {
      const cell=cells[i];
      result.push(cell.id);
      updateDate(cell);
      const chatRoom = cell.get("chatRoom");
      if(chatRoom!=null) {
        updateDate(chatRoom);
        chatRel.push(chatRoom.id);
      };
      const relation = cell.relation("members");
      const query = relation.query();
      query.include("objectId");
      query.include("updatedAt");
      const memberRel = [ className, "members", "_User","objectId", cell.id ];
      const members = await findFully(cell.relation("members").query());
      console.log(`got ${members.length} members`);
      memberRel.push(members.length);
      for(var j=0;j<members.length;j++){
        const member=members[j];
        updateDate(member);
        memberRel.push(member.id);
      };
      rels.push(memberRel);
    };

  }
  return rels;
}
async function relations(req) {
  const start = new Date().getTime();
  try {
    
    return gatherRelations(req);
  } finally {
    const now = new Date().getTime();
    console.log(`Done after: ${(now-start)/1000.0} seconds`);
  };
};
Parse.Cloud.define("relations2",relations);
