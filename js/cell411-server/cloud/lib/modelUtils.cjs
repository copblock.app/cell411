async function blocks(user1,user2){
  const query=new Parse.Query(Parse.User);
  query.equalTo("objectId", user1)
  query.equalTo("spamUsers", user2);
  const count = await query.count();
  return count!=0;
};
async function getFriendRequest(user,friend){
  var reqQuery;
  reqQuery = new Parse.Query(Request);
  reqQuery.equalTo("owner",user);
  reqQuery.equalTo("sentTo",friend);
  reqQuery.equalTo("status","PENDING");
  return await reqQuery.find();
};
async function markFriendRequests(user,friend,status,oneWay) {
  var res = 0;
  var req1 = await getFriendRequest(user,friend);
  for(var i=0;i<req1.length;i++){
    req1[i].set("status",status);
    await req1[i].save(null,{useMasterKey: true});
    res++;
  };
  if(!oneWay) {
    var req2 = await getFriendRequest(friend,user);
    for(var i=0;i<req2.length;i++){
      req2[i].set("status",status);
      await req2[i].save(null,{useMasterKey: true});
      res++;
    };
  };
  return res;
};
async function destroyFriendship(user,friend){
  user.relation("friends").remove(friend);
  await user.save(null,{useMasterKey: true});

  friend.relation("friends").remove(user);
  await friend.save(null,{useMasterKey: true});
};
global.destroyFriendship=destroyFriendship;
async function createFriendshipObj(user,friend){
  user.relation("friends").add(friend);
  await user.save(null,{useMasterKey: true});

  friend.relation("friends").add(user);
  await friend.save(null,{useMasterKey: true});
};
async function createFriendship(request){
  const owner=request.get("owner");
  const sentTo=request.get("sentTo");
 
  createFriendshipObj(owner,sentTo);
  const query=new Parse.Query("Request");
  query.containedIn("owner",[owner,sentTo]);
  query.containedIn("sentTo",[owner,sentTo]);
  query.equalTo("status","PENDING");
  const requests=await findFully(query);
  for(var i=0;i<requests.length;i++){
    const req=requests[i];
    req.set("status","APPROVED");
    await req.save(null,{useMasterKey:true});
  };
  return true;
};
global.createFriendship=createFriendship;
async function blockCheck(user1, user2) {
  if(await blocks(user1,user2))
    throw new Error(user1.getName()+" blocks "+user2.getName());
  
  if(await blocks(user2,user1))
    throw new Error(user2.getName()+" blocks "+user1.getName());
  
  return false;
}
async function storeUpdates(req,owningId,owningClass,fieldName){
  const object=JSON.parse(JSON.stringify(req.object));
  if(!object[fieldName]) {
    return;
  };
  if(!object[fieldName].__op) {
    return;
  };
  const op = object[fieldName].__op;
  if(op==="Batch"){
    const ops = object[fieldName].ops;
    console.log({ops});
  } else {
    const relatedClass=object[fieldName].objects[0].className;
    for(var i=0;i<object[fieldName].objects.length;i++){
      const relatedId=object[fieldName].objects[i].objectId;
      const owningField=fieldName;
      const relatedField="objectId";
      console.log({op,owningClass,owningField,owningId,relatedClass,relatedField,relatedId});
      const info = new Parse.Object("Update",{
        op,owningClass,owningField,owningId,relatedClass,relatedField,relatedId
      });
      await info.save();
    };
  }
};
global.storeUpdates=storeUpdates;
module.exports.storeUpdates=storeUpdates;
module.exports.blockCheck=blockCheck;
module.exports.markFriendRequests=markFriendRequests;
module.exports.createFriendship=createFriendship;
module.exports.destroyFriendship=destroyFriendship;

