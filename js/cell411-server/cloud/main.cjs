console.log("starting main.cjs");
const sums = {};
function firstTime() {
  const list = fs.readdirSync("cloud"); 
  for(var i=0;i<list.length;i++){
    const file=list[i];
    if(!file.endsWith(".cjs"))
      continue;
    require("./"+file);
  };
};
firstTime();
