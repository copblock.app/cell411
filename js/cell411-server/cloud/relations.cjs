async function gatherRelations(req)
{
  var user = req.user;
  if(user==null)
    throw new Error("You must be logged in to call relations");

  const params = req.params;
  const cDates = ( params && params.dates ) ? params.dates : {};
  var chatRel = [];

  const counterParties=[];
  const sDates = {};
  const objs = {};
  const rels = {};
  const removed = [];
  const result = { removed, rels, objs, dates: sDates, counterParties };
  function updateDate(obj){
    var date=obj.get("updatedAt");
    if(date==null)
      return;
    date=date.getTime();
    if(date==null)
      return;
    sDates[obj.id]=date;
  }
  user.fetch();
  updateDate(user);
  await loadFriends();
  console.log("Private Cells : owner");
  await loadCells("PrivateCell","owner");
  console.log("Private Cells : members");
  await loadCells("PrivateCell","members");

  console.log("Public Cells: owner");
  await loadCells("PublicCell","owner");
  console.log("Public Cells: members");
  await loadCells("PublicCell","members");

  console.log("Requests: owner");
  await loadRequests("owner");
  console.log("Requests: sentTo");
  await loadRequests("sentTo");

  console.log("Alerts");
  await loadRelatedAlerts();

  const chatRelId =  "_User:connectedTo:ChatRoom:objectId:"+user.id ;
  rels[chatRelId]=chatRel;
  async function loadRelatedAlerts() {
    var relId = [ "_User", "audienceOf", "Alert", "objectId", user.id];
    relId=relId.join(":");
    const rel=[];
    rels[relId]=rel;
    const time = new Date().getTime();
    const minTime = time-86400*1000*3;
    const query = Parse.Query.or(
      new Parse.Query("Alert").containedBy("audience",[user.id]),
      new Parse.Query("Alert").equalTo("owner",user.id)
    );
//    query.greaterThan("createdAt", new Date(minTime));
    const alerts = await findFully(query);
    for(var i=0;i<alerts.length;i++){
      const alert=alerts[i];
      updateDate(alert);
      rel.push(alert.id);
      objs[alert.id]=alert;
      const chatRoom = alert.get("chatRoom");
      if(chatRoom!=null) {
        updateDate(chatRoom);
        chatRel.push(chatRoom.id);
        objs[chatRoom.id]=chatRoom;
      };
    };
  };
  async function loadRequests(relName) {
    const query = new Parse.Query("Request").equalTo(relName,user.id);
    query.containedIn("status",["PENDING","RESENT"]);
    const requests=await findFully(query);
    const rel=[];
    for(var i=0;i<requests.length;i++){
      const request=requests[i];
      updateDate(request);
      rel.push(request.id);
      objs[request.id]=request;
      var party = request.get("owner");
      if(party.id !== user.id) {
        await party.fetch();
        counterParties.push(party.id);
        updateDate(party);
        objs[party.id]=party;
      }
      party=request.get("sentTo");
      if(party.id !== user.id) {
        await party.fetch();
        counterParties.push(party.id);
        updateDate(party);
        objs[party.id]=party;
      }
    };
    var relId = [ "_User", null, "Request", "objectId", user.id];
    if(relName=="owner"){
      relId[1]="ownerOf";
    } else {
      relId[1]="sentToOf";
    }
    relId=relId.join(":");
    rels[relId]=rel;
  };
  async function loadFriends() {
    var relId = [ "_User", "friends", "_User","objectId", user.id].join(":");
    var rel=[];
    var friends = await findFully(user.relation("friends").query());
    rels[relId]=rel;
    var i;
    for(i=0;i<friends.length;i++) {
      const friend = friends[i]; 
      updateDate(friend);
      rel.push(friend.id);
      objs[friend.id]=friend;
    };
  }
  async function loadCells(className, relname) {
    var query = new Parse.Query(className);
    query.equalTo(relname,user);
    var relId;
    relId = [ "_User", null, className, "objectId", user.id ];
    if(relname=="owner"){
      relId[1]="ownerOf";
    } else {
      relId[1]="memberOf";
    };
    relId=relId.join(":");
    const rel=[];
    rels[relId]=rel;
    const cells  = await findFully(query);
    for(var i=0;i<cells.length;i++) {
      const cell=cells[i];
      rel.push(cell.id);
      objs[cell.id]=cell;
      updateDate(cell);
      const chatRoom = cell.get("chatRoom");
      if(chatRoom!=null) {
        updateDate(chatRoom);
        chatRel.push(chatRoom.id);
        objs[chatRoom.id]=chatRoom;
      };
      const relation = cell.relation("members");
      const query = relation.query();
      query.include("objectId");
      query.include("updatedAt");
      const memRelId = [ className, "members", "_User","objectId", cell.id ].join(":");
      const memRel=[];
      rels[memRelId]=memRel;
      const members = await findFully(cell.relation("members").query());
      for(var j=0;j<members.length;j++){
        const member=members[j];
        updateDate(member);
        memRel.push(member.id);
        objs[member.id]=member;
      };
      rels[relId]=rel;
    };

  }
  for(var id in cDates) {
    if(cDates[id]===sDates[id]) {
      delete objs[id];
      removed.push(id);
    }
  }
  console.log({result});
  return result;
}
async function relations(req) {
  const date = new Date();
  const start = date.getTime();
  console.log(`started: ${date}`);
  try {
    return await gatherRelations(req);
  } finally {
    const now = new Date().getTime();
    console.log(`Done after: ${(now-start)/1000.0} seconds`);
  };
};
Parse.Cloud.define("relations",relations);
