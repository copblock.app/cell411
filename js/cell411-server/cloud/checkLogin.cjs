async function checkLogin(req) {
  const user = req.user;
  return user!=null;
}
Parse.Cloud.define("checkLogin", checkLogin);
