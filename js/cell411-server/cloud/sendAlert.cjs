const createAudience=require('./lib/createAudience.cjs');

async function createAlert(user, alert,audience) {
  var cell411Alert = new Alert();
  if(cell411Alert==null)
    throw new Error("failed to create alert object");
  var isGlobal=false;
  for(var i=0;i<audience.length;i++){
    if(audience[i]==="global"){
      isGlobal=true;
    }
  };
  //const problemType = await new Parse.Query("ProblemType").get(alert.problemType);
  cell411Alert.set("note", alert.note);
  cell411Alert.set("owner", user);
  cell411Alert.set("isGlobal", isGlobal);
  cell411Alert.set("problemType", alert.problemType);
  cell411Alert.set("objectId", alert.alertId);
  cell411Alert.set("location", alert.location);
  console.log({cell411Alert});
  return cell411Alert;
}
async function sendAlert(req) {
  const user = req.user;
  if(user==null)
    throw new Error("caller is not logged in");
  if(typeof(req.params.alert)==="undefined")
    throw new Error("no alert in params");
  var alertdata = req.params.alert;
  if(typeof(alertdata)==='string')
    alertdata=JSON.parse(req.params.alert);
  if(typeof(alertdata)=="undefined")
    throw new Error("no alertdata");
  if(alertdata.location==null || typeof(alertdata.location)==='undefined')
    throw new Error("no location in alert");
 
  const audience = await createAudience(user,alertdata,req.params.audience);

  const alert = await createAlert(user, alertdata, req.params.audience);
  console.log({alert});
  if(!alert)
    throw new Error("Sorry, no alert");
  alert.set("audience",audience);
  await alert.save(null,{useMasterKey: true});

  const resps = [];
  console.log(await new Parse.Query("Alert").containedIn("dev2"));
  return {
    success: true,
    count: audience.length,
    alert: alert.id
  };
};
Parse.Cloud.define("sendAlert", sendAlert);
