const cell411 = import('cell411-mjs');

function checkUserName(username) {
  if(username==null || !username.length){
    return {
      success: false,
      message: "username must be non-mull"
    };
  } else {
    const parts = username.split("@");
    if(parts.length!=2){
      return {
        success: false,
        message: "username/email must be valid address"
      };
    }
  };
  return null;
}
function checkPassword(password) {
  if(password==null || !password.length){
    return {
      success: false,
      message: "password must be non-mull"
    };
  };
  return null;
}
function cellQuery(type,user) {
  const query1=new Parse.Query(type).equalTo("owner",user);
  const query2=new Parse.Query(type).equalTo("members",user);
  return Parse.Query.or(query1,query2);
}
function memberQuery(cells) {
  const queries=[];
  for(var i=0;i<cells;i++)
    queries.push(cells[i].relation("members").query());
  return Parse.Query.or(queries);
}

async function logIn(req) {
  try {
    console.log(await cell411);
    console.log(cell411);
    console.log(pp(cell411));
    process.exit(0);
    const params = req.params;
    const username=params.username;
    const password=params.password;
    const lastLoad=params.lastLoad;
    var check = checkUserName(username);
    if(!check)
      check=checkPassword(password);
    if(check) {
      return check;
    }
    var user = User.logIn(username,password);
    var data = loadData(user);
    return {
      success: true,
      data,
      user
    };
  } catch ( err ) {
    console.log(err);
    throw err;
  };
}
Parse.Cloud.define("logIn", logIn);
