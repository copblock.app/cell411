const startTime = new Date().getTime();
const list1 = fileList(-1);
const list2 = fileList(1);
function isJS(path) {
  if(path.endsWith(".js"))
    return 1;
  if(path.endsWith(".cjs"))
    return 1;
  if(path.endsWith(".mjs"))
    return 1;
  return 0;
}
export function dirPaths(dir,list,val){
  const cloud = fs.readdirSync(dir);
  for(var i=0;i<cloud.length;i++){
    const path=dir+"/"+cloud[i];
    if(isJS(path)){
      list.push(path);
    }
  }
};
export function fileList(val) {
  const list=[];
  dirPaths(".",list,val);
  dirPaths("cloud",list,val);
  dirPaths("cloud/lib",list,val);
  return list;
}
const nextPrint = 0;
export function checkFiles() {
  const now = new Date().getTime();
  const list2 = fileList(1);
  const diff =compare(list1,list2);
  for(var i=0;i<list2.length;i++){
    const file=list2[i];
    const stat=fs.statSync(file);
    if(stat.mtime.getTime()>startTime){
      diff[file]=diff[file]||"changed";
      continue;
    };
  };
  if(Object.keys(diff).length!=0) {
    console.log(pp(diff));
    process.exit(0);
  }
};
export function compare(list1,list2){
  const diff={};
  for(var i=0;i<list1.length;i++){
    diff[list1[i]]=(diff[list1[i]]||0)+1;
  };
  for(var i=0;i<list2.length;i++){
    diff[list2[i]]=(diff[list2[i]]||0)-1;
  };
  for(var key of Object.keys(diff)){
    if(diff[key]==0)
      delete(diff[key]);
    else if(diff[key]<0)
      diff[key]="added";
    else
      diff[key]="removed";
  };
  return diff;
}
setInterval(checkFiles,15000);
