#!/usr/bin/env node

console.log("starting");
global.fetch = (await import('node-fetch')).default;
global.util=(await import("util")).default;
global.inspect=util.inspect;
global.fs = (await import("fs")).default;
global.path = (await import("path")).default;
global.cell411 = (await import("cell411-mjs"));
global.cell411=cell411;
global.MD5 = (await import('md5.js')).default;
global.keys=Object.keys;
const ParseServer = (await import("parse-server")).default.ParseServer;
const express = (await import("express")).default;
global.config=loadConfig();
global.pp=cell411.pp;
global.findFully=cell411.findFully;
global.keys=cell411.keys;
const app = express();
const pwd=process.env.PWD;
const server=new ParseServer(config);
app.use(config.mountPath, server);
app.get('/resetPassword.html', function(req,res){
  res.status(200).send(
    `url: ${req.url} query: ${JSON.stringify(req.query,null,2)}\n\n`
  );
});
function defpage (req, res) {
  res.status(200).send(
    "<html><body>\n\tWho ya gonna call?  The cops might well shoot YOU!\n</body></html>"
  );
}
app.get('/index.html', defpage);
app.get('/', defpage);

const httpServer = (await import('http')).default.createServer(app);
httpServer.listen(config.port, "127.0.0.1", function () {
  console.log('cell411 server running on port ' + config.port); 
});
// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);

function loadConfig() {
  const flavor = process.env.PARSE_FLAVOR;
  if(flavor==null || flavor.length==0)
    die("missing flavor");
  const home=process.env.HOME;
  const env=process.env;
  const configFile = home+"/.parse/config-"+flavor+".json";
  const configText = fs.readFileSync(configFile).toString();
  const config = JSON.parse(configText);
  config || die("failed to read: "+configFile);
  const url = cell411.makeDatabaseURI(config.databaseCred);
  if (typeof url === 'string') {
    config.databaseURI = url;
  } else {
    throw new TypeError('Parameter "url" must be a string, not ' + typeof url);
  }
  config.fileUpload={};
  config.revokeSessionOnPasswordReset=false;
  config.masterKeyIps=[];

  config || die("failed to read: "+configFile);

  if(env['PARSE_SERVER_LOGS_FOLDER']){
    config.logsFolder=env['PROCESS_SERVER_LOGS_FOLDER'];
  } else if(Object.prototype.hasOwnProperty(config,"logFolder")) {
    env['PARSE_SERVER_LOGS_FOLDER']=config.logsFolder;
  };
  return config;
};
function die(msg) {
  console.error("FATAL ERROR: "+msg);
  process.exit(1);
};
(await import("./checkFiles.js")).default;
