"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EventEmitterPubSub = void 0;

var _events = _interopRequireDefault(require("events"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const emitter = new _events.default.EventEmitter();

class Publisher {
  constructor(emitter) {
    this.emitter = emitter;
  }

  publish(channel, message) {
    this.emitter.emit(channel, message);
  }

}

class Subscriber extends _events.default.EventEmitter {
  constructor(emitter) {
    super();
    this.emitter = emitter;
    this.subscriptions = new Map();
  }

  subscribe(channel) {
    const handler = message => {
      this.emit('message', channel, message);
    };

    this.subscriptions.set(channel, handler);
    this.emitter.on(channel, handler);
  }

  unsubscribe(channel) {
    if (!this.subscriptions.has(channel)) {
      return;
    }

    this.emitter.removeListener(channel, this.subscriptions.get(channel));
    this.subscriptions.delete(channel);
  }

}

function createPublisher() {
  return new Publisher(emitter);
}

function createSubscriber() {
  // createSubscriber is called once at live query server start
  // to avoid max listeners warning, we should clean up the event emitter
  // each time this function is called
  emitter.removeAllListeners();
  return new Subscriber(emitter);
}

const EventEmitterPubSub = {
  createPublisher,
  createSubscriber
};
exports.EventEmitterPubSub = EventEmitterPubSub;