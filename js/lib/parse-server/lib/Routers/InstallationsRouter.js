"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.InstallationsRouter = void 0;

var _ClassesRouter = _interopRequireDefault(require("./ClassesRouter"));

var _rest = _interopRequireDefault(require("../rest"));

var _middlewares = require("../middlewares");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// InstallationsRouter.js
class InstallationsRouter extends _ClassesRouter.default {
  className() {
    return '_Installation';
  }

  handleFind(req) {
    const body = Object.assign(req.body, _ClassesRouter.default.JSONFromQuery(req.query));

    const options = _ClassesRouter.default.optionsFromBody(body);

    return _rest.default.find(req.config, req.auth, '_Installation', body.where, options, req.info.clientSDK, req.info.context).then(response => {
      return {
        response: response
      };
    });
  }

  mountRoutes() {
    this.route('GET', '/installations', req => {
      return this.handleFind(req);
    });
    this.route('GET', '/installations/:objectId', req => {
      return this.handleGet(req);
    });
    this.route('POST', '/installations', _middlewares.promiseEnsureIdempotency, req => {
      return this.handleCreate(req);
    });
    this.route('PUT', '/installations/:objectId', _middlewares.promiseEnsureIdempotency, req => {
      return this.handleUpdate(req);
    });
    this.route('DELETE', '/installations/:objectId', req => {
      return this.handleDelete(req);
    });
  }

}

exports.InstallationsRouter = InstallationsRouter;
var _default = InstallationsRouter;
exports.default = _default;