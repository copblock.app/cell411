"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.internalUpdateSchema = exports.internalCreateSchema = exports.SchemasRouter = void 0;

var _PromiseRouter = _interopRequireDefault(require("../PromiseRouter"));

var middleware = _interopRequireWildcard(require("../middlewares"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// schemas.js
var Parse = require('parse/node').Parse,
    SchemaController = require('../Controllers/SchemaController');

function classNameMismatchResponse(bodyClass, pathClass) {
  throw new Parse.Error(Parse.Error.INVALID_CLASS_NAME, `Class name mismatch between ${bodyClass} and ${pathClass}.`);
}

function getAllSchemas(req) {
  return req.config.database.loadSchema({
    clearCache: true
  }).then(schemaController => schemaController.getAllClasses({
    clearCache: true
  })).then(schemas => ({
    response: {
      results: schemas
    }
  }));
}

function getOneSchema(req) {
  const className = req.params.className;
  return req.config.database.loadSchema({
    clearCache: true
  }).then(schemaController => schemaController.getOneSchema(className, true)).then(schema => ({
    response: schema
  })).catch(error => {
    if (error === undefined) {
      throw new Parse.Error(Parse.Error.INVALID_CLASS_NAME, `Class ${className} does not exist.`);
    } else {
      throw new Parse.Error(Parse.Error.INTERNAL_SERVER_ERROR, 'Database adapter error.');
    }
  });
}

const checkIfDefinedSchemasIsUsed = req => {
  var _req$config, _req$config$schema;

  if (((_req$config = req.config) === null || _req$config === void 0 ? void 0 : (_req$config$schema = _req$config.schema) === null || _req$config$schema === void 0 ? void 0 : _req$config$schema.lockSchemas) === true) {
    throw new Parse.Error(Parse.Error.OPERATION_FORBIDDEN, 'Cannot perform this operation when schemas options is used.');
  }
};

const internalCreateSchema = async (className, body, config) => {
  const controller = await config.database.loadSchema({
    clearCache: true
  });
  const response = await controller.addClassIfNotExists(className, body.fields, body.classLevelPermissions, body.indexes);
  return {
    response
  };
};

exports.internalCreateSchema = internalCreateSchema;

const internalUpdateSchema = async (className, body, config) => {
  const controller = await config.database.loadSchema({
    clearCache: true
  });
  const response = await controller.updateClass(className, body.fields || {}, body.classLevelPermissions, body.indexes, config.database);
  return {
    response
  };
};

exports.internalUpdateSchema = internalUpdateSchema;

async function createSchema(req) {
  checkIfDefinedSchemasIsUsed(req);

  if (req.auth.isReadOnly) {
    throw new Parse.Error(Parse.Error.OPERATION_FORBIDDEN, "read-only masterKey isn't allowed to create a schema.");
  }

  if (req.params.className && req.body.className) {
    if (req.params.className != req.body.className) {
      return classNameMismatchResponse(req.body.className, req.params.className);
    }
  }

  const className = req.params.className || req.body.className;

  if (!className) {
    throw new Parse.Error(135, `POST ${req.path} needs a class name.`);
  }

  return await internalCreateSchema(className, req.body, req.config);
}

function modifySchema(req) {
  checkIfDefinedSchemasIsUsed(req);

  if (req.auth.isReadOnly) {
    throw new Parse.Error(Parse.Error.OPERATION_FORBIDDEN, "read-only masterKey isn't allowed to update a schema.");
  }

  if (req.body.className && req.body.className != req.params.className) {
    return classNameMismatchResponse(req.body.className, req.params.className);
  }

  const className = req.params.className;
  return internalUpdateSchema(className, req.body, req.config);
}

const deleteSchema = req => {
  if (req.auth.isReadOnly) {
    throw new Parse.Error(Parse.Error.OPERATION_FORBIDDEN, "read-only masterKey isn't allowed to delete a schema.");
  }

  if (!SchemaController.classNameIsValid(req.params.className)) {
    throw new Parse.Error(Parse.Error.INVALID_CLASS_NAME, SchemaController.invalidClassNameMessage(req.params.className));
  }

  return req.config.database.deleteSchema(req.params.className).then(() => ({
    response: {}
  }));
};

class SchemasRouter extends _PromiseRouter.default {
  mountRoutes() {
    this.route('GET', '/schemas', middleware.promiseEnforceMasterKeyAccess, getAllSchemas);
    this.route('GET', '/schemas/:className', middleware.promiseEnforceMasterKeyAccess, getOneSchema);
    this.route('POST', '/schemas', middleware.promiseEnforceMasterKeyAccess, createSchema);
    this.route('POST', '/schemas/:className', middleware.promiseEnforceMasterKeyAccess, createSchema);
    this.route('PUT', '/schemas/:className', middleware.promiseEnforceMasterKeyAccess, modifySchema);
    this.route('DELETE', '/schemas/:className', middleware.promiseEnforceMasterKeyAccess, deleteSchema);
  }

}

exports.SchemasRouter = SchemasRouter;