"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ParseCloudCodePublisher = void 0;

var _ParsePubSub = require("./ParsePubSub");

var _node = _interopRequireDefault(require("parse/node"));

var _logger = _interopRequireDefault(require("../logger"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class ParseCloudCodePublisher {
  // config object of the publisher, right now it only contains the redisURL,
  // but we may extend it later.
  constructor(config = {}) {
    this.parsePublisher = _ParsePubSub.ParsePubSub.createPublisher(config);
  }

  onCloudCodeAfterSave(request) {
    console.log("onCloudCodeAfterSave(", request, ")");

    this._onCloudCodeMessage(_node.default.applicationId + 'afterSave', request);
  }

  onCloudCodeAfterDelete(request) {
    console.log("onCloudCodeAfterDelete(", request, ")");

    this._onCloudCodeMessage(_node.default.applicationId + 'afterDelete', request);
  } // Request is the request object from cloud code functions. request.object is a ParseObject.


  _onCloudCodeMessage(type, request) {
    _logger.default.verbose('Raw request from cloud code current : %j | original : %j', request.object, request.original); // We need the full JSON which includes className


    const message = {
      currentParseObject: request.object._toFullJSON()
    };

    if (request.original) {
      message.originalParseObject = request.original._toFullJSON();
    }

    this.parsePublisher.publish(type, JSON.stringify(message)); //console.log("Published: ", JSON.stringify(message,null,2));
  }

}

exports.ParseCloudCodePublisher = ParseCloudCodePublisher;