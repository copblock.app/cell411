"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.AppCache = void 0;

var _InMemoryCache = require("./Adapters/Cache/InMemoryCache");

var AppCache = new _InMemoryCache.InMemoryCache({
  ttl: NaN
});
exports.AppCache = AppCache;
var _default = AppCache;
exports.default = _default;