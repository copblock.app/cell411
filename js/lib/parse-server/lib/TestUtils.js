"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.destroyAllDataPermanently = destroyAllDataPermanently;

var _cache = _interopRequireDefault(require("./cache"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Destroys all data in the database
 * @param {boolean} fast set to true if it's ok to just drop objects and not indexes.
 */
function destroyAllDataPermanently(fast) {
  if (!process.env.TESTING) {
    throw 'Only supported in test environment';
  }

  return Promise.all(Object.keys(_cache.default.cache).map(appId => {
    const app = _cache.default.get(appId);

    if (app.databaseController) {
      return app.databaseController.deleteEverything(fast);
    } else {
      return Promise.resolve();
    }
  }));
}