"use strict";

var _parseLiveQueryServer = _interopRequireDefault(require("./definitions/parse-live-query-server"));

var _runner = _interopRequireDefault(require("./utils/runner"));

var _index = require("../index");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _runner.default)({
  definitions: _parseLiveQueryServer.default,
  start: function (program, options, logOptions) {
    logOptions();

    _index.ParseServer.createLiveQueryServer(undefined, options);
  }
});