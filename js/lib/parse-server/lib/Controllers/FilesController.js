"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.FilesController = void 0;

var _cryptoUtils = require("../cryptoUtils");

var _AdaptableController = _interopRequireDefault(require("./AdaptableController"));

var _FilesAdapter = require("../Adapters/Files/FilesAdapter");

var _path = _interopRequireDefault(require("path"));

var _mime = _interopRequireDefault(require("mime"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// FilesController.js
const Parse = require('parse').Parse;

const legacyFilesRegex = new RegExp('^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}-.*');

class FilesController extends _AdaptableController.default {
  getFileData(config, filename) {
    return this.adapter.getFileData(filename);
  }

  createFile(config, filename, data, contentType, options) {
    const extname = _path.default.extname(filename);

    const hasExtension = extname.length > 0;

    if (!hasExtension && contentType && _mime.default.getExtension(contentType)) {
      filename = filename + '.' + _mime.default.getExtension(contentType);
    } else if (hasExtension && !contentType) {
      contentType = _mime.default.getType(filename);
    }

    if (!this.options.preserveFileName) {
      filename = (0, _cryptoUtils.randomHexString)(32) + '_' + filename;
    }

    const location = this.adapter.getFileLocation(config, filename);
    return this.adapter.createFile(filename, data, contentType, options).then(() => {
      return Promise.resolve({
        url: location,
        name: filename
      });
    });
  }

  deleteFile(config, filename) {
    return this.adapter.deleteFile(filename);
  }

  getMetadata(filename) {
    if (typeof this.adapter.getMetadata === 'function') {
      return this.adapter.getMetadata(filename);
    }

    return Promise.resolve({});
  }
  /**
   * Find file references in REST-format object and adds the url key
   * with the current mount point and app id.
   * Object may be a single object or list of REST-format objects.
   */


  expandFilesInObject(config, object) {
    if (object instanceof Array) {
      object.map(obj => this.expandFilesInObject(config, obj));
      return;
    }

    if (typeof object !== 'object') {
      return;
    }

    for (const key in object) {
      const fileObject = object[key];

      if (fileObject && fileObject['__type'] === 'File') {
        if (fileObject['url']) {
          continue;
        }

        const filename = fileObject['name']; // all filenames starting with "tfss-" should be from files.parsetfss.com
        // all filenames starting with a "-" seperated UUID should be from files.parse.com
        // all other filenames have been migrated or created from Parse Server

        if (config.fileKey === undefined) {
          fileObject['url'] = this.adapter.getFileLocation(config, filename);
        } else {
          if (filename.indexOf('tfss-') === 0) {
            fileObject['url'] = 'http://files.parsetfss.com/' + config.fileKey + '/' + encodeURIComponent(filename);
          } else if (legacyFilesRegex.test(filename)) {
            fileObject['url'] = 'http://files.parse.com/' + config.fileKey + '/' + encodeURIComponent(filename);
          } else {
            fileObject['url'] = this.adapter.getFileLocation(config, filename);
          }
        }
      }
    }
  }

  expectedAdapterType() {
    return _FilesAdapter.FilesAdapter;
  }

  handleFileStream(config, filename, req, res, contentType) {
    return this.adapter.handleFileStream(filename, req, res, contentType);
  }

  validateFilename(filename) {
    if (typeof this.adapter.validateFilename === 'function') {
      const error = this.adapter.validateFilename(filename);

      if (typeof error !== 'string') {
        return error;
      }

      return new Parse.Error(Parse.Error.INVALID_FILE_NAME, error);
    }

    return (0, _FilesAdapter.validateFilename)(filename);
  }

}

exports.FilesController = FilesController;
var _default = FilesController;
exports.default = _default;