global.fs = await import("fs");
global.util = await import("util");
global.inspector = util.inspector;
global.parseUsername=null;
global.parsePassword=null;
global.parseFlavor=process.env.PARSE_FLAVOR;

const Parse = (await import("parse")).default;


export function keys(obj) {
  if(obj == null) {
    return "obj id null";
  } else if ( Array.isArray(obj) ) {
    return "obj is array";
  } else {
    return Object.keys(obj);
  }
};
export const cell411={};
cell411.keys=keys;
cell411.Parse=Parse
global.Parse=Parse
const parseKeys = cell411.keys(Parse); 
export function makeDatabaseURI(cred) {
  return `postgres://${cred.user}:${cred.password}@${cred.host}/${cred.database}`;
};
cell411.makeDatabaseURI=makeDatabaseURI;
export function pp(obj) {
  return JSON.stringify(obj,null,2);
};
cell411.pp=pp;
export function isNoE(str) {
  return str==null || str.length==0;
};
cell411.isNoE=isNoE;

cell411.configFile=cell411.configFile;
cell411.parseFlavor=process.env.PARSE_FLAVOR;
cell411.homeDir=process.env.HOME;
cell411.configDir=`${cell411.homeDir}/.parse`;
cell411.configFile=`${cell411.configDir}/config-${cell411.parseFlavor}.json`;


export async function loadParseConfig(){
  if(cell411.parseConfig)
    return cell411.parseConfig;
  const configFile=cell411.configFile;
  const parseText = fs.readFileSync(cell411.configFile);
  cell411.parseConfig =JSON.parse(parseText);
  const cred = cell411.parseConfig.databaseCred;
  const url = makeDatabaseURI(cred);
  cell411.parseConfig.databaseURI=url;
  return cell411.parseConfig;
};
cell411.loadParseConfig=loadParseConfig;
export async function initializeParse() {
  cell411.parseConfig=await loadParseConfig();
  Parse.serverURL=cell411.parseConfig.serverURL; 
  await Parse.initialize(
    cell411.parseConfig.appId,
    cell411.parseConfig.javascriptKey,
    cell411.parseConfig.masterKey
  );
};
cell411.initializeParse=initializeParse;
const sessionTokenFile=process.env.HOME+"/.parse/sessionTokens.json";
export async function loadSessionTokens()
{
  if(!fs.existsSync(sessionTokenFile))
    return {};
  const text = fs.readFileSync(sessionTokenFile).toString();
  return JSON.parse(text);
}
export async function getSessionToken(username){
  const sessionTokens=await loadSessionTokens();
  const section=sessionTokens[parseFlavor];
  if(!sessionTokens[parseFlavor])
    return null;
  return section[username];
}
export async function saveSessionToken(username,token){
  const sessionTokens=await loadSessionTokens();
  if(!sessionTokens[parseFlavor])
    sessionTokens[parseFlavor]={};
  const section=sessionTokens[parseFlavor];
  if(token==null){
    delete section[username];
  } else {
    section[username]=token;
  }
  const text = JSON.stringify(sessionTokens,null,2);
  fs.writeFileSync(sessionTokenFile,text+"\n\n");
}
export function readJson(file){
  const text=fs.readFileSync(file);
  return JSON.parse(text);
};
cell411.readJson=readJson;
var rli=rli;
var user;
export async function parseLogin() {
  await initializeParse();
  if(parseUsername==null)
    parseUsername=process.env.PARSE_USER;
  if(parseUsername==null){
    console.log("no username.  set PARSE_USER");
    return null;
  }
  if(!parseUsername.match('@'))
    parseUsername=parseUsername+"@copblock.app";
  var sessionToken=await getSessionToken(parseUsername);
  if(sessionToken==null){
    console.log("no session token");
    if(parsePassword==null)
      parsePassword=process.env.PARSE_PASS;
    if(parseUsername==null){
      console.log("no password eitheer.  set PARSE_PASS");
      return null;
    }
    user = new Parse.User();
    user.set("username",parseUsername);
    user.set("password",parsePassword);
    await user.logIn({useMasterKey: true});
    sessionToken=await user.getSessionToken();
    if(user!=null && user.get("sessionToken")!=null)
      saveSessionToken(user.get("username"),user.get("sessionToken"));
  } else {
    console.log("logged in with token");
    user = await Parse.User.me(sessionToken);
  }
  return user;
}
cell411.parseLogin=parseLogin;
export async function wait(delay) {
  return new Promise((resolve,reject)=>{
    setTimeout(()=>{resolve("done")},delay);
  });
}
cell411.wait=wait;

export async function done() {
  const queue=await Parse.EventuallyQueue.length();
  if(queue){
    await Parse.EventuallyQueue.poll(1);
    console.log("Eventually Queue Run");
  } else {
    await Parse.EventuallyQueue.stopPoll();
    console.log("Eventually Queue Stopped");
  }
}
cell411.done=done;
cell411.parseLogin=cell411.parseLogin;
export async function findFully(query){
  const res=[];
  const limit=1000;
  query.limit(limit);
  while(true){
    query.skip(res.length);
    const batch=await query.find();
    for(var i=0;i<batch.length;i++)
      res.push(batch[i]);
    if(batch.length<limit)
      break;
  };
  return res;
};
export default cell411;
cell411.pp=pp;
cell411.findFully=findFully;
global.findFully=findFully;
global.User = Parse.Object.extend(Parse.User, {
    getName: function getName() {
      return this.get("firstName")+" "+this.get("lastName");
    },
    findFriends: function findFriends() {
      console.trace("findFriends");
    }
  });

global.Request=Parse.Object.extend("Request");
Parse.Object.registerSubclass("Request",Request);
global.Response=Parse.Object.extend("Response");
Parse.Object.registerSubclass("Response",Response);
global.Alert=Parse.Object.extend("Alert");
Parse.Object.registerSubclass("Alert",Alert);
global.PrivacyPolicy=Parse.Object.extend("PrivacyPolicy");
Parse.Object.registerSubclass("PrivacyPolicy",PrivacyPolicy);
global.PublicCell=Parse.Object.extend("PublicCell");
Parse.Object.registerSubclass("PublicCell",PublicCell);
global.PrivateCell=Parse.Object.extend("PrivateCell");
Parse.Object.registerSubclass("PrivateCell",PrivateCell);
global.Role=Parse.Object.extend("_Role");
Parse.Object.registerSubclass("__Role",Role);
global.ChatMsg=Parse.Object.extend("ChatMsg");
Parse.Object.registerSubclass("ChatMsg",ChatMsg);
global.ChatRoom=Parse.Object.extend("ChatRoom");
Parse.Object.registerSubclass("ChatRoom",ChatRoom);
global.Friendship=Parse.Object.extend("Friendship");
Parse.Object.registerSubclass("Friendship",Friendship);


Request.prototype.toString=function toString(){
  return "Request{"+this.id+"}";
};
User.prototype.toString=function toString(){
  return "User{"+this.id+","+this.get("firstName")+" "+this.get("lastName")+"}";
};
User.prototype.getName=function getName() {
  return this.get("firstName")+" "+this.get("lastName");
};
PublicCell.prototype.toString=function toString(){
  return "PublicCell{"+this.id+","+this.get("name")+"}";
};
PrivateCell.prototype.toString=function toString(){
  return "PrivateCell("+this.id+","+this.get("name")+"}";
};

const db = {
  initOptions : {
  },
};
db.setup=async function setup() {
  await loadParseConfig();
  db.pgpx=(await import('pg-promise')).default;
  db.pgp=pgpx(initOptions);
  db.pgm=(await import('pg-monitor')).default;
  db.pgm.attach(initOptions);
  db.pgs=(await import('pg-connection-string')).default;
  db.cn = pgs.parse(parseConfig.databaseURI);
  db = pgp(cn); // database instance;
}
