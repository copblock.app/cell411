await import('cell411-mjs');
async function main() {
  await initializeParse();
  const user = await parseLogin();
  const sessionToken=user.getSessionToken();
  const params={};
  params.start = new Date().getTime();
  params.start -= 86400*1000*365*10;
  const rows = await  Parse.Cloud.run("relations2",params,{sessionToken});
  console.log(new Date(rows.start));
}
main()
