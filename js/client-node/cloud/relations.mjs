//console.log=console.trace;
import { cell411 } from 'cell411-mjs';
import { execSync } from 'child_process';
const pp=cell411.pp;
let dates={};

async function main() {
  const obj = await cell411.initializeParse();
  const user = await cell411.parseLogin();
  const sessionToken=user.getSessionToken();
  const params = {};
  params.dates=dates;
  let rows = await  Parse.Cloud.run("relations",params,{sessionToken});
  console.error("relations1.json:1:created");
  fs.writeFileSync("relations1.json",pp(rows));
  params.dates=rows.dates;
  rows = await  Parse.Cloud.run("relations",params,{sessionToken});
  console.error("relations2.json:1:created");
  fs.writeFileSync("relations2.json",pp(rows));
  execSync("wc -l relations*.json", { stdio: 'inherit' });
  execSync("du -k relations*.json", { stdio: 'inherit' });
}
main()
