
export async function loadData(user) {
  const qs={};
  qs.friends = user.relation("friends").query();
  // the members will all be include in the friends
  // query, thats what private cells are.
  qs.privateCells = cellQuery("PrivateCell",user);
  // These members you need, tho.  You get anyone who is
  // in any group with you.  FIXME!  We need to limit the
  // columns returned.
  qs.publicCells = cellQuery("PublicCell",user);

  const data = {};
  data.privateCells= await findFully(qs.privateCells);
  data.publicCells= await findFully(qs.publicCells);

  qs.publicMembers=new Parse.Query("_User");
  qs.members = memberQuery(data.publicCells);
  qs.users=Parse.Query.or(qs.friends,qs.members);
  data.users = await findFully(qs.users);

  // And that is how you grab all the shit related to a
  // dude in one fell swoop.  Actually, I still need to add
  // any pending events .. Requests and Alerts.

  console.log(`${username} downloaded:"\n`+
    `   ${data.users.length} users,\n`
    `   ${data.privateCells.length} privateCells,\n`
    `and\n`+
    `   ${data.publicCells.length} privateCells,\n`);

  return data;

};
