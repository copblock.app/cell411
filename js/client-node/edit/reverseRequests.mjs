import cell411 from 'cell411-mjs';
const mkey={useMasterKey: true};
async function main() {
  await cell411.initializeParse();
  const query = Parse.Query.or(
    new Parse.Query("Request").equalTo("owner","dev1"),
    new Parse.Query("Request").equalTo("sentTo","dev1")
  );
  const reqs = await findFully(query);
  var out=0;
  for(var i=0;i<reqs.length;i++) {
    const req=reqs[i];
    if(req.get("owner").id === "dev1"){
      out++;
    }
  }
  console.log("fraction: "+out);
  for(var i=0;i<reqs.length;i++) {
    const req=reqs[i];
    const owner=req.get("owner");
    const sentTo=req.get("sentTo");
    if(owner.id === "dev1" && out > reqs.length/2) {
      req.set("owner",sentTo);
      req.set("sentTo",owner);
      req.save(null,mkey);
      out-=2;
    } else if ( owner.id !== "dev1" && out < reqs.length/2 ) {
      req.set("owner",sentTo);
      req.set("sentTo",owner);
      req.save(null,mkey);
      out+=2;
    }
  }
}
main()
