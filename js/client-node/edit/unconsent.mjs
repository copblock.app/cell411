import cell411 from 'cell411-mjs';
const mkey={useMasterKey: true};
async function main() {
  await cell411.initializeParse();
  const query = new Parse.Query("_User");
  query.endsWith("username","copblock.app");
  const users = await findFully(query);
  for(var i=0;i<users.length;i++){
    const consented = users[i].get("consented");
    console.log(consented);
    users[i].set("consented",!consented);
    users[i].save(null,mkey);
  }
}
main()
