#!node
import cell411 from 'cell411-mjs';
const pp = cell411.pp;

await cell411.initializeParse();
import fs from 'fs';

const dir = "json";
const schemas = fs.readdirSync(dir);
for(var i=0;i<schemas.length;i++) {
  const path=dir+"/"+schemas[i];
  if(!path.endsWith(".json"))
    continue;
  const text = fs.readFileSync(path).toString();
  const json = JSON.parse(text);
  console.log({path,json});
};
