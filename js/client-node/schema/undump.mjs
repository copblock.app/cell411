#!node
import cell411 from 'cell411-mjs';
import { existsSync } from 'fs';
import readline from 'readline-sync';
global.keys=Object.keys;
const Schema = Parse.Schema;

await cell411.initializeParse();
const schemaList = [];
const schemas = {};
await files();
console.log(schemaList);
if( !readline.keyInYN("process theese files? ")) {
  console.log("aborting.");
  process.exit(1);
};
await readFiles();
console.log(schemas);
await processSchemas();
process.exit(0);

async function readFiles() {
  const skips = {
    ACL: 1, createdAt: 1, updatedAt: 1, objectId: 1,
    "_Session": 1, "_Installation": 1
  };
  for(const path of schemaList) { 
    const text = fs.readFileSync(path).toString();
    const json = JSON.parse(text);
    const className = json.className;
    for(const skip of keys(skips)) {
      delete json.fields[skip];
    };
    schemas[className]=json.fields;
  };
};

async function processSchemas() {
  for(var name in schemas) {
    const schema = new Schema(name);
    const fields = schemas[name];
    for(var field in fields) {
      const data=fields[field];
      const type=data['type'];
      delete data['type'];
      schema.addField(field,type,data);
    }
    try {
      await schema.save();
    } catch ( err ) {
      await schema.update();
    }
  }
}
async function files() {
  if(process.argv.length>2) {
    for(var i=2;i<process.argv.length;i++){
      const file=process.argv[i];
      if(file.endsWith('.json')) {
        schemaList.push(file);
      } else {
        console.error(`file ${file} does not end in json`);
        process.exit(2);
      }
    }
  } else {
    const dir = "json";
    const list = fs.readdirSync(dir);
    for(var i=0;i<list.length;i++){
      if(list[i].endsWith(".json")) {
        schemaList.push([dir,list[i]].join("/"));
      } 
    };
  }
  for(var file of schemaList) {
    if(!existsSync(file)){
      console.error(`file does not exist: ${file}`);
      process.exit(1);
    };
  };
}
