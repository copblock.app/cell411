import fs from 'fs';
import 'cell411-mjs';
import 'moment';
async function load_fetch() {
  var module = await import('node-fetch');
  global.fetch=module.default;
};
const tok={};
await load_fetch();
const opts ={};
async function reverseGeocode(location,type) {
  if(location==null)
    return null;
  return Parse.Cloud.run("reverseGeocode",{location,type},tok);
};
//async function geocode(address,type) {
//  if(address==null)
//    return null;
//    var params = {address,type};
//  return Parse.Cloud.run("geocode",params,tok);
//};
// export async function iParse() {
//   if(parseInitialized)
//     return;
//   parseConfig=await loadParseConfig();
//   Parse.publicServerURL=parseConfig.serverURL; 
//   console.log({serverUrl: Parse.serverURL});
//   await Parse.initialize(
//     parseConfig.appId,
//     parseConfig.javascriptKey,
//     parseConfig.masterKey
//   );
//   return;
// };
async function main() {
  await initializeParse();
  var param = {
    "location" : {
      "__type" : "GeoPoint",
      "latitude" : 37.4226711,
      "longitude" : -122.0849872
    },
    "type" : "Address"
  };
  console.log(JSON.stringify(Parse.Cloud.run("reverseGeocode",param,tok)));
}
await main();
