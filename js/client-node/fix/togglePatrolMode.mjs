await import('cell411-mjs');
const mkey={useMasterKey: true};
async function main(args) {
  await initializeParse();
  const query = new Parse.Query("_User");
  query.containedIn("objectId",args);
  const users = await findFully(query);
  const passwd = process.env.PARSE_PASSWORD || "asdf";
  for(var i=0;i<users.length;i++){
    const patrolMode = users[i].get("patrolMode");
    console.log(patrolMode);
    users[i].set("patrolMode",!patrolMode);
    users[i].save(null,mkey);
  }
}
const args=[];
for(var i=2;i<process.argv.length;i++){
  args.push(process.argv[i]);
}
main(args)
