await import('cell411');
async function main() {
  await initializeParse();
  const user = await parseLogin();
  const sessionToken=user.getSessionToken();
  const rows = await  Parse.Cloud.run("relations",null,{sessionToken});
  const dates = rows.shift();
  for(var i=0;i<rows.length;i++){
    const row=rows[i];
    row.splice(0,6);
    for(var j=0;j<row.length;j++){
      const id=row[j];
      const date=dates[id];
      console.log({id,date});
    };
  };
}
main()
