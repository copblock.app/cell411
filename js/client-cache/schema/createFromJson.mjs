#!node
import 'cell411-mjs';
import { pp } from 'cell411-utils';

await initializeParse();
import fs from 'fs';

var names={};
const dir="../json-schema"; 
var oldSchemas = {};

var list = await Parse.Schema.all();
for(var i=0;i<list.length;i++){
  const name = list[i].className;
  oldSchemas[name]=list[i];
  names[name]=1;
};
const newSchemas = {};
list = fs.readdirSync(dir);
for(var i=0;i<list.length;i++){
  if(list[i] === "constraints.json")
    continue;
  const text = fs.readFileSync(dir+"/"+list[i]);
  const obj = JSON.parse(text);
  const name=obj.className;
  newSchemas[name]=obj;
  names[name]=1;
}
names=Object.keys(names);
const skip = {
  ACL: 1,
  objectId: 1,
  createdAt: 1,
  updatedAt: 1,
};
for(var i=0;i<names.length;i++){
  const name = names[i];
  if(oldSchemas[name]==null){
    oldSchemas[name]=new Parse.Schema(name);
    console.log("creating: "+name);
    oldSchemas[name].save();
  };
};
for(var i=0;i<names.length;i++){
  const name = names[i];
  console.log(name);
  const oObj = oldSchemas[name];
  const nObj = newSchemas[name];
  console.log({oObj,nObj});
  console.log("creating "+name);
  const obj = new Parse.Schema(name);
  const fields = nObj.fields;
  const fns = Object.keys(fields);
  for(var j=0;j<fns.length;j++) {
    const name=fns[j];
    if(oObj.fields[name]!=null){
      continue;
    };
    const field=fields[name];
    obj.addField(name,field.type,field);
  }
  obj.update();
  console.log(obj.className+" saved");
};
