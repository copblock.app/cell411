console.log('started');
import repl from 'repl';
import cell411 from 'cell411-mjs';
console.log(Object.keys(cell411));

const replConfig = {
  prompt: '> ',
  useColors: true,
  ignoreUndefined: true,
  preview: true
};
const replServer = repl.start(replConfig);
replServer.setupHistory(process.env.HOME+'/.parse/cli-history', ()=>{});
replServer.defineCommand('login', {
  help: 'logins you in',
  action(name) {
    this.clearBufferedCommand();
    console.log(`Hello, ${name}!`);
    this.displayPrompt();
  }
});
