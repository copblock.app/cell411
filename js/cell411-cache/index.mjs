#!/usr/bin/env node
import { findFully } from 'cell411-mjs';
import { cell411 } from 'cell411-mjs';
import { ParseServer } from 'parse-server';
async function require(mod) {
  const obj = await import(mod);
  //console.log(obj);
  return obj.default;
};
const express = await require("express");

import 'fs';
import 'path';

async function setup() {
//  console.log(setup);
//  global.res={};
//  const cell411_mod = await require("cell411-mjs");
//  global.ParseServer = (await require("parse-server")).ParseServer;
//  const express = await require("express");
//  const fs = await require("fs");
//  const path = await require("path");
//  res["cell411"]=cell411;
//  return 1;
}
function jsFile(name){
  if(name.endsWith(".js")){
    return true;
  } else if ( name.endsWith(".mjs") ) {
    return true;
  } else if ( name.endsWith(".cjs") ) {
    return true;
  } else {
    return false;
  }
}

async function queryAddress() {
  const query = new Parse.Query("address");
  query.equalTo("city","ann arbor");
  const res = await findFully(query);
  console.log(pp({query,res}));
};
async function queryCity() {
  const query = new Parse.Query("city");
  query.equalTo("objectId", "GFpJ732yHm");
  const res = await findFully(query);
  console.log(pp({query,res}));
};
async function main() {
  console.log(main);
  global.config1=await loadConfig();
  global.config2=await cell411.loadParseConfig();
  console.log({config1});
  console.log({config2});
  await setupExpress();
  //await makeAddress();
  //await queryAddress();
  //await makeCity();
  //await queryCity();


  const startTime=new Date().getTime();
  setTimeout(checkFiles,5000);
  function checkFiles() {
    const list = [ './index.mjs', './cloud/main.cjs' ];
    for(var i=0;i<list.length;i++){
      const file=list[i];
      if(!jsFile(file))
        continue;
      const stat=fs.statSync(file);
      if(stat.mtime.getTime()>startTime){
        console.warn("Exiting due to cloud code update");
        process.exit(0);
      };
    };
    setTimeout(checkFiles,5000);
  };
}
async function addressHandler(req,res) {
  const query = new Parse.Query("address");
  const address = await findFully(query);
  const json=JSON.stringify(address,null,2);
  console.log(json);
  return res.status(200).send(json);
};
async function cityHandler(req,res) {
  const query = new Parse.Query("city");
  const city = await findFully(query);
  const json=JSON.stringify(city,null,2);
  console.log(json);
  return res.status(200).send(json);
};
async function reverseGeocodeHandler(req, res) {
  try {
    const query=req.query;
    const request={};
    request.params=query;
    const result=await reverseGeocode(request);
    return res.status(200).send( JSON.stringify(result,null,2) );
  } catch ( err ) {
    console.error(err);
    return res.status(400).send( err );
  };
};
async function geocodeHandler(req, res) {
  var dump="";
  try {
    const query=req.query;
    const request={};
    request.params=query;
    const result=await geocode(request);
    return res.status(200).send( pp(result) );
  } catch ( err ) {
    err=pp(err);
    console.error(err);
    return res.status(400).send( pp({err,dump}) );
  };
};
async function die(msg) {
  console.error(msg);
  process.exit(1);
};
async function loadConfig() {
  console.log(loadConfig);
  const home=process.env.HOME;
  const flavor=process.env.PARSE_FLAVOR;
  const configFile = home+"/.parse/config-"+flavor+".json";
  const configText = fs.readFileSync(configFile)
    || die("failed to read: "+configFile);
  const config = JSON.parse(configText);
  const env=process.env;

  const url = cell411.makeDatabaseURI(config.databaseCred);
  if (typeof url !== 'string') {
    throw new TypeError('Parameter "url" must be a string, not ' + typeof url);
  }
  if(config) 

  if(env['PARSE_SERVER_LOGS_FOLDER']){
    console.log("overriding conf log folder from environment");
    config.logsFolder=env['PROCESS_SERVER_LOGS_FOLDER'];
  } else if(Object.prototype.hasOwnProperty(config,"logFolder")) {
    env['PARSE_SERVER_LOGS_FOLDER']=config.logsFolder;
  };

  return config;
};
function dumpObjInt(obj) {
  const copy={};
  const keys=Object.keys(obj);
  for(var i=0;i<keys.length;i++) {
    const key=keys[i];
    var text;
    try {
      text=dumpObj(obj[key]);
    } catch ( err ) {
      text=JSON.stringify(err);
    };
    copy[key]=text;
  }
  return copy;
}
function dumpObj(obj) {
  return pp(dumpObjInt(obj));
}
function defpage (req, res) {
  res.status(200).send(
    "<html><body>\n\tcell411-cache\n</body></html>"
  );
}
async function setupExpress() {
  console.log(setupExpress);
  const app = express();
  const config=cell411.parseConfig;
  //console.log(pp(config));
  app.use(config.mountPath, new ParseServer(config));
  app.get('/index.html', defpage);
  app.get('/', defpage);
  app.get('/city', cityHandler);
  app.get('/address', addressHandler);
  app.get('/reverseGeocode', reverseGeocodeHandler);
  app.get('/geocode', geocodeHandler);
  const httpServer = (await require('http')).createServer(app);
  httpServer.listen(config.port, "127.0.0.1", function () {
    console.log('geocache server running on port ' + 
      config.port + '.');
  });
};
setup().then(main).then((res)=>{console.log("main returned");})
  .catch((err)=>{console.error("error:",err)});

//  Ann Arbor is a city located in southeastern Michigan, USA, and is
//  the sixth-largest city in the state.012 It is situated on the
//  Huron River and is about 35 miles away from Detroit City. The city
//  is home to the University of Michigan, a public research
//  university, and Ann Arbor Municipal Airport, which is about 4
//  miles away from the city center.01 The University of Michigan's
//  main campus is located at 500 S State St, Ann Arbor.4 Michigan
//  Stadium, the second-largest sporting facility in the world, is
//  located at 1201 S Main St, Ann Arbor.3 The GPS coordinates of Ann
//  Arbor are 42.279594, -83.732124.01
//const keys= [
//  '_readableState',   '_events',
//  '_eventsCount',     '_maxListeners',
//  'socket',           'httpVersionMajor',
//  'httpVersionMinor', 'httpVersion',
//  'complete',         'rawHeaders',
//  'rawTrailers',      'aborted',
//  'upgrade',          'url',
//  'method',           'statusCode',
//  'statusMessage',    'client',
//  '_consuming',       '_dumped',
//  'next',             'baseUrl',
//  'originalUrl',      '_parsedUrl',
//  'params',           'query',
//  'res',              'route'
//];
//function dumpReq(req) {
//  for(var i=0;i<keys.length;i++){
//    const key=keys[i];
//    try {
//      const tmp={};
//      tmp[key]=req[key];
//      console.log(pp(tmp));
//    } catch ( err ) {
//      console.log({key,err});
//    };
//  };
//  const res={};
//  res.method=req.method;
//  res.statusMessage=req.statusMessage;
//  res.url=req.url;
//  res.query=req.query;
//  res.params=req.params;
//  res.rawHeaders=req.rawHeaders;
//  res.dumped=req._dumped;
//  res.parsedUrl=req._parsedUrl;
//  res.route=req.route;
//  res.rawTrailers=req.rawTrailers;
//
//  const json = pp({res});
//  console.log(json);
//  return json;
//};
//console.log("index.js");
//function xxx(req, res, next) {
//  req.socket=null;
//  res.socket=null;
//  next.socket=null;
//  dumpObj(req);
//  dumpObj(res);
//  dumpObj(next);
//     authenticate(req.body.username, req.body.password, function(err, user){
//       if (err) return next(err)
//       if (user) {
//         // Regenerate session when signing in
//         // to prevent fixation
//         req.session.regenerate(function(){
//           // Store the user's primary key
//           // in the session store to be retrieved,
//           // or in this case the entire user object
//           req.session.user = user;
//           req.session.success = 'Authenticated as ' + user.name
//             + ' click to <a href="/logout">logout</a>. '
//             + ' You may now access <a href="/restricted">/restricted</a>.';
//           res.redirect('back');
//         });
//       } else {
//         req.session.error = 'Authentication failed, please check your '
//           + ' username and password.'
//           + ' (use "tj" and "foobar")';
//         res.redirect('/login');
//       }
//     });
//};
//async function makeCity() {
//  const City = Parse.Object.extend("city");
//  const annArbor = new City();
//  const point = new Parse.GeoPoint(42.279594,-83.732124);
////  42.279594, -83.732124.01
//  annArbor.set("location",point);
//  annArbor.set("city","ann arbor");
//  annArbor.set("state","mi");
//  annArbor.set("country","usa");
//  annArbor.set("fromCache",true);
//  annArbor.set("minLng",point.latitude);
//  annArbor.set("maxLat",point.latitude);
//  annArbor.set("maxLng",point.latitude);
//  annArbor.set("minLat",point.latitude);
//  annArbor.save();
//
//  //console.trace(annArbor);
//};
//async function makeAddress() {
//  const City = Parse.Object.extend("address");
//  const oldHome = new City();
//  const point = new Parse.GeoPoint(42.279594,-83.732124);
////  42.279594, -83.732124.01
//  oldHome.set("location",point);
//  oldHome.set("city","ann arbor");
//  oldHome.set("state","mi");
//  oldHome.set("country","usa");
//  oldHome.set("address","1415 brooklyn, ann arbor, mi, 48104");
//  oldHome.set("fromCache",true);
//  const res = await oldHome.save();
//  console.log(pp({oldHome,res}));
//};
