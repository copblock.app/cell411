package cell411.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.annotation.Nonnull;

import cell411.LogicApp;
import cell411.utils.Util;
import cell411.utils.collect.Collect;

public class RingtoneData
{
  private final List<RingtonePair> smTones = new ArrayList<>();
  private final TreeMap<String, Tone> mData = new TreeMap<>();
  private final TreeSet<String> smAllKeys =
    Collect.asTreeSet("ALERT", "REQUEST", "CHAT");
//  private final SharedPreferences smPrefs;
  private final LogicApp mApp = LogicApp.req();
  public RingtoneData() {
//    smPrefs = LogicApp.req().getAppPrefs();
  }
  public TreeMap<String, Tone> getTones() {
    return null;
  }

  public class Tone {
    private final String mKey;
    private final Uri mUri;
    private final String mTitle;

    public Tone(String key, Uri uri, String title) {
      mKey = key;
      mUri = uri;
      mTitle = Util.isNoE(uri) ? "" : title;
    }

    public String getKey() {
      return mKey;
    }

    public Uri getUri() {
      return mUri;
    }

    public String getTitle() {
      return mTitle;
    }
    private void store()
    {
      SharedPreferences.Editor edit = getPrefs().edit();
      if (!Util.isNoE(mUri) && !Util.isNoE(mTitle)) {
        edit.putString(getUriKey(mKey), mUri.toString());
        edit.putString(getTitleKey(mKey), mTitle);
      } else {
        edit.remove(getUriKey(mKey));
        edit.remove(getTitleKey(mKey));
      }
      edit.apply();
    }
    public boolean isEmpty()
    {
      return Util.isNoE(mUri) || Util.isNoE(mTitle);
    }



  }
  boolean mLoaded=false;
  public TreeMap<String, Tone> getRingtoneData()
  {
    if (!mLoaded) {
      mLoaded = true;
      loadRingtoneData();
    }
    return mData;
  }

  public List<RingtonePair> fetchAvailableRingtones(Context context)
  {
    List<RingtonePair> ringtones = new ArrayList<>();
    if (Util.theGovernmentIsLying())
      return ringtones;
    RingtoneManager mgr = new RingtoneManager(context);
    mgr.setType(RingtoneManager.TYPE_RINGTONE);

    int n = mgr.getCursor().getCount();
    for (int i = 0; i < n; i++) {
      Ringtone ringtone = mgr.getRingtone(i);
      Uri uri = mgr.getRingtoneUri(i);
      if (ringtone == null || uri == null) {
        continue;
      }
      RingtonePair pair = new RingtonePair();
      pair.mUri = uri;
      pair.mRingtone = ringtone;
      ringtones.add(pair);
    }
    return ringtones;
  }

  public void addRingtoneData(final String key, final Uri uri)
  {
    String title = uri.getQueryParameter("title");
    if (Util.isNoE(title)) {
      throw new IllegalArgumentException("No title for ringtone!");
    }
    RingtoneData.Tone value = new Tone(key, uri, title);
    value.store();
    getRingtoneData().put(key, value);
  }

  public SharedPreferences getPrefs()
  {
    return Util.req(mApp.getPrefs());
  }

  private void loadRingtoneData(String key)
  {
    SharedPreferences prefs = getPrefs();
    String title = prefs.getString(getTitleKey(key), "");
    String uri = prefs.getString(getUriKey(key), "");
    boolean unsaved = false;
    if (Util.isNoE(title) || Util.isNoE(uri)) {
      unsaved = true;
      smTones.addAll(fetchAvailableRingtones(LogicApp.req()));
      while (Util.isNoE(title) || Util.isNoE(uri)) {
        try {
          if (smTones.isEmpty())
            break;
          int position = (int) (Math.random() * smTones.size());
          RingtonePair pair = smTones.get(position);
          Ringtone tone = pair.mRingtone;
          if (pair.mUri != null) {
            uri = pair.mUri.toString();
          }
          title = tone.getTitle(mApp);
        } catch (Throwable throwable) {
          throwable.printStackTrace();
        }
      }
    }
    if (Util.isNoE(title) || Util.isNoE(uri)) {
      getRingtoneData().put(key, null);
    } else {
      Tone value = new Tone(key, Uri.parse(uri), title);
      if (unsaved) {
        value.store();
      }
      getRingtoneData().put(key, value);

    }
  }

  public void loadRingtoneData()
  {
    for (String key : smAllKeys) {
      loadRingtoneData(key);
    }
  }

  @Nonnull
  private String getUriKey(final String key)
  {
    return key + ".uri";
  }

  @Nonnull
  private String getTitleKey(final String key)
  {
    return key + ".title";
  }



  static class RingtonePair
  {
    Uri mUri;
    Ringtone mRingtone;
  }
}
