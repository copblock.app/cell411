// package cell411.logic;
//
// import java.io.File;
//
// import javax.annotation.Nonnull;
//
// import cell411.config.ConfigDepot;
// import cell411.utils.io.IOUtil;
//
// public abstract class CacheObject
//   extends ObjectCache
//   implements ICacheObject
// {
//   private final String mName;
//   private final File mCacheFile;
//   protected boolean mDoneLoading = false;
//
//   public CacheObject(LiveQueryService service, String name)
//   {
//     mName = name;
//     mCacheFile = ConfigDepot.getJsonCacheFile(name);
//   }
//
//   @Override
//   public String getName()
//   {
//     return mName;
//   }
//
//   public void clear()
//   {
//     IOUtil.delete(getCacheFile());
//   }
//
//   @Override
//   public boolean cacheExists()
//   {
//     return getCacheFile().exists();
//   }
//
//   @Override
//   public File getCacheFile()
//   {
//     return mCacheFile;
//   }
//
//   @Override
//   @Nonnull
//   public File getBackupFile()
//   {
//     return new File(getCacheFile().toString() + ".bak");
//   }
//
//   public void setInitialLoadComplete()
//   {
//     mDoneLoading = true;
//   }
//
// }
//