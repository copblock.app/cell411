package cell411.logic;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;
import static androidx.core.app.NotificationManagerCompat.IMPORTANCE_DEFAULT;
import static androidx.core.app.NotificationManagerCompat.IMPORTANCE_MAX;
import static com.parse.livequery.LiveQueryEventId.CREATE;

import android.Manifest.permission;
import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.net.Uri;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationChannelCompat;
import androidx.core.app.NotificationChannelCompat.Builder;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.graphics.drawable.IconCompat;

import com.parse.model.ObjectEvent;
import com.parse.model.ParseGeoPoint;
import com.parse.model.ParseObject;

import java.util.Date;
import java.util.HashMap;
import java.util.StringJoiner;
import java.util.TreeMap;

import javax.annotation.Nonnull;

import cell411.LogicApp;
import cell411.android.RingtoneData;
import cell411.android.RingtoneData.Tone;
import cell411.enums.ProblemType;
import cell411.model.XAlert;
import cell411.model.XChatMsg;
import cell411.model.XPublicCell;
import cell411.model.XRequest;
import cell411.model.XUser;
import cell411.utils.ImageUtils;
import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.PrintString;
import cell411.utils.concurrent.Task;

public class NotificationAgent
{
  private static final String REQUEST_CHANNEL = "bREQUEST_CHANNEL";
  private static final String ALERT_CHANNEL = "bALERT_CHANNEL";
  private static final String CHAT_CHANNEL = "bCHAT_CHANNEL";

  private final HashMap<String, ChData> mChData = new HashMap<>();
  private final NotificationManagerCompat mManager =
    NotificationManagerCompat.from(LogicApp.req());
  private final LQListener<XRequest> mRequestListener =
    new LQListener<XRequest>()
    {
      public void change()
      {

      }

      @Override
      public void onEvents(ObjectEvent<XRequest> event)
      {
        if (event.getEventId() == CREATE && !event.getObject().isSelfAlert()) {
          sendRequestNotification(event.getObject());
        }
      }
    };
  LogicApp mApp = LogicApp.req();
  private final LQListener<XPublicCell> mPublicCellListener =
    new LQListener<XPublicCell>()
    {
      @Override
      public void change()
      {

      }

      @Override
      public void onEvents(ObjectEvent<XPublicCell> event)
      {
        LQListener.super.onEvents(event);
        if (event.getObject().ownedByCurrent()) {
          return;
        }
        ParseGeoPoint currentLocation = mApp.getCurrentLocation();
        ParseGeoPoint cellLocation = event.getObject().getLocation();
        if (currentLocation.distanceInMilesTo(cellLocation) >= 50) {
          return;
        }
        sendPublicCellNotification(event.getObject());
      }

    };
  private final LiveQueryService mService;
  //Keys not yet used.
  //builder.setConversationId(null);
  //builder.setGroup(null);
  //builder.setLightColor(null);
  //builder.setLightsEnabled(null);
  //builder.setVibrationEnabled(null);
  //builder.setVibrationPattern(null);
  LQListener<XAlert> mAlertListener = new LQListener<XAlert>()
  {
    public void change()
    {
    }

    public void onEvents(ObjectEvent<XAlert> event)
    {
      sendAlertNotification(event.getObject());
    }
  };
  Task<Void> mSubscribeToQueries =
    Task.forVirtual(Void.class,this, "subscribeToQueries");
  private AudioAttributes mAudioAttributes;

  public NotificationAgent(LiveQueryService service)
  {
    mService = service;
    createChannels();
  }

  void createChannels()
  {
    RingtoneData ringtoneData = mApp.getRingtoneData();
    TreeMap<String, Tone> tones = ringtoneData.getTones();
    ChData data;

    Tone toneData;
    // ALERT_CHANNEL
    data = new ChData(ALERT_CHANNEL);
    toneData = tones.get("ALERT");
    if (toneData != null) {
      data.mRingtone = toneData.getUri();
    }
    NotificationChannelCompat.Builder builder;
    builder = new Builder(ALERT_CHANNEL, IMPORTANCE_MAX);
    builder.setName("Emergency Alerts");
    builder.setDescription("The channel for emergencies");
    builder.setShowBadge(true);
    builder.setSound(data.mRingtone, audioAttributes());
    builder.setVibrationEnabled(true);
    data.mChannel = builder.build();
    mManager.createNotificationChannel(data.mChannel);

    // REQUEST_CHANNEL
    data = new ChData(REQUEST_CHANNEL);
    //    UrlUtils.toUri(tones.get("REQUEST.uri"));
    toneData = tones.get("REQUEST");
    if (toneData != null) {
      data.mRingtone = toneData.getUri();
    }

    builder = new Builder(REQUEST_CHANNEL, IMPORTANCE_DEFAULT);
    builder.setName("Requests");
    builder.setDescription("A channel for friend and cell join requests");
    builder.setShowBadge(true);
    builder.setSound(data.mRingtone, audioAttributes());
    data.mChannel = builder.build();
    mManager.createNotificationChannel(data.mChannel);

    // CHAT_CHANNEL
    data = new ChData(CHAT_CHANNEL);
    toneData = tones.get("CHAT");
    if (toneData != null) {
      data.mRingtone = toneData.getUri();
    }
    builder = new Builder(CHAT_CHANNEL, IMPORTANCE_DEFAULT);
    builder.setName("Chat Messages");
    builder.setDescription("The channel for chats");
    builder.setShowBadge(true);
    builder.setSound(data.mRingtone, audioAttributes());
    builder.setVibrationEnabled(true);
    data.mChannel = builder.build();

    mManager.createNotificationChannel(data.mChannel);
  }

  AudioAttributes audioAttributes()
  {
    if (mAudioAttributes == null) {
      mAudioAttributes = new AudioAttributes.Builder().setContentType(
          AudioAttributes.CONTENT_TYPE_SONIFICATION)
        .setUsage(AudioAttributes.USAGE_ALARM).build();
    }
    return mAudioAttributes;
  }

  public void sendLocationNotification(ParseGeoPoint geoPoint)
  {
    ThreadUtil.onExec(new Notifier(geoPoint));
  }

  public void sendRequestNotification(XRequest request)
  {
    ThreadUtil.onExec(new Notifier(request));
  }

  public void sendAlertNotification(XAlert alert)
  {
    ThreadUtil.onExec(new Notifier(alert));
  }

  private ChData getChData(final ParseObject object)
  {
    String className = object.getClassName();
    switch (className) {
      case "Request":
        return mChData.get(REQUEST_CHANNEL);
      case "Alert":
        return mChData.get(ALERT_CHANNEL);
      default:
        return mChData.get(CHAT_CHANNEL);
    }
  }

  public void subscribeToQueries()
  {
    LiveQueryService lqs = mService;
    if (lqs == null) {
      ThreadUtil.onExec(mSubscribeToQueries,500);
      return;
    }
    AlertWatcher alertWatcher = lqs.getAlertWatcher();
    RequestWatcher requestWatcher = lqs.getRequestWatcher();
    CellWatcher<XPublicCell> cellWatcher = lqs.getPublicCellWatcher();
    if (alertWatcher == null || requestWatcher == null || cellWatcher == null) {
      ThreadUtil.onExec(mSubscribeToQueries,500);
    }

  }

  public void restore()
  {
    ChData data;
    RingtoneData toneData = mApp.getRingtoneData();
    TreeMap<String, Tone> tones = toneData.getTones();

    data = mChData.get(ALERT_CHANNEL);
    assert data != null;
    Tone tone = tones.get("ALERT");
    if (tone != null) {
      data.mRingtone = tone.getUri();
    }

    data = mChData.get(REQUEST_CHANNEL);
    assert data != null;
    tone = tones.get("REQUEST");
    if (tone != null) {
      data.mRingtone = tone.getUri();
    }
  }

  private void sendPublicCellNotification(XPublicCell ignoredObject)
  {

  }

  @Nonnull
  private Intent createIntent(final LogicApp context)
  {
    Context intentContext = context;
    Activity currentActivity = context.getCurrentActivity();
    if (currentActivity != null) {
      intentContext = currentActivity;
    }

    int flags = 0;
    if (currentActivity == null) {
      flags = FLAG_ACTIVITY_NEW_TASK;
    }
    Intent notificationIntent =
      new Intent(intentContext, mApp.getActivityClass());
    flags |= FLAG_ACTIVITY_SINGLE_TOP;
    notificationIntent.addFlags(flags);
    notificationIntent.putExtra("fragment", "FriendRequestFragment");
    return notificationIntent;
  }

  @Nonnull
  public String toString()
  {
    StringJoiner joiner = new StringJoiner("");
    joiner.add("TAG").add("[");
    joiner.add(getClass().getSimpleName());
    if (Util.theGovernmentIsHonest()) {
      joiner.add(mManager.toString());
    }
    joiner.add("]");
    return joiner.toString();
  }

  public void sendChatNotification(final XChatMsg chatMsg)
  {
    ThreadUtil.onExec(new Notifier(chatMsg));
  }

  static class Counter
  {
    int mValue;

    Counter()
    {
      this(0);
    }

    Counter(int value)
    {
      mValue = value;
    }

    public int next()
    {
      return mValue++;
    }
  }

  class Subscriber
    implements Runnable
  {

    @Override
    public void run()
    {
      boolean done = false;
      try {
        LiveQueryService service = LiveQueryService.opt();
        if (service == null)
          return;
        if (!service.isReady())
          return;
        subscribeToQueries();
        done = true;
      } finally {
        if (!done) {
          ThreadUtil.onExec(this,1000);
        }
      }
    }
  }

  class Notifier
    implements Runnable
  {
    final ParseObject mObject;

    Notifier(final ParseObject object)
    {
      mObject = object;
    }
    LogicApp app;
    public Notifier(ParseGeoPoint geoPoint)
    {
      mObject = new ParseObject("Holder");
      mObject.put("location", geoPoint);
    }

    public void run()
    {
      ChData chData = getChData(mObject);
      assert chData != null;
      String message = chData.genMessage(mObject);
      if (message == null)
        return;
      NotificationChannelCompat nc = chData.mChannel;
      NotificationCompat.Builder builder;
      LogicApp context = LogicApp.req();
      builder = new NotificationCompat.Builder(context, nc.getId());
      builder.setAutoCancel(true);
      builder.setSound(chData.mRingtone);
      Bitmap avatar = chData.getAvatar(mObject);
      if (avatar != null) {
        IconCompat compat = IconCompat.createWithBitmap(avatar);
        builder.setSmallIcon(compat);
        builder.setLargeIcon(avatar);
      } else {
        Bitmap bitmap = null;
        if (mObject instanceof XAlert) {
          XUser owner = (XUser) mObject.get("owner", false);
          if (owner != null) {
            bitmap = owner.getAvatarPic();
          }
        }
        if (bitmap == null) {
          bitmap = ImageUtils.getLargeIconBitmap(mApp.getLargeIcon());
        }
        builder.setLargeIcon(bitmap);
        builder.setSmallIcon(mApp.getSmallIcon());
      }
      builder.setPriority(chData.getPriority());
      Date createdAt = mObject.getCreatedAt();
      if (createdAt != null) {
        builder.setShowWhen(true);
        builder.setWhen(createdAt.getTime());
      }
      Counter counter = chData.mCounter;

      Intent intent = createIntent(context);
      PendingIntent pendingIntent =
        PendingIntent.getActivity(context, 0, intent,
          PendingIntent.FLAG_IMMUTABLE);
      builder.setContentIntent(pendingIntent);

      int notificationId =
        counter == null ? (int) (Math.random() * Integer.MAX_VALUE) :
          counter.next();

      String title = chData.genTitle(mObject);
      builder.setContentTitle(title + "  #" + notificationId);
      builder.setChannelId(nc.getId());
      builder.setContentText(message);
      builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));

      builder.setSound(chData.mRingtone);

      Notification notification = builder.build();

      if (ActivityCompat.checkSelfPermission(mService,
        permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED)
      {
        mApp.showToast("Unable to display notification");
      }
      mManager.notify(notificationId, notification);
    }
  }

  class ChData
  {
    String mId;
    NotificationChannelCompat mChannel;
    Uri mRingtone;
    Counter mCounter = new Counter();

    ChData(String id)
    {
      mId = id;
      mChData.put(id, this);
    }

    public String genTitle(final ParseObject object)
    {
      if (object instanceof XAlert) {
        return "Emergency Alert Received";
      }
      return "Title:  " + object;
    }

    public Bitmap getAvatar(final ParseObject object)
    {
      XUser user = object.has("owner") ? (XUser) object.get("owner") : null;
      if (user == null) {
        return null;
      }
      return user.getAvatarPic();
    }

    public int getPriority()
    {
      return mChannel.getImportance();
    }

    public String genMessage(final ParseObject object)
    {
      if (object instanceof XAlert) {
        XAlert alert = (XAlert) object;
        XUser user = alert.getOwner();
        ProblemType problemType = alert.getProblemType();
        String note = alert.getNote();
        if (Util.isNoE(note)) {
          note = "";
        } else {
          note = " '" + note + "'";
        }
        return Util.format("%s issued a %s alert%s", user.getName(),
          problemType, note);
      } else if (object instanceof XRequest) {
        XRequest request = (XRequest) object;
        XPublicCell cell = request.getCell();
        XUser owner = request.getOwner();
        XUser sentTo = request.getSentTo();
        XUser curr = XUser.reqCurrentUser();
        try (PrintString ps = new PrintString()) {
          String oName, sName, gNote = "", rType;
          if (owner == curr) {
            oName = "You";
            sName = owner.getName();
          } else if (sentTo == curr) {
            oName = sentTo.getName();
            sName = "You";
          } else {
            return null;
          }
          if (cell == null) {
            rType = "friend request";
          } else {
            rType = "cell join request";
            gNote = "\n\nFor cell " + cell.getName();
          }
          ps.p(oName).p(" sent a ").p(rType).p(" to ").pl(sName).pl(gNote);
          return ps.toString();
        }
      } else if (object instanceof XPublicCell) {
        XPublicCell cell = (XPublicCell) object;
        return "A new public cell called " + cell + "\n" +
          "was created near you";
      } else if (object instanceof XChatMsg) {
        return "A new chat message arrived";
      } else {
        return null;
      }
    }
  }
  //  public void showNotification(String title, String message, XObject object)
  //  {
  //    NotificationManagerCompat nm = NotificationManagerCompat.from(mContext);
  //
  //    Intent notificationIntent = new Intent(mContext,
  //      MainActivity.class);
  //    //FIXME!    mCurrentUser.addPendingEvent(object);
  //
  //    notificationIntent.putExtra("object", object.getClassName() + "::" +
  //    object.getObjectId());
  //
  //    PendingIntent pendingIntent =
  //      PendingIntent.getActivity(mContext, 0, notificationIntent,
  //      PendingIntent.FLAG_IMMUTABLE);
  //
  //    NotificationCompat.Builder notificationBuilder =
  //      new NotificationCompat.Builder(mContext, CHANNEL);
  //    notificationBuilder.setSmallIcon(cell411.services.R.mipmap.ic_launcher);
  //    notificationBuilder.setOngoing(true);
  //    notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
  //    notificationBuilder.setShowWhen(true);
  //    notificationBuilder.setWhen(object.getCreatedAt().getTime());
  //    notificationBuilder.setContentTitle(title + "  #" + mLastId);
  //    notificationBuilder.setChannelId(CHANNEL);
  //    Bitmap bitmap = BitmapFactory.decodeResource(Bro411.get()
  //    .getResources(), R.mipmap
  //    .ic_launcher);
  //
  //    notificationBuilder.setLargeIcon(bitmap);
  //
  //    if (message == null)
  //      message = "The service is running";
  //
  //    notificationBuilder.setContentText(message);
  //
  //    notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
  //    .bigText(message));
  //    if (pendingIntent != null)
  //      notificationBuilder.setContentIntent(pendingIntent);
  //
  //    Notification notification = notificationBuilder.build();
  //    notification.defaults =
  //      Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
  //    ++mLastId;
  //    nm.notify(mLastId, notification);
  //  }

}
