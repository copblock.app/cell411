package cell411.logic;

import cell411.model.XUpdate;
import cell411.model.XUser;
import com.parse.ParseQuery;
import com.parse.model.ObjectEvent;
import org.jetbrains.annotations.NotNull;

public class UpdateWatcher extends Watcher<XUpdate> {

  public UpdateWatcher(@NotNull RelationWatcher relationWatcher) {
    super(relationWatcher, "Update", XUpdate.class);
  }

  @Override
  public ParseQuery<XUpdate> directQuery() {
    XUser user = XUser.reqCurrentUser();

    ParseQuery<XUpdate> query1 = ParseQuery.getQuery(XUpdate.class);
    query1.whereEqualTo("owningId",user.getObjectId());

    ParseQuery<XUpdate> query2 = ParseQuery.getQuery(XUpdate.class);
    query1.whereEqualTo("relatedId",user.getObjectId());

    return ParseQuery.or(query1,query2);
  }

  @Override
  public synchronized void onEvents(ObjectEvent<XUpdate> event) {
    super.onEvents(event);
  }
}
