package cell411.logic.rel;

import java.util.TreeMap;

import cell411.logic.RelMap;

public class Rel
  extends BaseRel
{
  final static RelMap smMap = new RelMap();
  final static TreeMap<Key,Rel> smCounts = new TreeMap<>();
  public static RelMap getMap() {
    return smMap;
  }
  private Rel(Key key)
  {
    super(key);
    synchronized(smCounts) {
      assert smCounts.get(key) == null;
      smCounts.put(key, this);
      assert smCounts.get(key) == this;
    }
  }

  public static Rel createInRelMap(Key key) {
    return new Rel(key);
  }
}
