package cell411.logic.rel;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import cell411.logic.AggMap;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;

public class AggRel
  extends BaseRel
  implements MyObserver
{
  final static AggMap smMap = new AggMap();
  public static AggMap getMap() {
    return smMap;
  }
  private final TreeSet<BaseRel> mRels = new TreeSet<>();
  public Set<BaseRel> values()
  {
    return getRels();
  }

  public Set<BaseRel> getRels()
  {
    return Collections.unmodifiableSet(mRels);
  }


  public static AggRel create(Key key) {
    return new AggRel(key);
  }

  private AggRel(Key key)
  {
    super(key);
    smMap.put(key,this);
  }

  @Override
  public Iterator<BaseRel> relIterator() {
    return getRels().iterator();
  }

  public void addRels(BaseRel... rels)
  {
    TreeSet<String> ids = new TreeSet<>(getRelatedIds());
    for (BaseRel rel : rels) {
      if (rel == null) {
        System.out.println("Warning:  missing rel");
        continue;
      }
      mRels.add(rel);
      rel.addObserver(this);
    }
    update(null, null);
  }

  @Override
  public void update(MyObservable o, Object arg)
  {
    HashSet<String> ids = new HashSet<>();
    for (BaseRel rel : getRels()) {
      ids.addAll(rel.getRelatedIds());
    }
    setRelatedIds(ids);
  }

}
