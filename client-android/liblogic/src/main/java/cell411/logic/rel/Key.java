package cell411.logic.rel;


import com.parse.Parse;
import com.parse.model.ParseUser;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

import javax.annotation.Nonnull;

import cell411.utils.Util;

public class Key
  implements Comparable<Key> {
  static Key smCurrentUserKey = new Key("*");

  static KeyFactory mKeyFactory = new KeyFactory();
  static HashMap<String, Key> mCache = new HashMap<>();

  public final String mOwningClass;
  public final String mOwningField;
  public final String mOwningId;
  // The className of the related Object
  final String mRelatedClass;
  final String mRelatedField;
  public String mString;

  private Key(String string) {
    mString = string.intern();
    mOwningClass = null;
    mOwningField = null;
    mRelatedClass = null;
    mRelatedField = null;
    mOwningId = null;
  }

  private Key(String owningClass, String owningField, String relatedClass,
              String relatedField, String owningId) {
    mOwningClass = owningClass;
    mOwningField = owningField;
    mRelatedClass = relatedClass;
    mRelatedField = relatedField;
    ParseUser parseUser = Parse.reqCurrentUser();
    String objectId = parseUser.getObjectId();
    if (owningId.equals(objectId))
      owningId = "*";
    mOwningId = owningId;
  }

  public static Key create(String str) {
    return mCache.computeIfAbsent(str, mKeyFactory);
  }

  public static Key create(String str, boolean b) {
    assert !str.contains(":") || (b);
    return mCache.computeIfAbsent(str, mKeyFactory);
  }

  public static Key create(List<String> parts) {
    return create(parts.toArray(new String[0]));
  }

  public static Key create(String s1, String s2, String s3, String s4,
                           String s5) {
    return new Key(s1, s2, s3, s4, s5);
  }

  public static Key create(String[] keyStrs) {
    assert keyStrs.length == 5;
    return new Key(keyStrs[0], keyStrs[1], keyStrs[2],
      keyStrs[3], keyStrs[4]);
  }

  @Override
  public int compareTo(Key o) {
    return toString().compareTo(String.valueOf(o));
  }

  @Nonnull
  public String toString() {
    if (mString == null) {
      mString = makeString();
    }
    return mString;
  }

  public String makeString() {
    if(mString!=null)
      return mString;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
      baos.write(mOwningClass.getBytes());
      baos.write(':');
      baos.write(mOwningField.getBytes());
      baos.write(':');
      baos.write(mRelatedClass.getBytes());
      baos.write(':');
      baos.write(mRelatedField.getBytes());
      baos.write(':');
      baos.write(mOwningId.getBytes());
      mString= baos.toString();
    } catch (Throwable t) {
      throw Util.rethrow(t);
    }
    return mString;
  }


  private static class KeyFactory
    implements Function<String, Key>
  {
    public KeyFactory()
    {
    }

    @Override
    public Key apply(String s)
    {
      if (s.contains(":")) {
        String[] parts = s.split(":");
        if (parts.length != 5)
          throw new RuntimeException("Key must have one seg or 5");
        return new Key(parts[0], parts[1], parts[2], parts[3], parts[4]);
      } else if (s.equals("*") || s.equals("CurrentUser")) {
        return new Key("*");
      } else {
        return new Key(s);
      }
    }
  }
}
