package cell411.logic;

import com.parse.ParseQuery;

import java.util.Set;

import cell411.logic.rel.AggRel;
import cell411.logic.rel.Rel;
import cell411.model.XRequest;
import cell411.model.XUser;

public class UserWatcher
  extends Watcher<XUser>
{
  Rel mFriends = mRelationWatcher.getFriends();
  AggRel mCounterParties = mRelationWatcher.getCounterParties();
  AggRel mCellMembers = mRelationWatcher.getAllCellMembers();

  public UserWatcher(final RelationWatcher relationWatcher)
  {
    super(relationWatcher, "users", XUser.class);
    mIds.addRels(mFriends,mCounterParties);
  }

  @Override
  public ParseQuery<XUser> directQuery()
  {
    XUser user = XUser.reqCurrentUser();
    ParseQuery<XUser> fr1 = user.queryFriends();
    ParseQuery<XUser> fr2 = ParseQuery.getQuery("_User");
    fr2.whereEqualTo("friends", user);

    ParseQuery<XRequest> rq1 = getService().getRequestWatcher().directQuery();
    ParseQuery<XUser> fr3 =  ParseQuery.getQuery("_User");
    fr3.whereMatchesKeyInQuery("objectId", "owner", rq1);

    return ParseQuery.or(fr1, fr2, fr3);
  }

  @Override
  public synchronized ParseQuery<XUser> createQuery(Set<String> dirty)
  {
    ParseQuery<XUser> query = super.createQuery(dirty);
    query.include("avatarURL");
    query.include("thumbNailURL");
    return query;
  }

  public boolean isFriend(XUser user)
  {
    return isFriend(user.getObjectId());
  }

  public boolean isFriend(String id)
  {
    return mFriends.contains(id);
  }

}
