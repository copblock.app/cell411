package cell411.logic;

import com.parse.ParseException;
import com.parse.model.ObjectEvent;
import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseObject;

public interface LQListener<X extends ParseObject>
  extends ObjectEventsCallback<X>
{
  default void done(Watcher<X> watcher, ParseException e)
  {
    if (e == null) {
      change();
    }
  }

  void change();

  default void onEvents(ObjectEvent<X> event)
  {
    change();
  }
}
