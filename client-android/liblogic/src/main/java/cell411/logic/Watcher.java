package cell411.logic;

import androidx.annotation.CallSuper;

import com.parse.Parse;
import com.parse.ParseQuery;
import com.parse.model.ObjectEvent;
import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Nonnull;

import cell411.logic.rel.AggRel;
import cell411.logic.rel.Key;
import cell411.model.XChatRoom;
import cell411.model.XEntity;
import cell411.utils.Util;
import cell411.utils.reflect.XTAG;

public abstract class Watcher<X extends ParseObject>
  implements ObjectEventsCallback<X>
{
  //  final protected HashSet<LQListener<X>> mListeners = new HashSet<>();
  final protected RelationWatcher mRelationWatcher;
  final protected AggRel mIds;
  final protected Class<X> mType;
  protected final String mClassName;
  private final String mName;
  protected ParseQuery<X> mQuery;
  volatile protected Date mLastBatch;
  private static final XTAG TAG = new XTAG();
  private final boolean mInitialQuery = true;
  private ParseQuery<X> mLiveQuery;
  final static LiveQueryService mService = LiveQueryService.req();
  LiveQueryService getService() {
    assert LiveQueryService.opt()==mService :
      "The installed LiveQueryService is not the one I knew!";
    return mService;
  }
  public Watcher(@Nonnull RelationWatcher relationWatcher, @Nonnull String name,
                 @Nonnull Class<X> type)
  {
    assert relationWatcher != null;
    mRelationWatcher = relationWatcher;
    mType = type;
    mClassName = ParseQuery.getQuery(type).getClassName();
    mIds = mRelationWatcher.getAggRel(Key.create(mClassName));
    mName = name;
  }

  public RelationWatcher getRelationWatcher() {
    return mRelationWatcher;
  }
  public String getName()
  {
    return mName;
  }


  abstract public ParseQuery<X> directQuery();

  synchronized public ParseQuery<X> createQuery(Set<String> dirty)
  {
    ParseQuery<X> query = ParseQuery.getQuery(mType);
    TreeSet<String> ids = new TreeSet<>(dirty);
    query.whereContainedIn("objectId", ids);
    return query;
  }

  public synchronized Collection<X> getValues()
  {
    return getValues(mIds, mType);
  }

  private Collection<X> getValues(AggRel ids, Class<X> type)
  {
    return Util.transform(ids.getRelatedIds(), Parse::getObject);
  }

  public synchronized Map<String, X> getData()
  {
    Map<String, X> map = new HashMap<>();
    for (Iterator<String> it = mIds.idIterator(); it.hasNext(); ) {
      String keyStr = it.next();
      map.put(keyStr, Parse.getObject(keyStr));
    }
    return map;
  }

  synchronized public void createSubscription()
  {
    // first time
    if (mLiveQuery == null) {
      mLiveQuery = mQuery;
      if (mLiveQuery == null)
        mLiveQuery = directQuery();
      getService().subscribe(mLiveQuery, this);
    } else {
      ParseQuery<X> newQuery = directQuery();
      if (newQuery == null) {
        newQuery = createQuery(getIds(true));
      }

      ParseQuery<X> oldQuery = mLiveQuery;
      if (oldQuery != null) {
        if (newQuery != null) {
          int cmp = oldQuery.compareTo(newQuery);
          if (cmp != 0) {
            getService().unsubscribe(oldQuery);
            mLiveQuery = newQuery;
            getService().subscribe(mLiveQuery, this);
          }
        } else {
          getService().unsubscribe(oldQuery);
        }
      } else {
        getService().subscribe(newQuery, this);
      }
    }
  }

  protected synchronized Set<String> getIds(boolean dirty)
  {
    if (dirty) {
      Set<String> result = getIds(false);
      for (String id : result) {
        ParseObject object = ParseObject.getIfExists(id);
        Date updated = object == null ? null : object.getUpdatedAt();
      }
      result.removeIf(id -> !isDirty(id));
      return result;
    } else {
      return new TreeSet<>(mIds.getRelatedIds());
    }
  }

  protected synchronized boolean isDirty(final String key)
  {
    X object = ParseObject.getIfExists(key);
    if (object == null)
      return true;
    Date update = object.getUpdatedAt();
    Date latest = mRelationWatcher.getDate(key);
    if (update == null)
      return true;
    if (latest == null) {
      mRelationWatcher.setDate(key, update);
      return false;
    }
    return update.before(latest);
  }

  @Override
  synchronized public void onEvents(final ObjectEvent<X> event)
  {
    HashSet<ParseObject> needs = new HashSet<>();
    switch (event.getEventId()) {
      case CREATE:
      case ENTER:
      case UPDATE:
        greetObject(event.getObject());
        break;
      case LEAVE:
      case DELETE:
        removeObject(event.getObject());
        break;
    }
  }

  @CallSuper
  synchronized void greetObject(X po)
  {
    po.fetchIfNeeded();
    if (po instanceof XEntity) {
      XChatRoom room = (XChatRoom) po.getParseObject("chatRoom");
      if (room != null)
        room.fetchIfNeeded();
    }
    System.out.println(ParseObject.getIfExists(po.getObjectId()));
  }

  synchronized protected void removeObject(final X object)
  {
    mIds.remove(object.getObjectId());
  }

  @Nonnull
  synchronized public String toString()
  {
    return Util.format("Watcher{name=%-10s,items=%d}", getName(), size());
  }

  public int size()
  {
    return mIds.getSize();
  }

  public void greetObjects(ArrayList<ParseObject> list) {
    for(String id : mIds.getRelatedIds()) {
      X x = Parse.getObject(id);
      if(x==null) {
        ParseObject po = Parse.getOrStub(id, mType);
        list.add(po);
      } else {
        greetObject(x);
      }
    }
  }
}