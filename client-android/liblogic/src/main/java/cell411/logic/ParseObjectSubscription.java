package cell411.logic;

import com.parse.ParseQuery;
import com.parse.livequery.ErrorCallback;
import com.parse.livequery.HandleSubscribeCallback;
import com.parse.livequery.HandleUnsubscribeCallback;
import com.parse.livequery.LiveQueryException;
import com.parse.livequery.ParseLiveQueryClient;
import com.parse.livequery.Subscription;
import com.parse.model.ObjectEvent;
import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseObject;

import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.Reflect;

public class ParseObjectSubscription<T extends ParseObject>
  implements HandleUnsubscribeCallback<T>,
             com.parse.model.ObjectEventsCallback<T>,
             ErrorCallback<T>,
             HandleSubscribeCallback<T>
{
  final ParseQuery<T> mQuery;
  final ObjectEventsCallback<T> mCallback;
  private final MultiCacheObject mCache;

  public ParseObjectSubscription(final LiveQueryService iface,
                                 final ParseQuery<T> query,
                                 final ObjectEventsCallback<T> callback)
  {
    mCache = iface.getCache(false);
    mQuery = query;
    mCallback = callback;
    ParseLiveQueryClient client = iface.getClient();
    Subscription<T> handler = client.subscribe(query);//,callback);
    handler.handleEvents(this);
    handler.handleError(this);
    handler.handleSubscribe(this);
    handler.handleUnsubscribe(this);
  }

  @Override
  public void onEvents(final ObjectEvent<T> event)
  {
    ThreadUtil.onExec(event.dispatcher(mCallback));
  }

  public void onUnsubscribe(final ParseQuery<T> tParseQuery)
  {
    Reflect.announce(tParseQuery.getClassName());
  }

  public void onError(final ParseQuery<T> tParseQuery,
                      final LiveQueryException e)
  {
    Reflect.announce(tParseQuery.getClassName());
  }

  public void onSubscribe(final ParseQuery<T> tParseQuery)
  {
    Reflect.announce(tParseQuery.getClassName());
  }
}
