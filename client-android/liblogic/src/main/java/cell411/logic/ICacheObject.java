package cell411.logic;

import java.io.File;

import cell411.json.JSONObject;
import cell411.utils.io.TimeLog;

public interface ICacheObject
{

  String getName();

  void loadFromNet(final TimeLog log);

  void loadFromCache(final TimeLog log);

  void saveToCache(JSONObject json);

  void clear();

  boolean cacheExists();

  File getCacheFile();

  File getBackupFile();

  void report(TimeLog ps);

  void createSubscription();

  void setInitialLoadComplete();
}
