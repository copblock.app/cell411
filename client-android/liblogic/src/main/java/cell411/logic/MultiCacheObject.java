package cell411.logic;

import cell411.json.JSONObject;
import cell411.model.XPrivateCell;
import cell411.model.XPublicCell;
import cell411.utils.Util;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.TimeLog;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;
import com.parse.Parse;
import com.parse.model.ParseObject;
import com.parse.model.ParseUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

public class MultiCacheObject implements ValueObserver<ParseUser>
{
  private final static XTAG TAG = new XTAG();
  private static int smId = 0;

  static {
    XLog.i(TAG, "Loading Class");
  }

  private final ArrayList<Watcher<?>> mAllCacheObjects = new ArrayList<>();
  private final RelationWatcher mRelationWatcher = new RelationWatcher();
  private final CellWatcher<XPrivateCell> mPrivateCellWatcher = new CellWatcher<>(mRelationWatcher, XPrivateCell.class);
  private final RequestWatcher mRequestWatcher = new RequestWatcher(mRelationWatcher);
  private final UserWatcher mUserWatcher = new UserWatcher(mRelationWatcher);
  private final AlertWatcher mAlertWatcher = new AlertWatcher(mRelationWatcher);
  private final CellWatcher<XPublicCell> mPublicCellWatcher = new CellWatcher<>(mRelationWatcher, XPublicCell.class);
  private final ChatRoomWatcher mChatRoomWatcher = new ChatRoomWatcher(mRelationWatcher);
  private final UpdateWatcher mUpdateWatcher = new UpdateWatcher(mRelationWatcher);
  int mId = smId++;
  String mName = "MultiCache: " + mId;

  public MultiCacheObject(@Nonnull LiveQueryService service) {
    Parse.addCurrentUserObserver(this);
  }

  public synchronized RelationWatcher getRelationWatcher() {
    return mRelationWatcher;
  }

  public synchronized RequestWatcher getRequestWatcher() {
    return mRequestWatcher;
  }

  public synchronized CellWatcher<XPublicCell> getPublicCellWatcher() {
    return mPublicCellWatcher;
  }

  public synchronized CellWatcher<XPrivateCell> getPrivateCellWatcher() {
    return mPrivateCellWatcher;
  }

  public synchronized UpdateWatcher getUpdateWatcher() {
    return mUpdateWatcher;
  }

  public synchronized AlertWatcher getAlertWatcher() {
    return mAlertWatcher;
  }

  public synchronized ChatRoomWatcher getChatRoomWatcher() {
    return mChatRoomWatcher;
  }

  public synchronized UserWatcher getUserWatcher() {
    return mUserWatcher;
  }

  public String getName() {
    return toString();
  }

  public void clear() {
    getRelationWatcher().clear();
  }

  public void loadFromCache(final TimeLog log) {
    if (Parse.getCurrentUser() == null) return;
    log.println("Loading From Cache");
    getRelationWatcher().loadFromCache(log);
    log.println("Loading Complete");
  }

  public void saveToCache(JSONObject json) {
    Reflect.announce(mName);
    getRelationWatcher().saveToCache();
  }

  public void report(TimeLog ps) {
    Reflect.announce(mName);
    ps.pl("Starting report");
    RelationWatcher relationWatcher = getRelationWatcher();
    if (relationWatcher != null) {
      relationWatcher.report(ps);
    }
    ps.pl("Report complete");
  }

  public void createSubscription() {
    Reflect.announce(mName);
    getUserWatcher().createSubscription();
    getAlertWatcher().createSubscription();
    getUpdateWatcher().createSubscription();
    getRequestWatcher().createSubscription();
    getChatRoomWatcher().createSubscription();
    getPublicCellWatcher().createSubscription();
    getPrivateCellWatcher().createSubscription();
  }

  public synchronized void run() {
    try {
      mAllCacheObjects.add(getRequestWatcher());

      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      ThreadUtil.waitUntil(this, Parse::isInitialized, 2000);
      if (Parse.getCurrentUser() == null) return;
      TimeLog log = new TimeLog(stream);
      log.println("Starting Load");
      File fileName = getRelationWatcher().getCacheFile();
      Parse.countObjects();
      if (fileName.exists()) {
        try {
          loadFromCache(log);
          if (Parse.getObjectIds().size() < 2) {
            fileName.delete();
          }
        } catch (Exception e) {
          Util.printStackTrace(e);
          fileName.delete();
        }
      }
      log.println("Load Complete");
      mRelationWatcher.loadFromNet(log);
      ArrayList<ParseObject> missing = new ArrayList<>();
      for(Watcher<?> watcher : mAllCacheObjects) {
        watcher.greetObjects(missing);
      }
      getService().setReady(true);
      createSubscription();
      saveToCache(null);
    } catch (Throwable t) {
      throw Util.rethrow(t);
    }
  }

  LiveQueryService getService() {
    return LiveQueryService.req();
  }

  private File getLogFile() {
    File file = getRelationWatcher().getCacheFile();
    file=file.getParentFile();
    file=new File(file, "logfile.log");
    return file;
  }

  @Override
  public synchronized void onChange(@Nullable final ParseUser newValue, @Nullable final ParseUser oldValue) {
    if (newValue == null) clear();
    notifyAll();
  }

}
