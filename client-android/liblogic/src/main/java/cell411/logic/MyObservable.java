package cell411.logic;

import java.util.ArrayList;
import java.util.List;

public class MyObservable
{
  private final List<MyObserver> obs;
  private boolean changed = false;

  /**
   * Construct an Observable with zero Observers.
   */

  public MyObservable()
  {
    obs = new ArrayList<>();
  }

  /**
   * Adds an observer to the set of observers for this object, provided
   * that it is not the same as some observer already in the set.
   * The order in which notifications will be delivered to multiple
   * observers is not specified. See the class comment.
   *
   * @param o an observer to be added.
   * @throws NullPointerException if the parameter o is null.
   */
  public synchronized void addObserver(MyObserver o)
  {
    if (o == null) {
      throw new NullPointerException();
    }
    if (!obs.contains(o)) {
      obs.add(o);
    }
  }

  /**
   * Deletes an observer from the set of observers of this object.
   * Passing {@code null} to this method will have no effect.
   *
   * @param o the observer to be deleted.
   */
  public synchronized void deleteObserver(MyObserver o)
  {
    obs.remove(o);
  }

  /**
   * If this object has changed, as indicated by the
   * {@code hasChanged} method, then notify all of its observers
   * and then call the {@code clearChanged} method to
   * indicate that this object has no longer changed.
   * <p>
   * Each observer has its {@code update} method called with two
   * arguments: this observable object and {@code null}. In other
   * words, this method is equivalent to:
   * <blockquote>{@code
   * notifyObservers(null)}</blockquote>
   *
   * @see MyObservable#clearChanged()
   * @see MyObservable#hasChanged()
   * @see MyObserver#update(MyObservable, java.lang.Object)
   */
  public void notifyObservers()
  {
    notifyObservers(null);
  }

  /**
   * If this object has changed, as indicated by the
   * {@code hasChanged} method, then notify all of its observers
   * and then call the {@code clearChanged} method to indicate
   * that this object has no longer changed.
   * <p>
   * Each observer has its {@code update} method called with two
   * arguments: this observable object and the {@code arg} argument.
   *
   * @param arg any object.
   * @see MyObservable#clearChanged()
   * @see MyObservable#hasChanged()
   * @see MyObserver#update(MyObservable, java.lang.Object)
   */
  public void notifyObservers(Object arg)
  {
    /*
     * a temporary array buffer, used as a snapshot of the state of
     * current Observers.
     */
    MyObserver[] arrLocal;
    synchronized (this) {
      if (!hasChanged())
        return;
      clearChanged();
      if(obs.isEmpty())
        return;
      arrLocal = obs.toArray(new MyObserver[0]);
    }

    for (final MyObserver o : arrLocal) {
      o.update(this, arg);
    }
  }

  /**
   * Tests if this object has changed.
   *
   * @return {@code true} if and only if the {@code setChanged}
   * method has been called more recently than the
   * {@code clearChanged} method on this object;
   * {@code false} otherwise.
   * @see MyObservable#clearChanged()
   * @see MyObservable#setChanged()
   */
  public synchronized boolean hasChanged()
  {
    return changed;
  }

  /**
   * Indicates that this object has no longer changed, or that it has
   * already notified all of its observers of its most recent change,
   * so that the {@code hasChanged} method will now return {@code false}.
   * This method is called automatically by the
   * {@code notifyObservers} methods.
   *
   * @see java.util.Observable#notifyObservers()
   * @see java.util.Observable#notifyObservers(java.lang.Object)
   */
  protected synchronized void clearChanged()
  {
    changed = false;
  }

  /**
   * Clears the observer list so that this object no longer has any observers.
   */
  public synchronized void deleteObservers()
  {
    obs.clear();
  }

  /**
   * Marks this {@code Observable} object as having been changed; the
   * {@code hasChanged} method will now return {@code true}.
   */
  protected synchronized void setChanged()
  {
    changed = true;
  }

  /**
   * Returns the number of observers of this {@code Observable} object.
   *
   * @return the number of observers of this object.
   */
  public synchronized int countObservers()
  {
    return obs.size();
  }
}
