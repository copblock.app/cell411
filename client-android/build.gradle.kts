@file:Suppress("RedundantSemicolon")

buildscript {
  repositories {
    google();
    mavenCentral();
    gradlePluginPortal();
  }
  dependencies {
    classpath("com.android.tools.build:gradle:8.0.2")
  }
}

tasks.register("hello") {
  doLast {
    println("Hello, World!");
  }
}

tasks.register("install-secrets") {
  val secretsFile = file("cell411/src/main/res/values/secrets.xml")
  val secretsTemplateFile = file("../keys/secrets.xml.empty")

  if (!secretsFile.exists()) {
    secretsTemplateFile.copyTo(secretsFile)
    println("Secrets file installed.")
  }
}
fun dispProp(k: String, s: Boolean){
  val t : String;
  val v : String;
  if(s) {
    t="System";
    v=""+System.getProperty(k);
  } else {
    t="Project";
    v = ""+project.property(k);
  }
  println(String.format("%8s :: %20s => %s",t,k,v));
}
task("props") {
  doLast {
    dispProp("java.version",true);
    dispProp("android.enableJetifier",false);

  }
}
//tasks.named(":cell411:buildDependents").configure {
//  dependsOn("install-secrets")
//}
//
//tasks.named(":cell411:buildDependents").configure {
//  dependsOn("install-secrets")
//}
tasks.register("uploadApk") {
  dependsOn(":cell411:assembleDebug") // Change to "assembleRelease" if needed

  doLast {
    val apkFile = file("cell411/build/outputs/apk/debug/cell411-debug.apk")
    // Adjust the file path as per your project structure
    val remoteDirectory = "dev:/opt/nginx/html/copblock/download"
    // Adjust the server details and remote directory path

    exec {
      commandLine(
        "scp", apkFile.absolutePath,
        "$remoteDirectory/cell411-debug.apk"
      )
    }

    println("APK uploaded successfully.")
  }
}
