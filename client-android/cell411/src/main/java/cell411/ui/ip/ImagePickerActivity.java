package cell411.ui.ip;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.CallSuper;
import androidx.core.util.Pair;
import cell411.ui.base.BaseActivity;
import cell411.utils.ImageUtils;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.XLog;
import cell411.utils.reflect.XTAG;
import com.safearx.cell411.R;

import javax.annotation.Nullable;
import java.io.File;

/**
 * Created by Sachin on 11/1/2015.
 */
public class ImagePickerActivity
  extends BaseActivity
{
  private static final XTAG TAG = new XTAG();

  static {
    XLog.i(TAG, "loading class");
  }

  ActivityResultContracts.TakePicture mTakePicture =
    new ActivityResultContracts.TakePicture();
  ActivityResultContracts.GetContent mGetContent =
    new ActivityResultContracts.GetContent();
  private TextView txtComments;
  private PicPrefs mPicPrefs;
  ActivityResultLauncher<Uri> mTPLauncher =
    registerForActivityResult(mTakePicture, this::tpCallback);
  ActivityResultLauncher<String> mGCLauncher =
    registerForActivityResult(mGetContent, this::gcCallback);
  private Button btnPickPic;
  private Button btnTakePic;

  public ImagePickerActivity()
  {
    super(R.layout.fragment_image_picker);
  }

  void tpCallback(final Boolean success)
  {
    if (!success) {
      showToast("Failed to take picture");
      return;
    }
    mPicPrefs.mBitmap = ImageUtils.loadCameraImage(mPicPrefs.mFile);
    mPicPrefs.mFile = null;
    ThreadUtil.onMain(this::gotBitmap);
  }

  public void gotBitmap()
  {
    Intent intent = new Intent();
    int index = ImagePickerContract.checkPicPrefs(mPicPrefs);
    intent.putExtra("index", index);
    setResult(RESULT_OK, intent);
    finish();
  }

  private void gcCallback(final Uri uri)
  {
    if (uri != null) {
      mPicPrefs.mUri = uri;
      mPicPrefs.mBitmap = ImageUtils.loadGalleryImage(uri);
      ThreadUtil.onMain(this::gotBitmap);
    }
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    btnPickPic = findViewById(R.id.btn_pick_pic);
    btnPickPic.setOnClickListener(this::onButtonClick);
    btnTakePic = findViewById(R.id.btn_take_pic);
    btnTakePic.setOnClickListener(this::onButtonClick);
    txtComments = findViewById(R.id.txt_comments);
    txtComments.setText(R.string.choosing_method);
    Intent intent = getIntent();
    PicPrefs prefs = new PicPrefs();
    prefs.updateFromIntent(intent);
    mPicPrefs = prefs;
  }

  @CallSuper
  @Override
  protected void onActivityResult(int requestCode, int resultCode,
                                  @Nullable Intent data)
  {
    super.onActivityResult(requestCode, resultCode, data);
  }

  private void onButtonClick(View view)
  {
    if (view == btnTakePic) {
      txtComments.setText(R.string.taking_new_picture);
      Pair<File, Uri> pair =
        ImageUtils.createUriAndFile(mPicPrefs.mBaseName, ".dev.file.provider");
      if (pair == null) {
        showToast("Failed to get file and url");
        return;
      }
      mPicPrefs.mFile = pair.first;
      mPicPrefs.mUri = pair.second;
      mTPLauncher.launch(mPicPrefs.mUri);
    } else if (view == btnPickPic) {
      txtComments.setText(R.string.picking_from_gallery);
      mGCLauncher.launch(mPicPrefs.mMimeType);
    } else {
      showAlertDialog("alert",
        "I don't know what that view you clicked is for.  :(");
    }
  }
}

