package cell411.ui.ip;

import static android.app.Activity.RESULT_OK;

import android.content.Context;
import android.content.Intent;

import androidx.activity.result.contract.ActivityResultContract;

import java.util.ArrayList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ImagePickerContract
  extends ActivityResultContract<PicPrefs, Integer>
{
  static ArrayList<PicPrefs> smPicPrefs = new ArrayList<>();
  private PicPrefs mPrefs;

  public ImagePickerContract()
  {
  }

  public static PicPrefs claimPicPref(final int index)
  {
    PicPrefs res = smPicPrefs.get(index);
    smPicPrefs.set(index, null);
    return res;
  }

  public static int checkPicPrefs(final PicPrefs picPrefs)
  {
    int index = smPicPrefs.size();
    smPicPrefs.add(picPrefs);
    return index;
  }

  @Nonnull
  public Intent createIntent(@Nonnull Context context, PicPrefs prefs)
  {
    mPrefs = prefs;
    Intent intent = new Intent(context, ImagePickerActivity.class);
    prefs.addToIntent(intent);
    return intent;
  }

  @Override
  public Integer parseResult(int resultCode, @Nullable Intent intent)
  {
    if (resultCode == RESULT_OK) {
      int index = -1;
      if (intent != null) {
        index = intent.getIntExtra("index", index);
      }
      if (index < 0) {
        index = smPicPrefs.size();
        smPicPrefs.add(mPrefs);
      }
      return index;
    } else {
      return null;
    }
  }
}
