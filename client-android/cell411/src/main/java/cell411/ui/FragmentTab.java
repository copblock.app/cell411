package cell411.ui;

import static cell411.ui.FragmentTab.Size.TAB_HEIGHT_ACTIVE;
import static cell411.ui.FragmentTab.Size.TAB_HEIGHT_INACTIVE;
import static cell411.ui.FragmentTab.Size.TAB_MARGIN_TOP;
import static cell411.ui.FragmentTab.Size.TAB_WIDTH;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cell411.ui.alert.TabAlertFragment;
import cell411.ui.base.BaseFragment;
import cell411.ui.base.FragmentFactory;
import cell411.ui.cell.TabCellFragment;
import cell411.ui.chat.TabChatFragment;
import cell411.ui.friend.TabFriendFragment;
import cell411.ui.alert.TabMapFragment;
import cell411.utils.ImageUtils;
import cell411.utils.Util;
import cell411.utils.collect.Collect;

public class FragmentTab
  extends FragmentFactory
{
  static final int[] tab_title =
    new int[]{R.string.app_name, R.string.title_friends, R.string.title_cells,
      R.string.title_alerts, R.string.title_chats};
  static final int[] tab_layout_ids =
    new int[]{R.id.rl_tab_map, R.id.rl_tab_friends, R.id.rl_tab_cells,
      R.id.rl_tab_alerts, R.id.rl_tab_chats};
  static final int[] tab_image =
    new int[]{R.id.img_tab_map, R.id.img_tab_friends, R.id.img_tab_cells,
      R.id.img_tab_alerts, R.id.img_tab_chats};
  static final int[] tab_label =
    new int[]{R.id.txt_tab_map, R.id.txt_tab_friends, R.id.txt_tab_cells,
      R.id.txt_tab_alerts, R.id.txt_tab_chats};
  static final int[] tab_sel_draw =
    new int[]{R.drawable.tab_map_selected, R.drawable.tab_friend_selected,
      R.drawable.tab_cell_selected, R.drawable.tab_alert_selected,
      R.drawable.tab_chat_selected};
  static final int[] tab_uns_draw =
    new int[]{R.drawable.tab_map_unselected, R.drawable.tab_friend_unselected,
      R.drawable.tab_cell_unselected, R.drawable.tab_alert_unselected,
      R.drawable.tab_chat_unselected};
  static final List<FragmentTab> mFragmentTabs = new ArrayList<>();
  static final float mDensity = ImageUtils.getDensity();
  static final List<Class<? extends BaseFragment>> tab_types =
    Collect.asList(TabMapFragment.class, TabFriendFragment.class,
      TabCellFragment.class, TabAlertFragment.class, TabChatFragment.class);
  //  private static final TAG TAG = new TAG();
  static RelativeLayout.LayoutParams smSelLayoutParams;
  static RelativeLayout.LayoutParams smUnsLayoutParams;

  //  static {
  //    XLog.i(TAG, "loading class");
  //  }
  //
  public final int mIndex;
  String mTitle;
  Class<? extends BaseFragment> mType;
  ImageView mImage;
  TextView mLabel;
  RelativeLayout mRelativeLayout;
  int mDrawSelected;
  int mDrawUnselected;
  BaseFragment mFragment = null;
  private int mRelativeLayoutId;
  private int mImageId;
  private int mLabelId;

  public FragmentTab(int index)
  {
    mIndex = index;
  }

  static void setupTabBar(final MainFragment mainFragment)
  {
    if (mFragmentTabs.isEmpty()) {
      final FragmentTab[] tabs = new FragmentTab[tab_types.size()];

      int o = 0;
      for (int i = 0; i < tabs.length; i++) {
        int j = (i + o);
        FragmentTab tab = new FragmentTab(i);
        tabs[i] = tab;
        tab.mTitle = Util.getString(tab_title[j]);
        //tab.mType           = EmptyFragment.class; //tab_types.get(i);
        tab.mType = tab_types.get(j);
        tab.mDrawSelected = tab_sel_draw[j];
        tab.mDrawUnselected = tab_uns_draw[j];
        tab.mRelativeLayoutId = tab_layout_ids[j];
        tab.mImageId = tab_image[j];
        tab.mLabelId = tab_label[j];
      }

      mFragmentTabs.addAll(Collect.wrapArray(tabs));
    }
//    System.out.println("mFragmentTabs populated");
    for (FragmentTab tab : mFragmentTabs) {
      tab.mRelativeLayout = mainFragment.findViewById(tab.mRelativeLayoutId);
      tab.mImage = mainFragment.findViewById(tab.mImageId);
      tab.mLabel = mainFragment.findViewById(tab.mLabelId);
      List<View> views =
        Arrays.asList(tab.mImage, tab.mLabel, tab.mRelativeLayout);
      for (View control : views) {
        control.setOnClickListener(mainFragment);
        control.setTag(tab.mIndex);
      }
      tab.setSelected(false);
    }
    System.out.println("views adjusted");
  }

  public void setSelected(boolean selected)
  {
    checkParams();
    if (mImage != null) {
      int drawRes = selected ? mDrawSelected : mDrawUnselected;
      if (drawRes != 0) {
        mImage.setImageResource(drawRes);
      }
    }

    int typefaceRes = selected ? Typeface.BOLD : Typeface.NORMAL;
    int textColor = selected ? Color.WHITE : 0xfff1f1f1;

    mLabel.setTextColor(textColor);
    mLabel.setTypeface(Typeface.defaultFromStyle(typefaceRes));

  }

  private void checkParams()
  {
    if (smSelLayoutParams == null || smUnsLayoutParams == null) {
      if (mImage != null) {
        LayoutParams params = (LayoutParams) mImage.getLayoutParams();
        if (smSelLayoutParams == null) {
          smSelLayoutParams = makeParams(params, true);
        }
        if (smUnsLayoutParams == null) {
          smUnsLayoutParams = makeParams(params, false);
        }
      }
    }
  }

  private <X extends RelativeLayout.LayoutParams> X makeParams(X params,
                                                               boolean selected)
  {
    if (mImage == null) {
      return null;
    }
    int top = selected ? 0 : TAB_MARGIN_TOP.getSize();

    params.height =
      (selected ? TAB_HEIGHT_ACTIVE : TAB_HEIGHT_INACTIVE).getSize();
    params.width = TAB_WIDTH.getSize();
    params.setMargins(0, top, 0, 0);
    mImage.setLayoutParams(params);
    return params;
  }

  public static FragmentTab get(int selectedIdx)
  {
    return mFragmentTabs.get(selectedIdx);
  }

  //  public void setSelected(boolean selected) {
  //    if (mImage != null) {
  //      RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
  //      mImage
  //      .getLayoutParams();
  //      params.setMargins(0, 0, 0, 0);
  //      params.width  = TAB_WIDTH;
  //      params.height = TAB_HEIGHT_ACTIVE;
  //      mImage.setLayoutParams(params);
  //    }
  //    if (mImage != null) {
  //      RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
  //      mImage
  //      .getLayoutParams();
  //      params.setMargins(0, TAB_MARGIN_TOP, 0, 0);
  //      params.width  = TAB_WIDTH;
  //      params.height = TAB_HEIGHT_INACTIVE;
  //      mImage.setLayoutParams(params);
  //    }
  //       if (mDrawSelected != 0 && mImage != null)
  //        mImage.setImageResource(mDrawSelected);
  //      if (mLabel != null) {
  //        mLabel.setTypeface(mLabel.getTypeface(), Typeface.BOLD);
  //        mLabel.setTextColor(Color.WHITE);
  //      }
  //      //      getMainActivity().setTitle(mTitle);
  //    } else {
  //      if (mDrawSelected != 0 && mImage != null)
  //        mImage.setImageResource(mDrawUnselected);
  //      if (mLabel != null) {
  //        mLabel.setTextColor(Color.parseColor(COLOR_DIRTY_WHITE));
  //        mLabel.setTypeface(mLabel.getTypeface(), Typeface.NORMAL);
  //      }
  //    }
  //    if (getFragment() != null)
  //      getFragment().setSelected(selected);
  //  }
  public BaseFragment getFragment()
  {
    return mFragment;
  }

  public void setFragment(BaseFragment fragment)
  {
    mFragment = fragment;
  }

  @Override
  public BaseFragment doCreate()
  {
    try {
      return mFragment = mType.newInstance();
    } catch (Exception e) {
      Cell411.req().handleException("selecting tab", e);
      return null;
    }
  }

  @Override
  public String getTitle()
  {
    return mTitle;
  }

  enum Size
  {
    TAB_HEIGHT_INACTIVE((int) (30 * mDensity + 0.5f)),
    TAB_HEIGHT_ACTIVE((int) (50 * mDensity + 0.5f)),
    TAB_WIDTH((int) (30 * mDensity + 0.5f)),
    TAB_MARGIN_TOP((int) (20 * mDensity + 0.5f));
    final int mSize;

    Size(int size)
    {
      mSize = size;
    }

    int getSize()
    {
      return mSize;
    }
  }

}
