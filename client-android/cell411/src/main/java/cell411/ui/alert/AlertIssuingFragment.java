package cell411.ui.alert;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cell411.enums.ProblemType;
import cell411.logic.CellWatcher;
import cell411.logic.LiveQueryService;
import cell411.model.*;
import cell411.model.util.XItem;
import cell411.services.LocationService;
import cell411.ui.base.BaseApp;
import cell411.ui.base.ModelBaseFragment;
import cell411.ui.widget.ProblemTypeInfo;
import cell411.utils.LocationUtil;
import cell411.utils.Util;
import cell411.utils.collect.Collect;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseQuery;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

import static android.content.Context.MODE_PRIVATE;
import static cell411.enums.ProblemType.Photo;
import static cell411.enums.ProblemType.Video;
import static cell411.utils.ViewType.*;

/**
 * Created by Sachin on 21-03-2018.
 */
public class AlertIssuingFragment
  extends ModelBaseFragment
{
  private static final XTAG TAG = new XTAG();
  final String[] smPerms =
    new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
  private final XItem mGlobal =
    makeItem("global", R.string.audience_global_alert);
  private final XItem mCells = makeItem("allCells", R.string.audience_cells);
  private final XItem mFriends =
    makeItem("allFriends", R.string.audience_friends);
  private final AudienceListAdapter mListAdapter = new AudienceListAdapter();
  private final List<XPrivateCell> mPrivateCells = new ArrayList<>();
  private final List<XPublicCell> mOwnedCells = new ArrayList<>();
  private final List<XPublicCell> mJoinedCells = new ArrayList<>();
  SharedPreferences mAudience =
    Cell411.req().getSharedPreferences("audience", MODE_PRIVATE);
  SharedPreferences.Editor mAudienceState = mAudience.edit();
  private ProblemTypeInfo mProblemTypeInfo;
  private ProblemType mProblemType;
  private RecyclerView mRecyclerView;
  private EditText mEtAdditionalNote;
  private TextView mTxtAlertTitle;
  private FrameLayout mMainLayout;
  private SwitchMaterial mStreamVideo;

  public AlertIssuingFragment()
  {
    super(R.layout.fragment_alert_issuing);
  }

  static XItem makeItem(String id, int res)
  {
    return new XItem(id, Util.getString(res));
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    View result = super.onCreateView(inflater, container, savedInstanceState);
    Bundle arguments = getArguments();
    if (arguments != null) {
      String problemType = arguments.getString("problemType");
      mProblemType = ProblemType.fromString(problemType);
      mProblemTypeInfo = ProblemTypeInfo.valueOf(mProblemType);
      if (mProblemTypeInfo == null) {
        showAlertDialog("alert", "Error:  no problem type set");
      }
    }
    return result;
  }

  @Override
  public void onViewCreated(@Nonnull final View view,
                            @Nullable final Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    //    Bundle extras = getIntent().getExtras();

    //    String str    = extras.getString("problemType");
    //    if (str == null)
    //      str = extras.getString("type");
    //    if (problemTypeStr == null) {
    //      mProblemType = ProblemType.values()[0];
    //      mProblemTypeInfo = ProblemTypeInfo.values()[0];
    //    } else {
    //      mProblemTypeInfo = ProblemTypeInfo.fromString(problemTypeStr);
    //      mProblemType = mProblemTypeInfo.getType();
    //    }

    mMainLayout = findViewById(R.id.rl_main_container);
    TextView btnCancel = findViewById(R.id.txt_btn_cancel);
    btnCancel.setOnClickListener(this::cancelClicked);
    TextView btnSend = findViewById(R.id.txt_btn_send);
    btnSend.setOnClickListener(this::sendClicked);
    mTxtAlertTitle = findViewById(R.id.txt_alert_title);
    mEtAdditionalNote = findViewById(R.id.et_additional_note);
    mEtAdditionalNote.setHint(R.string.enter_alert_additional_note);
    mRecyclerView = findViewById(R.id.rv_audience);
    mRecyclerView.setAdapter(mListAdapter);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(BaseApp.req().getCurrentActivity()));
    mStreamVideo = findViewById(R.id.switch_stream_video);
    mMainLayout.setBackgroundColor(mProblemTypeInfo.getBackgroundColor());
    mTxtAlertTitle.setText(mProblemTypeInfo.getTitle());
  }

  private void cancelClicked(View view)
  {
    finish();
  }

  private void sendClicked(View view)
  {
    sendAlert();
  }

  private void sendAlert()
  {
      BaseApp.getDataServer();

      ThreadUtil.onExec(new AlertSender());
  }

  @Override
  public void onResume()
  {
    super.onResume();

  }

  @Override
  public void onPause()
  {
    super.onPause();
    mAudienceState.commit();
  }

  public void loadData2()
  {
    Cell411 app = Cell411.req();
    LocationService loc = app.getLocationService();
    loc.addObserver(this::onLocationChanged);
    //    final XUser user = (XUser) ParseUser.getCurrentUser();
    //    ParseQuery<XPrivateCell> privateCells = XPrivateCell.q(XPrivateCell
    //    .class);
    //    privateCells.whereEqualTo("owner",user);
    //    ParseQuery<XPublicCell> publicCells = XPublicCell.q();
    //    publicCells.whereEqualTo("owner",user);
    //    ArrayList<ParseQuery<XPublicCell>> queries = new ArrayList<>();
    //    queries.add(publicCells);
    //    publicCells=XPublicCell.q();
    //    publicCells.whereEqualTo("members",user);
    //    queries.add(publicCells);
    //    publicCells=ParseQuery.or(queries);
    //    List<XPublicCell> cells = publicCells.find();
    //    mOwnedCells=new ArrayList<>();
    //    mJoinedCells=new ArrayList<>();
    //    for(XPublicCell cell : cells) {
    //      if(cell.getOwner().hasSameId(user)) {
    //        mOwnedCells.add(cell);
    //      } else {
    //        mJoinedCells.add(cell);
    //      }
    //    }
    //    mPrivateCells=privateCells.find();
    CellWatcher<XPrivateCell> privateCellWatcher =
      lqs().getPrivateCellWatcher();
    CellWatcher<XPublicCell> publicCellWatcher = lqs().getPublicCellWatcher();
    for (XPrivateCell cell : privateCellWatcher.getValues()) {
      if (cell.ownedByCurrent()) {
        mPrivateCells.add(cell);
      }
    }
    for (XPublicCell cell : publicCellWatcher.getValues()) {
      if (cell.ownedByCurrent()) {
        mOwnedCells.add(cell);
      } else {
        mJoinedCells.add(cell);
      }
    }
    mPrivateCells.sort(Comparator.comparing(XPrivateCell::getCellType)
      .thenComparing(XPrivateCell::getName, String.CASE_INSENSITIVE_ORDER)
      .thenComparing(XPrivateCell::getObjectId));
    Comparator<XPublicCell> cmp =
      Comparator.comparing(XPublicCell::getName, String.CASE_INSENSITIVE_ORDER)
        .thenComparing(XPublicCell::getObjectId);
    mOwnedCells.sort(cmp);
    mJoinedCells.sort(cmp);
    createAudienceList();
  }

  private void onLocationChanged(Location location, Location location1)
  {
    XLog.i(TAG, "location: " + location);
  }

  LiveQueryService lqs()
  {
    return LiveQueryService.opt();
  }

  private void createAudienceList()
  {
    // Now we put it all together.
    List<XItem> list = new ArrayList<>();
    getSelected(mGlobal);
    list.add(mGlobal);
    getSelected(mFriends);
    list.add(mFriends);
    getSelected(mCells);

    if (!mFriends.isSelected()) {
      // All members of private cells are friends as well,
      // so we can leave the private cells out, if friends
      // is selected.
      list.addAll(Util.transform(mPrivateCells, XItem::new));
    }
    list.add(mCells);
    if (!mCells.isSelected()) {
      // To avoid being too confusing, we treat the Cells button
      // as selecting ALL public cells.
      list.addAll(Util.transform(mOwnedCells, XItem::new));
      list.addAll(Util.transform(mJoinedCells, XItem::new));
    }
    for (XItem item : list) {
      getSelected(item);
    }

    mListAdapter.setData(list);
  }

  private void getSelected(XItem item)
  {
    item.setSelected(
      mAudience.getBoolean(item.getObjectId(), item.isSelected()));
    putSelected(item);
  }

  void putSelected(XItem item)
  {
    mAudienceState.putBoolean(item.getObjectId(), item.isSelected());
    mAudienceState.apply();
  }

  private HashMap<String, Object> createParams(boolean video)
  {
    Map<String, Object> objAlert = new HashMap<>();
    if (mProblemType == Video) {
      video = true;
    }
    try {
      objAlert.put("problemType", mProblemType.toString());
      Cell411 cell411 = Cell411.req();
      LocationService locationService = BaseApp.req().getLocationService();
      Location location = locationService.getLocation();
      objAlert.put("location", LocationUtil.getGeoPoint(location));
      String note = mEtAdditionalNote.getText().toString().trim();
      if (!Util.isNoE(note)) {
        objAlert.put("note", note);
      }

      final XUser user = (XUser) Parse.getCurrentUser();
      if (user == null) {
        pop();
        return new HashMap<>();
      }
      if (video || mProblemType == Photo) {
        String uploadName = user.getObjectId() + "-" + Util.isoDate();
        uploadName = uploadName.replaceAll(":", "-");
        if (video) {
          uploadName = uploadName + ".flv";
          objAlert.put("video", uploadName);
        } else {
          uploadName = uploadName + ".png";
          objAlert.put("photo", uploadName);
          //          File photoName = new File(
          //            getExternalFilesDir(null).getAbsolutePath());
          //          photoName = new File(photoName, "Photos");
          //          photoName = new File(photoName, "/photo_alert.png");
          //          //FIXME: .uploadPhoto(photoName, (String) objAlert.get
          //           ("photo"));
        }
      }
      HashMap<String, Object> params = new HashMap<>();
      createAudienceList();
      ArrayList<String> arrAudience = new ArrayList<>();
      for (XItem item : mListAdapter.mItems) {
        if (!item.isSelected()) {
          continue;
        }
        arrAudience.add(item.getObjectId());
      }
      params.put("audience", arrAudience);
      params.put("alert", objAlert);
      return params;
    } catch (Exception e) {
      throw new RuntimeException("Exception in createParams: ", e);
    }
  }

  //  }
  //  private void streamVideo() {
  //    Bro411.later(() -> {
  //      final SharedPreferences prefs = Bro411.i()
  //                                             .getAppPrefs();
  //      boolean streamVideoToYTChannel = prefs.getBoolean
  //      ("StreamVideoToYouTubeChannel", false);
  //      boolean streamVideoToMyYTChannel = prefs.getBoolean
  //      ("StreamVideoToMyYouTubeChannel",
  //      false);
  //      if (streamVideoToYTChannel || streamVideoToMyYTChannel) {
  //        Location location = getLocationWatcher().getLocation();
  //        DataService.i()
  //                   .requestCity(location, address -> {
  //                     URL url = SingletonConfig.getGoLiveURL(mAlert
  //                     .getCreatedAt(), address);
  //                     Task.call(new AlertIssuingActivity.NewGoLiveApiCall
  //                     (url));
  //                   });
  //      } else {
  //        URL url = mAlert.getVideoLink();
  //        VideoStreamingActivity.start(AlertIssuingActivity.this, url);
  //      }
  //    });
  //    finish();
  //  }
  //  public void onClick(View v) {
  //    XLog.i(TAG, Reflect.currentMethodName() + " invoked.
  //    isSendAlertTapped=" +
  //    mIsSendAlertTapped);
  //    int id = v.getId();
  //    if (id == R.id.txt_btn_cancel || id == R.id.img_close) {
  //      if (mIsSendAlertTapped) {
  //        Bro411.i()
  //               .showToast(getString(R.string.please_wait));
  //      } else {
  //        finish();
  //      }
  //    } else if (id == R.id.txt_btn_send) {
  //      if (mIsSendAlertTapped) {
  //        Bro411.i()
  //               .showToast(getString(R.string.please_wait));
  //      } else {
  //        setAlert();
  //        retrieveCurrentLocationAndIssueAlert();
  //      }
  //    }
  //  }
  //  static class NewGoLiveApiCall implements Callable<Boolean> {
  //    private final URL mURL;
  //
  //    public NewGoLiveApiCall(URL url) {
  //      mURL = url;
  //    }
  //
  //    @Override
  //    public Boolean call() throws Exception {
  //      if (Util.isCurrentThread(Looper.getMainLooper())) {
  //        throw new IllegalStateException("Cannot run doInBackground on
  //        main thread");
  //      }
  //      InputStream in;
  //      try {
  //        URLConnection urlConnection = mURL.openConnection();
  //        urlConnection.setConnectTimeout(30000);
  //        urlConnection.setReadTimeout(30000);
  //        urlConnection.setDoInput(true);
  //        in = new BufferedInputStream(urlConnection.getInputStream());
  //        BufferedReader reader = new BufferedReader(new InputStreamReader
  //        (in), 8);
  //        StringBuilder sb = new StringBuilder();
  //        String line;
  //        while ((line = reader.readLine()) != null) {
  //          sb.append(line);
  //        }
  //        Bro411.i()
  //               .showAlertDialog(sb.toString());
  //        XLog.i("GoLiveApiCall response: ", sb.toString());
  //        return true;
  //      } catch (final Exception e) {
  //        XLog.i("GoLiveApiCall", "Exception: " + e);
  //        return null;
  //      }
  //    }
  //  }

  public class AlertSender
    implements Runnable
  {
    final Cell411 cell411;
    final boolean video;
    final HashMap<String, Object> params;

    public AlertSender()
    {

      cell411 = Cell411.req();
      video = mStreamVideo.isChecked();
      params = createParams(video);
    }

    public void run()
    {
      if (!Parse.isInitialized()) {
        ThreadUtil.onMain((Runnable) this, (long) 15000);
        return;
      }
      for (Object obj : params.values()) {
        assert (!(obj instanceof String));
        System.out.println(obj);
      }
      HashMap<String, Object> res = ParseCloud.run("sendAlert", params);
      if (res == null) {
        res = new HashMap<>();
      }
      if (res.containsKey("error")) {
        cell411.showAlertDialog("alert", "Error sending Alert: " + res.get("error"));
        return;
      } else if (res.containsKey("alert")) {
        String alertId = (String) res.get("alertId");
        ParseQuery<XAlert> query = XAlert.q();
        query.whereEqualTo("objectId", alertId);
        List<XAlert> result = query.find();
        XAlert alert = null;
        if (result.size() > 0) {
          alert = result.get(0);
        }
        Reflect.announce("Alert: " + alert);
      } else {
        cell411.showAlertDialog("alert", "No error, no alert!");
      }
      if (video) {
        Cell411.req().requestPermission(smPerms, (result, throwable) ->
        {
          if (throwable != null) {
            throwable.printStackTrace();
          } else if (result != null) {
            System.out.println(Collect.asArrayList(result));
          } else {
            throw new RuntimeException(
              "Either result or throwable should have been non-null");
          }
        });
      } else {
        BaseApp.req();
        ThreadUtil.onMain((Runnable) AlertIssuingFragment.this::finish,
          (long) 0);
      }

      //      URL    url    = mAlert.getVideoLink();
      //      Intent intent = new Intent(AlertIssuingActivity.this,
      //      VideoStreamingActivity.class);
      //      intent.putExtra("url", url);
      //      startActivity(intent);
    }
  }

  private class AudienceListAdapter
    extends RecyclerView.Adapter<ViewHolder>
  {
    private final ArrayList<XItem> mItems = new ArrayList<>();

    public AudienceListAdapter()
    {
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<XItem> items)
    {
      mItems.clear();
      mItems.addAll(items);
      BaseApp.req();
      if (ThreadUtil.isMainThread()) {
        notifyDataSetChanged();
      } else {
        ThreadUtil.onMain(this::notifyDataSetChanged);
      }
    }

    public XItem getItem(int position)
    {
      return mItems.get(position);
    }

    public ArrayList<XItem> getItems()
    {
      return mItems;
    }

    @Nonnull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
      LayoutInflater inflater = LayoutInflater.from(parent.getContext());
      View v = inflater.inflate(R.layout.cell_audience, parent, false);
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder,
                                 final int position)
    {
      XItem item = mItems.get(position);
      viewHolder.cbAudience.setOnCheckedChangeListener(null);
      viewHolder.cbAudience.setChecked(item.isSelected());
      viewHolder.cbAudience.setOnCheckedChangeListener(
        viewHolder::onCheckedChanged);
      //      imgExpand.setVisibility(View.GONE);
      //      imgInfo.setVisibility(View.GONE);
      viewHolder.txtAudience.setVisibility(View.VISIBLE);
      String itemText;
      String audText = "";
      if (item.getViewType() == vtString) {
        viewHolder.viewSpacer.setVisibility(View.GONE);
        itemText = item.getText();
        if (item == mCells) {
          //          audText = "???";
          //          imgExpand.setVisibility(View.VISIBLE);
        } else if (item == mFriends) {
          //FIXME audText = Util.format("(%d)", user.getFriendList().size());
          viewHolder.txtTotalAudience.setText(audText);
        } else if (item == mGlobal) {
          //          audText = "(???)";
          viewHolder.txtTotalAudience.setVisibility(View.GONE);
        }
      } else if (item.getViewType() == vtPrivateCell ||
        item.getViewType() == vtPublicCell)
      {
        viewHolder.viewSpacer.setVisibility(View.VISIBLE);
        XBaseCell cell = item.getCell();
        itemText = cell.getName();
        //        audText  = Util.format("(%d))", cell.getMemberIds().size());
      } else {
        itemText = "WTF?";
      }
      viewHolder.txtTotalAudience.setVisibility(View.GONE);
      viewHolder.txtTotalAudience.setText(audText);
      viewHolder.txtAudience.setText(itemText);
      viewHolder.txtTotalAudience.setVisibility(
        audText.length() > 0 ? View.VISIBLE : View.GONE);
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
      return mItems.size();
    }

    //    public ArrayList<XItem> getSelectedAudience() {
    //      ArrayList<XItem> res = new ArrayList<>();
    //      for (XItem item : mItems) {
    //        XLog.i(TAG, "audience: " + item);
    //        if (item.isSelected()) {
    //          res.add(item);
    //        }
    //      }
    //      return res;
    //    }
  }

  public class ViewHolder
    extends RecyclerView.ViewHolder
  {
    private final CheckBox cbAudience;
    private final TextView txtAudience;
    private final TextView txtTotalAudience;
    private final View viewSpacer;
    //    private final ImageView imgExpand;
    //    private final ImageView imgInfo;

    public ViewHolder(View view)
    {
      super(view);
      cbAudience = view.findViewById(R.id.cb_audience);
      txtAudience = view.findViewById(R.id.txt_audience);
      txtTotalAudience = view.findViewById(R.id.txt_total_audience);
      viewSpacer = view.findViewById(R.id.spacer);
      //      imgExpand        = view.findViewById(R.id.img_expand);
      //      imgInfo          = view.findViewById(R.id.img_info);
    }

    private void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
      View view = buttonView;
      while (view.getParent() != mRecyclerView) {
        view = (View) view.getParent();
      }
      int position = mRecyclerView.getChildAdapterPosition(view);
      XItem changedItem = mListAdapter.getItem(position);
      changedItem.setSelected(isChecked);
      putSelected(changedItem);

      if (changedItem == mFriends || changedItem == mCells) {
        // mFriends removes all private cells with checked,
        // and returns them when unchecked.  This does not change
        // the states of the cells ... they will stick.
        //
        // mCells does the same with public cells.
        createAudienceList();
      }
    }
  }
}
