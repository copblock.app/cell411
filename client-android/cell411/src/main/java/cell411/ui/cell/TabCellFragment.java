package cell411.ui.cell;

import android.os.Bundle;

import androidx.annotation.MainThread;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

import cell411.ui.base.FragmentFactory;
import cell411.ui.base.XSelectFragment;
import cell411.utils.reflect.Reflect;

/**
 * Created by Sachin on 18-04-2016.
 */
public class TabCellFragment
  extends XSelectFragment
{
  public TabCellFragment()
  {
  }

  public List<FragmentFactory> createFactories()
  {
    return Arrays.asList(
      FragmentFactory.fromClass(PublicCellFragment.class, "Public"),
      FragmentFactory.fromClass(PrivateCellFragment.class, "Private"),
      FragmentFactory.fromClass(PublicCellSearchFragment.class, "Search"));
  }

  @MainThread
  @Override
  public void onSaveInstanceState(@Nonnull Bundle outState)
  {
    Reflect.announce("onSaveInstanceState");
    super.onSaveInstanceState(outState);
  }
}
