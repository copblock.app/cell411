package cell411.ui.cell;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Looper;
import cell411.enums.RequestType;
import cell411.model.XPrivateCell;
import cell411.model.XPublicCell;
import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseContext;
import cell411.ui.base.EnterTextDialog;
import cell411.utils.OnCompletionListener;
import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.XLog;
import cell411.utils.reflect.XTAG;
import com.parse.Parse;
import com.parse.callback.ParseCallback1;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nullable;
import java.util.List;

public class CellDialogs
  implements BaseContext
{
  private static final XTAG TAG = new XTAG();

  public static void leaveCell(XPublicCell cell, OnCompletionListener listener)
  {
    if (Util.isCurrentThread(Looper.getMainLooper())) {
      Cell411 app = Cell411.req();

      ThreadUtil.onExec(() ->
      {
        XLog.i(TAG, "calling leaveCell on new thread");
        leaveCell(cell, listener);
      });
      return;
    }
    List<String> members = cell.getList("memberList");
    if (members != null) {
      members.remove(Parse.getCurrentUser().getObjectId());
    }
    if (listener != null) {
      BaseApp.req();
      ThreadUtil.onMain(() -> listener.done(true), (long) 0);
    }
  }

  public static void joinCell(XPublicCell publicCell, ParseCallback1 listener)
  {
    Cell411 app = Cell411.req();
      BaseApp.getDataServer()
      .handleRequest(RequestType.CellJoinRequest, publicCell, listener, null);
  }

  public static void showLeaveCellDialog(Activity context, XPublicCell cell,
                                         @Nullable
                                         OnCompletionListener listener)
  {
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setTitle("Leave Public Cell?");
    builder.setMessage("Are you sure you want to leave cell " + cell.getName());
    builder.setPositiveButton("Yes",
      (dialog, which) -> leaveCell(cell, listener));
    builder.setNegativeButton("No", (dialog, which) ->
    {
      if (listener != null) {
        listener.done(false);
      }
    });
    AlertDialog dialog = builder.create();
    dialog.show();
    XLog.i(TAG, "Exiting showLeaveCellDialog");
  }

  public static void showDeleteCellDialog(Activity context, XPublicCell cell,
                                          OnCompletionListener listener)
  {
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setMessage(context.getString(R.string.dialog_msg_delete_public_cell,
      cell.getName()));
    alert.setNegativeButton(R.string.cancel, Util.nullClickListener());
    alert.setPositiveButton(R.string.dialog_btn_ok, (dialogInterface, i) ->
    {
      try {
        cell.delete();
        if (listener != null) {
          listener.done(true);
        }
      } catch (Exception e) {
        if (listener != null) {
          listener.done(false);
        }
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showDeleteCellDialog(Activity activity,
                                          final XPrivateCell cell,
                                          OnCompletionListener listener)
  {
    AlertDialog.Builder alert = new AlertDialog.Builder(activity);
    alert.setMessage(
      activity.getString(R.string.dialog_msg_delete_cell, cell.getName()));
    alert.setNegativeButton(R.string.dialog_btn_cancel,
      (dialogInterface, i) -> listener.done(false));
    alert.setPositiveButton(R.string.dialog_btn_ok,
      (dialogInterface, i) -> cell.deleteInBackground(e ->
      {
        if (e == null) {
          listener.done(true);
        } else {
          Cell411.req().handleException("While deleting group", e, null, true);
          listener.done(false);
        }
      }));
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showCreateNewCellDialog(Activity context)
  {
    EnterTextDialog dialog = new EnterTextDialog(context);
    dialog.setMessage(
      dialog.getString(R.string.dialog_message_create_new_cell));
    dialog.setNegativeButton(dialog.getString(R.string.dialog_btn_cancel));
    dialog.setPositiveButton(dialog.getString(R.string.dialog_btn_ok));

    dialog.setOnDismissListener(
      dialogIf -> createNewPrivateCell(dialog.getAnswer()));
    //    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    //    alert.setMessage(R.string.dialog_message_create_new_cell);
    //    LayoutInflater inflater = (LayoutInflater) context.getSystemService
    //    (Service
    //    .LAYOUT_INFLATER_SERVICE);
    //    View view = inflater.inflate(R.layout.layout_create_cell, null);
    //    final EditText etCellName = view.findViewById(R.id.et_cell_name);
    //    alert.setView(view);
    //    alert.setNegativeButton(R.string.dialog_btn_cancel, (dialog, arg1)
    //    -> dialog.dismiss());
    //    alert.setPositiveButton(R.string.dialog_btn_ok, (dialog, which) -> {
    //      final String cellName = etCellName.getText()
    //                                        .toString()
    //                                        .trim();
    //      if (cellName.isEmpty()) {
    //        showToast(R.string.please_enter_cell_name);
    //        return;
    //      }
    //      createNewPrivateCell(context, cellName);
    //    });
    //    AlertDialog dialog = alert.create();
    //    dialog.show();
  }

  public static void createNewPrivateCell(final String cellName)
  {
    final XPrivateCell cellObject = new XPrivateCell();
    cellObject.put("owner", Parse.getCurrentUser());
    cellObject.put("name", cellName);
    ThreadUtil.onExec(()-> {
      try {
        cellObject.save();
        Cell411.req().showToast(R.string.cell_added_successfully);
      } catch ( Exception e ) {
        Cell411.req().handleException("While saving cell", e, null, true);
      }
    });
  }
}

