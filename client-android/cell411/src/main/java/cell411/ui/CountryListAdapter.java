package cell411.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.safearx.cell411.R;

import java.util.ArrayList;

import javax.annotation.Nonnull;

import cell411.model.CountryInfo;
import cell411.ui.widget.UtilityMethods;

public class CountryListAdapter
  extends ArrayAdapter<CountryInfo>
{
  private final ArrayList<CountryInfo> mData;
  private final LayoutInflater mInflator;
  private final int mHighlightColor;

  public CountryListAdapter(Context context, ArrayList<CountryInfo> data)
  {
    super(context, R.layout.cell_country, data);
    UtilityMethods.initializeCountryCodeList(data);
    data.add(0, new CountryInfo("", "", ""));
    mData = data;
    mInflator = LayoutInflater.from(context);
    mHighlightColor = getContext().getColor(R.color.highlight_color_light);
  }

  @Override
  public View getView(final int position, View convertView, ViewGroup parent)
  {
    CountryInfo item = mData.get(position);
    //    convertView = super.getView(position,convertView,parent);
    if (convertView == null) {
      convertView =
        mInflator.inflate(R.layout.cell_country_code, parent, false);
      convertView.setTag(new ItemViewHolder(convertView));
    }
    ItemViewHolder holder = (ItemViewHolder) convertView.getTag();
    if (item.shortCode == null) {
      holder.txtCountryCode.setText(item.name);
      holder.txtCountryCode.setTextColor(mHighlightColor);
    } else {
      final String text = "+" + item.dialingCode;
      holder.txtCountryCode.setText(text);
    }
    return convertView;
  }

  @Override
  public View getDropDownView(int position, View convertView,
                              @Nonnull ViewGroup parent)
  {
    //    convertView = super.getDropDownView(position,convertView,parent);
    if (convertView == null) {
      convertView = mInflator.inflate(R.layout.cell_country, parent, false);
      new ItemViewHolder(convertView);
    }
    ItemViewHolder holder = (ItemViewHolder) convertView.getTag();
    if (holder == null) {
      holder = new ItemViewHolder(convertView);
    }

    CountryInfo item = mData.get(position);

    if (item.shortCode == null) {
      holder.txtCountryName.setText(item.name);
      holder.imgFlag.setImageBitmap(null);
      holder.imgTick.setVisibility(View.GONE);
    } else {
      final String text = item.name + " +" + item.dialingCode;
      holder.txtCountryName.setText(text);
      holder.imgFlag.setImageResource(item.flagId);
      if (item.selected) {
        holder.imgTick.setVisibility(View.VISIBLE);
      } else {
        holder.imgTick.setVisibility(View.GONE);
      }
    }
    return convertView;
  }

  private static class ItemViewHolder
  {
    TextView txtCountryName;
    TextView txtCountryCode;
    ImageView imgFlag;
    ImageView imgTick;

    public ItemViewHolder(final View convertView)
    {
      txtCountryName = convertView.findViewById(R.id.txt_country_name);
      txtCountryCode = convertView.findViewById(R.id.txt_country_code);
      imgFlag = convertView.findViewById(R.id.img_flag);
      imgTick = convertView.findViewById(R.id.img_tick);
      convertView.setTag(this);
    }
  }
}
