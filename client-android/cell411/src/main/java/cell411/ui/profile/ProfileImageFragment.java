package cell411.ui.profile;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import cell411.utils.Util;

import com.parse.model.ParseObject;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.imgstore.ImageStore;
import cell411.config.ConfigDepot;
import cell411.model.XUser;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.MainActivity;
import cell411.ui.base.ModelBaseFragment;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.collect.ValueObserver;
//import cell411.utils.ImageFactory;

public class ProfileImageFragment
  extends ModelBaseFragment
{
  static FragmentFactory smFragmentFactory =
    FragmentFactory.fromClass(ProfileImageFragment.class);
  XUser mUser;
  private ImageView mImageView;
  private TextView mTxtNoProfilePic;

  public ProfileImageFragment()
  {
    super(R.layout.fragment_image);
  }

  public static View.OnClickListener getOpener(XUser user)
  {
    return new Opener(user);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    Bundle arguments = Util.req(getArguments());
    String objectId = arguments.getString("objectId");
    mUser = ParseObject.getIfExists(objectId);
    if (!mUser.hasProfileImage()) {
      finish();
      return;
    }
    mImageView = findViewById(R.id.image);
    mTxtNoProfilePic = findViewById(R.id.txt_no_profile_picture);
    mTxtNoProfilePic.setVisibility(View.VISIBLE);
    mImageView.setVisibility(View.VISIBLE);
    mTxtNoProfilePic.setVisibility(View.GONE);
    ImageStore.req().setupImage(mUser, mImageView);
    mImageView.setOnClickListener(new OnClickListener()
    {
      @Override
      public void onClick(final View v)
      {
        pop();
      }
    });
  }

  static class Opener
    implements OnClickListener
  {
    final XUser mUser;

    Opener(XUser user)
    {
      mUser = user;
    }

    @Override
    public void onClick(View v)
    {
      smFragmentFactory.setObjectId(mUser.getObjectId());
      MainActivity activity = Cell411.req().getMainActivity();
      activity.push(smFragmentFactory);
    }
  }

//  private class BitmapValueObserver
//    implements ValueObserver<Bitmap>,
//               Runnable
//  {
//    private Drawable mNewValue;
//
//    @Override
//    public void onChange(@Nullable Bitmap newValue, @Nullable Bitmap oldValue)
//    {
//      mNewValue = newValue;
//      ThreadUtil.onMain(this);
//    }
//
//    public void run()
//    {
//      if (mNewValue == null)
//        mNewValue = ConfigDepot.getPlaceHolder();
//      mImageView.setImageBitmap(mNewValue);
//    }
//  }
}

