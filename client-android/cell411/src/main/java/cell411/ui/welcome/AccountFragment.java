package cell411.ui.welcome;

import android.os.Bundle;
import android.view.View;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.ui.base.BaseFragment;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.XSelectFragment;
import cell411.utils.collect.Collect;
import cell411.utils.reflect.Reflect;

public class AccountFragment
  extends XSelectFragment
{
  public AccountFragment()
  {
    Reflect.announce("Entering AccountFragment constructor");
  }

  @Override
  public List<FragmentFactory> createFactories()
  {
    return super.createFactories();
  }

  public List<Class<? extends BaseFragment>> getTypes()
  {
    List<Class<? extends BaseFragment>> list = super.getTypes();
    list.addAll(Collect.asArrayList(GalleryFragment.class, LoginFragment.class,
      RegisterFragment.class));
    return list;
  }

  @Override
  public int getHeaderVisibility()
  {
    return View.GONE;
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);

  }

}
