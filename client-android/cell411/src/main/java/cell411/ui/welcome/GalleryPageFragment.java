package cell411.ui.widget;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.ui.base.BaseFragment;

/**
 * Created by Sachin on 15-04-2016.
 */
public class GalleryPageFragment
  extends BaseFragment
{
  static TypedArray imageArray =
    Cell411.req().getResources().obtainTypedArray(R.array.gallery_images);
  static String[] titlesArray =
    Cell411.req().getResources().getStringArray(R.array.gallery_titles);
  static String[] descArray =
    Cell411.req().getResources().getStringArray(R.array.gallery_desc);
  private int imageId;
  private String title;
  private String desc;
  private int index;

  public GalleryPageFragment()
  {
    super(R.layout.fragment_gallery_page);
  }

  //  static GalleryImageFragment makeFragment(int arg0) {
  //    Bundle               args     = new Bundle();
  //    args.putString("title", titlesArray[arg0]);
  //    args.putString("desc", descArray[arg0]);
  //    args.putInt("imageId", imageArray.getResourceId(arg0, -1));
  //
  //    GalleryImageFragment fragment = new GalleryImageFragment();
  //    fragment.setArguments(args);
  //    return fragment;
  //  }
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    Bundle arguments = getArguments();
    index = 0;
    if (arguments != null) {
      index = arguments.getInt("index");
    }
    imageId = imageArray.getResourceId(index, -1);
    title = titlesArray[index];
    desc = descArray[index];
    ImageView imgGallery = view.findViewById(R.id.img_gallery);
    TextView txtTitle = view.findViewById(R.id.lbl_title);
    TextView txtDesc = view.findViewById(R.id.lbl_description);
    txtTitle.setText(title);
    txtDesc.setText(desc);
    imgGallery.setImageResource(imageId);

    imgGallery.setImageResource(imageId);
    txtTitle.setText(title);
    txtDesc.setText(desc);
    //    View      avatar     = view.findViewById(R.id.avatar);
    //    View      avatarBG   = view.findViewById(R.id.view_avatag_bg);
    //    if (index > 0) { // Cell 411 or gallery page other than 1
    //      if (view instanceof ViewGroup) {
    //        ViewGroup group = (ViewGroup) view;
    //        group.removeView(avatar);
    //        group.removeView(avatarBG);
    //      } else {
    //        if(avatar!=null)
    //          avatar.setVisibility(View.GONE);
    //        if(avatarBG!=null)
    //          avatarBG.setVisibility(View.GONE);
    //      }
    //    } else {
    //      if(avatar!=null)
    //        avatar.setVisibility(View.VISIBLE);
    //      if(avatarBG!=null)
    //        avatarBG.setVisibility(View.VISIBLE);
    //    }

  }
}

