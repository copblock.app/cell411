package cell411.ui.nav;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.model.ParseUser;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.annotation.Nullable;

import cell411.config.ConfigDepot;
import cell411.imgstore.ImageStore;
import cell411.imgstore.ImageStoreConnection;
import cell411.model.XUser;
import cell411.ui.base.BaseContext;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.Util;
import cell411.utils.collect.ValueObserver;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

public class NavHeaderMain
  extends RelativeLayout
  implements BaseContext,
             Runnable,
             ValueObserver<Bitmap>
{
  private final static XTAG TAG = new XTAG();
  private ImageStoreConnection mImageStoreConnection;
  private ImageView imgUser;
  private TextView txtName;
  private TextView txtEmail;
  private TextView txtBloodGroup;
  private TextView txtVersion;
  private Future<ImageStore> mImageStoreFuture;

  public NavHeaderMain(Context context)
  {
    this(context, null);
    mImageStoreConnection = new ImageStoreConnection(context);
    mImageStoreFuture = ThreadUtil.submit(mImageStoreConnection);
  }

  public NavHeaderMain(Context context, AttributeSet attrs)
  {
    this(context, attrs, 0);
  }

  public NavHeaderMain(Context context, AttributeSet attrs, int defStyleAttr)
  {
    this(context, attrs, defStyleAttr, 0);
  }

  public NavHeaderMain(Context context, AttributeSet attrs, int defStyleAttr,
                       int defStyleRes)
  {
    super(context, attrs, defStyleAttr, defStyleRes);
  }

  @Override
  protected void onAttachedToWindow()
  {
    Reflect.announce(this);
    super.onAttachedToWindow();
  }

  @Override
  protected void onDetachedFromWindow()
  {
    Reflect.announce(this);
    super.onDetachedFromWindow();
  }

  public void run()
  {
    if (ThreadUtil.isMainThread()) {
      ThreadUtil.onExec(this);
      return;
    }
    try {
      XUser user = (XUser) Parse.getUserWaiter().get();
      if (imgUser == null) {
        imgUser = findViewById(R.id.avatar);
        txtName = findViewById(R.id.name);
        txtEmail = findViewById(R.id.txt_email);
        txtBloodGroup = findViewById(R.id.txt_blood_group);
        txtVersion = findViewById(R.id.txt_version);
      }
      if (user == null) {
        setVisibility(GONE);
        return;
      }
      setVisibility(VISIBLE);
      ImageStore.req().setupImage(user, imgUser);
      if (txtName != null) {
        txtName.setText(Util.nvl(user.getName(), "no name"));
      }
      if (txtEmail != null) {
        txtEmail.setText(Util.nvl(user.getEmail(), "no email"));
      }
      if (txtBloodGroup != null) {
        txtBloodGroup.setText(Util.nvl(user.getBloodType(), "N/A"));
      }
      if (txtVersion != null) {
        txtVersion.setText(getVersion());
      }
    } catch (ExecutionException | InterruptedException e) {
      handleException("Setting Up Nav View", e);
    }
  }

  public String getVersion()
  {
    Cell411.req();
    return ConfigDepot.getAppVersion();
  }

  public void onChange(@Nullable Bitmap newValue, @Nullable Bitmap oldValue)
  {
    ParseUser user = Parse.getCurrentUser();
    XLog.e(TAG,"FIXME!  Images disabled");
//    Bitmap bitmap = null;
//    if (user != null) {
//      XUser xUser = (XUser) user;
//      if (Util.theGovernmentIsLying())
//        throw new RuntimeException();
//      else
//        bitmap = xUser.getAvatarPic();
//    }
//    if (bitmap == null) {
//      bitmap = ImageStore.req().getPlaceHolder();
//    }
//    Bitmap finalBitmap = bitmap;
//    Runnable runnable = () -> imgUser.setImageBitmap(finalBitmap);
//    if (ThreadUtil.isMainThread())
//      runnable.run();
//    else
//      ThreadUtil.onMain(runnable);
  }
}
