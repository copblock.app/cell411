package cell411.ui.nav;

import android.content.Intent;
import android.net.Uri;
import android.view.MenuItem;

import androidx.annotation.IdRes;

import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener;
import com.parse.model.ParseUser;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import java.util.HashMap;

import javax.annotation.Nonnull;

import cell411.android.DroidUtil;
import cell411.config.ConfigDepot;
import cell411.logic.LiveQueryService;
import cell411.ui.base.BaseContext;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.MainActivity;
import cell411.ui.profile.ProfileViewFragment;
import cell411.ui.settings.SettingsFragment;
import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;

public class NavListener
  implements OnNavigationItemSelectedListener,
             BaseContext
{
  final MainActivity mMainActivity;
  IdRunMap mIdRunMap = new IdRunMap();
  FragmentFactory mKnowYourRightsFactory =
    FragmentFactory.fromLayout("Know Your Rights",
      R.layout.activity_know_your_rights);
  FragmentFactory mAboutFactory =
    FragmentFactory.fromClass(AboutFragment.class);

  {
    new LogOut();
    new KnowYourRights();
    new FaqAndTutorials();
    new Notifications();
    new GenerateQR();
    new MyProfile();
    new Settings();
    new ShareThisApp();
    new ChangePassword();
    new AboutCell411();
  }

  public NavListener(MainActivity mainActivity)
  {
    mMainActivity = mainActivity;
  }

  String className(Object obj)
  {
    if (obj == null) {
      return Void.class.getSimpleName();
    } else {
      return obj.getClass().getSimpleName();
    }
  }

  @Override
  public boolean onNavigationItemSelected(@Nonnull MenuItem item)
  {
    int id = item.getItemId();
    IdRunnable runnable = mIdRunMap.get(id);
    ThreadUtil.onMain(runnable);
    mMainActivity.closeDrawer();
    return false;
  }

  private void startActivity(Intent chooser)
  {
    mMainActivity.startActivity(chooser);
  }

  private void push(FragmentFactory settingsFactory)
  {
    getMainActivity().push(settingsFactory);
  }

  private MainActivity getMainActivity()
  {
    return mMainActivity;
  }

  interface IdRunnable
    extends Runnable
  {
    @IdRes
    int id();
  }

  static class IdRunMap
    extends HashMap<Integer, IdRunnable>
  {
    public void put(IdRunnable idRunnable)
    {
      Object res = put(idRunnable.id(), idRunnable);
      if (res != null && res != idRunnable) {
        System.out.println("Warning:  duplicate tag!");
      }
    }
  }

  abstract class ActionBase
    implements IdRunnable
  {
    ActionBase()
    {
      mIdRunMap.put(this);
    }
  }

  class LogOut
    extends ActionBase
    implements IdRunnable
  {
    public @IdRes int id()
    {
      return R.id.nav_logout;
    }

    @Override
    public void run()
    {
      LiveQueryService service = LiveQueryService.opt();
      if (service != null) {
        service.clear();
      }
      ParseUser.logOut();
    }
  }

  class Notifications
    extends ActionBase
    implements IdRunnable
  {
    public @IdRes int id()
    {
      return R.id.nav_notifications;
    }

    @Override
    public void run()
    {

    }
  }

  class KnowYourRights
    extends ActionBase
    implements IdRunnable
  {
    public @IdRes int id()
    {
      return R.id.nav_know_your_rights;
    }

    @Override
    public void run()
    {
      push(mKnowYourRightsFactory);
    }
  }

  class ShareThisApp
    extends ActionBase
    implements IdRunnable
  {
    public @IdRes int id()
    {
      return R.id.nav_share_this_app;
    }

    @Override
    public void run()
    {
      Intent sharingIntent = new Intent(Intent.ACTION_SEND);
      sharingIntent.setType("text/plain");
      Cell411.req();
      String version = ConfigDepot.getAppVersion();
      String object3 = DroidUtil.getString(R.string.share_app_subject);
      sharingIntent.putExtra(Intent.EXTRA_SUBJECT,
        Util.req(object3).replace("num",
          version));
      String object1 = DroidUtil.getString(R.string.app_url);
      String object2 = Util.getString(R.string.share_app_text);
      sharingIntent.putExtra(Intent.EXTRA_TEXT,
        Util.req(object2).replace("link",
          Util.req(object1)));
      String object = DroidUtil.getString(R.string.share_app_title);
      String title = Util.req(object);
      startActivity(Intent.createChooser(sharingIntent, title));
    }
  }

  class ReloadCache
    extends ActionBase
    implements IdRunnable
  {
    public @IdRes int id()
    {
      return R.id.reloadCache;
    }

    @Override
    public void run()
    {
      LiveQueryService.req().clear();
    }
  }

  class FaqAndTutorials
    extends ActionBase
    implements IdRunnable
  {
    public @IdRes int id()
    {
      return R.id.nav_faq_and_tutorials;
    }

    @Override
    public void run()
    {
      Intent intentWeb = new Intent(Intent.ACTION_VIEW);
      String object = DroidUtil.getString(R.string.faq_url);
      intentWeb.setData(
        Uri.parse(Util.req(object)));
      startActivity(intentWeb);
    }
  }

  class AboutCell411
    extends ActionBase
    implements IdRunnable
  {
    public @IdRes int id()
    {
      return R.id.nav_about;
    }

    @Override
    public void run()
    {
      mMainActivity.push(mAboutFactory);
    }
  }

  class ChangePassword
    extends ActionBase
    implements IdRunnable
  {
    public @IdRes int id()
    {
      return R.id.nav_change_password;
    }

    @Override
    public void run()
    {

    }
  }

  class MyProfile
    extends ActionBase
    implements IdRunnable
  {
    FragmentFactory mProfileViewFactory =
      FragmentFactory.fromClass(ProfileViewFragment.class);

    public @IdRes int id()
    {
      return R.id.nav_my_profile;
    }

    @Override
    public void run()
    {
      push(mProfileViewFactory);
    }
  }

  class GenerateQR
    extends ActionBase
    implements IdRunnable
  {
    public @IdRes int id()
    {
      return R.id.nav_generate_qr;
    }

    @Override
    public void run()
    {
      showAlertDialog("alert", "FIXME:  still need to do qr codes!");
    }
  }

  class Settings
    extends ActionBase
    implements IdRunnable
  {
    FragmentFactory mSettingsFactory =
      FragmentFactory.fromClass(SettingsFragment.class);

    public @IdRes int id()
    {
      return R.id.nav_settings;
    }

    @Override
    public void run()
    {
      push(mSettingsFactory);
    }
  }
}
