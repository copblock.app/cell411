package cell411.ui.nav;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatDelegate;

import cell411.ui.base.BaseApp;

import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.ui.base.BaseActivity;
import cell411.ui.base.BaseFragment;

public class AboutFragment
  extends BaseFragment
  implements View.OnClickListener
{
  private WebView wvAbout;
  private boolean mIsLoading = false;

  public AboutFragment()
  {
    super(R.layout.fragment_about);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    final ImageView imgPrevious = findViewById(R.id.img_previous);
    final ImageView imgForward = findViewById(R.id.img_forward);
    final ImageView imgRefresh = findViewById(R.id.img_refresh);
    imgPrevious.setOnClickListener(this);
    imgForward.setOnClickListener(this);
    imgRefresh.setOnClickListener(this);
    final ColorFilter colorFilterActive = mkFilter(R.color.highlight_color);
    final ColorFilter colorFilterInActive = mkFilter(R.color.gray_666);
    wvAbout = findViewById(R.id.wv_about);
    wvAbout.getSettings().setJavaScriptEnabled(true);
    wvAbout.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
    wvAbout.getSettings().setDatabaseEnabled(true);
    wvAbout.getSettings().setDomStorageEnabled(true);
    //wvAbout.getSettings().setLoadWithOverviewMode(true);

        /*
        wvAbout.getSettings().setUseWideViewPort(true);
        wvAbout.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm
        .SINGLE_COLUMN);
        wvAbout.getSettings().setPluginState(WebSettings.PluginState.ON);
        wvAbout.setBackgroundColor(Color.argb(1, 0, 0, 0));
        wvAbout.getSettings().setBuiltInZoomControls(true);*/
    wvAbout.setWebViewClient(new WebViewClient()
    {
      public boolean shouldOverrideUrlLoading(WebView view, String url)
      {
        if ((url.contains("http") || url.contains("market://") ||
          url.contains("mailto:") || url.contains("play.google") ||
          url.contains("tel:") || url.contains("vid:")))
        {
          // Load new URL Don't override URL Link
          url = url.replace("file:///android_asset/", "");
          view.getContext()
            .startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
          return true;
        } else {
          // do your handling codes here, which url is the requested url
          // probably you need to open that url rather than redirect:
          view.loadUrl(url);
          return false; // then it is not handled by default action
        }
      }

      @Override
      public void onPageStarted(WebView view, String url, Bitmap favicon)
      {
        super.onPageStarted(view, url, favicon);
        mIsLoading = true;
        imgRefresh.setImageResource(R.drawable.ic_web_cancel);
      }

      public void onPageFinished(WebView view, String url)
      {
        imgRefresh.setImageResource(R.drawable.ic_web_refresh);
        mIsLoading = false;
        if (wvAbout.canGoBack()) {
          imgPrevious.setColorFilter(colorFilterActive);
        } else {
          imgPrevious.setColorFilter(colorFilterInActive);
        }
        if (wvAbout.canGoForward()) {
          imgForward.setColorFilter(colorFilterActive);
        } else {
          imgForward.setColorFilter(colorFilterInActive);
        }
      }
    });
    if (AppCompatDelegate.getDefaultNightMode() ==
      AppCompatDelegate.MODE_NIGHT_NO)
    {
      wvAbout.loadUrl("file:///android_asset/about_en.html");
    } else {
      wvAbout.loadUrl("file:///android_asset/about_en_night.html");
    }
  }

  @Nonnull
  private PorterDuffColorFilter mkFilter(int highlight_color)
  {
    return new PorterDuffColorFilter(getColor(highlight_color),
      PorterDuff.Mode.MULTIPLY);
  }

  public int getColor(int highlight_color)
  {
      BaseActivity activity = BaseApp.req().getCurrentActivity();
    if (activity == null) {
      return 0;
    }
    return getResources().getColor(highlight_color, activity.getTheme());
  }

  @Override
  public void onClick(View view)
  {
    int id = view.getId();
    if (id == R.id.img_previous) {
      wvAbout.goBack();
    } else if (id == R.id.img_refresh) {
      if (mIsLoading) {
        wvAbout.stopLoading();
      } else {
        wvAbout.reload();
      }
    } else if (id == R.id.img_forward) {
      wvAbout.goForward();
    }
  }
}