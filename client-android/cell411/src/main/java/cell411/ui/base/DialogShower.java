package cell411.ui.base;

import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import cell411.methods.Dialogs;
import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.XTAG;

public abstract class DialogShower
  implements DialogInterface.OnClickListener,
             Runnable,
             BaseContext
{
  private static final XTAG TAG = new XTAG();
  protected AlertDialog mDialog;
  protected Boolean mPositive = null;
  protected Integer mWhich = null;
  protected String mText = null;
  protected BaseActivity mActivity;

    {
        BaseApp.req();
        mActivity = BaseApp.req().getCurrentActivity();
    }

    boolean mStarted = false;
  private boolean mCancelable = true;
  private int mThemeResId = android.R.style.Theme_Material_Dialog_Alert;

  public DialogShower()
  {
    System.out.println("Dialog Shower created");
  }

  public int getThemeRes()
  {
    return mThemeResId;
  }

  public void setThemeResId(int themeResId)
  {
    mThemeResId = themeResId;
  }

  public Integer getWhich()
  {
    return mWhich;
  }

  public Boolean getPositive()
  {
    return mPositive;
  }

  boolean success()
  {
    return mPositive == Boolean.TRUE;
  }

  @Override
  public void run()
  {
    BaseApp.req();
    if (!ThreadUtil.isMainThread()) {
      ThreadUtil.onMain(this, 1000);
      return;
    }
    if (!mStarted) {
      if (mActivity == null) {
        BaseApp app = BaseApp.req();
        mActivity = app.getCurrentActivity();
      }
      if (mActivity == null) {
        ThreadUtil.onMain(this, 1000);
        return;
      }
      mDialog = createDialog();
      Dialogs.getShowing().add(mDialog);
      mDialog.show();
      mStarted = true;
    } else if (mDialog.isShowing()) {
      ThreadUtil.onMain((Runnable) this, (long) 1000);
    } else {
      int num = 0;
      while (Dialogs.getShowing().remove(mDialog)) {
        num++;
      }
      System.out.println(Util.format("Removed %d time(s)", num));
    }
  }

  protected abstract AlertDialog createDialog();

  @Override
  public void onClick(DialogInterface dialog, int which)
  {
    //    assert (dialog == mDialog);
    if (which == DialogInterface.BUTTON_POSITIVE) {
      mPositive = true;
    } else if (which == DialogInterface.BUTTON_NEGATIVE) {
      mPositive = false;
    }
    mWhich = which;
  }

  public String getText()
  {
    return mText;
  }

  public void dismiss()
  {
    mDialog.dismiss();
  }

  public boolean getCancelable()
  {
    return mCancelable;
  }

  public void setCancelable(final boolean cancelable)
  {
    mCancelable = cancelable;
  }
}
