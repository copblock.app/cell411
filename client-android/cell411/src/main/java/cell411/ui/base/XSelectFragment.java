package cell411.ui.base;

import static cell411.utils.Util.constrain;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.MainThread;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.concurrent.ThreadUtil;

public class XSelectFragment
  extends BaseFragment
{
  private final List<FragmentFactory> mFactories = new ArrayList<>();
  FragmentFactory mPanicFactory =
    FragmentFactory.fromClass(LayoutFragment.class);

  private View header;
  private Button btnPrev;
  private Button btnNext;
  private TextView txtTitle;
  private int mIndex = -1;

  public XSelectFragment(@LayoutRes int layout)
  {
    super(layout);
    setFactories(createFactories());
  }

  public XSelectFragment()
  {
    this(R.layout.fragment_selectx);
  }

  public List<FragmentFactory> createFactories()
  {
    return FragmentFactory.fromLayout(getTypes());
  }

  public List<Class<? extends BaseFragment>> getTypes()
  {
    return new ArrayList<>();
  }

  public int getHeaderVisibility()
  {
    return getFactories().size() < 2 ? View.GONE : View.VISIBLE;
  }

  int nextPrev(int mIndex, int btn, int items)
  {
    mIndex = constrain(0, mIndex, items - 1);
    mIndex += btn;
    mIndex = constrain(0, mIndex, items - 1);
    return mIndex;
  }

  private void onNextPrevClick(View view)
  {
    setIndex(constrain(getIndex(), 0, getFactories().size() - 1));
    assert view == btnPrev || view == btnNext;
    setIndex(
      nextPrev(getIndex(), view == btnPrev ? -1 : 1, getFactories().size()));
    assert (getIndex() >= 0 &&
      (getFactories().size() == 0 || getIndex() < getFactories().size()));
    selectFragment();
  }

  public int color(boolean enabled, boolean foreground)
  {
    return (enabled == foreground) ? 0xffffffff : 0xff000000;
  }

  public void selectFragment(int index)
  {
    if (index >= mFactories.size()) {
      index = mFactories.size() - 1;
    }
    if (index < 0) {
      index = 0;
    }
    mIndex = index;
    selectFragment();
  }

  public void selectFragment()
  {
    if (!ThreadUtil.isMainThread() || !isAdded()) {
      ThreadUtil.onMain((Runnable) this::selectFragment, (long) 100);
      return;
    }
    FragmentManager fm = getChildFragmentManager();
    assert fm != null;
    FragmentTransaction tx = fm.beginTransaction();
    FragmentFactory toAdd = null;
    for (int i = 0; i < mFactories.size(); i++) {
      FragmentFactory factory = mFactories.get(i);
      if (i == getIndex()) {
        toAdd = factory;
      } else {
        BaseFragment fragment = factory.get(false);
        if (fragment != null && fragment.isAdded()) {
          tx.remove(fragment);
        }
      }
    }
    if (toAdd != null) {
      BaseFragment fragment = toAdd.get(true);
      tx.replace(R.id.pager, fragment);
    }
    if (txtTitle != null) {
      txtTitle.setText(getTitle());
    }
    tx.commitNowAllowingStateLoss();
    enableButton(btnPrev, getIndex() > 0);
    enableButton(btnNext, getIndex() < getFactories().size() - 1);
  }

  @Nonnull
  //  public List<FragmentFactory> getFactoryList(int index) {
  //    int size = getFactories().size();
  //    if (index < 0 || index >= size)
  //      throw new ArrayIndexOutOfBoundsException(Util.format("size=%d
  //      index=%d",size,index));
  //    return getFactories().get(index);
  //  }
  public FragmentFactory getFactory(int index)
  {
    if (index == 0 && getFactories().size() == 0) {
      return mPanicFactory;
    } else {
      return mFactories.get(index);
    }
  }

  public int indexOf(FragmentFactory factory)
  {
    return mFactories.indexOf(factory);
  }

  public void selectFragment(final @Nonnull FragmentFactory factory)
  {
    int pos = indexOf(factory);
    if (pos < 0) {
      mFactories.add(factory);
      pos = indexOf(factory);
    }
    selectFragment(pos);
  }

  @SuppressWarnings("deprecation")
  @MainThread
  @CallSuper
  @Override
  public void onAttach(@Nonnull Activity activity)
  {
    super.onAttach(activity);
  }

  @MainThread
  @CallSuper
  @Override
  public void onCreate(@Nullable Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
  }

  @MainThread
  @CallSuper
  @Override
  public void onAttach(@Nonnull Context context)
  {
    super.onAttach(context);
  }

  @Override
  @CallSuper
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    assert isAdded();
    if (mIndex < 0 || mIndex >= mFactories.size()) {
      this.selectFragment(0);
    }
    header = view.findViewById(R.id.header);
    btnNext = view.findViewById(R.id.btn_next);
    btnPrev = view.findViewById(R.id.btn_prev);
    txtTitle = view.findViewById(R.id.page_title);

    if (btnNext != null || btnPrev != null) {
      if (btnNext == null || btnPrev == null) {
        showAlertDialog("alert",
          "If you set either btnPrev or btnNext, you should do both.");
      }
      btnNext.setOnClickListener(this::onNextPrevClick);
      btnPrev.setOnClickListener(this::onNextPrevClick);
    }
    if (getIndex() < 0 || getIndex() >= getFactories().size()) {
      setIndex(0);
    }

    if (header != null) {
      header.setVisibility(getHeaderVisibility());
    }

    selectFragment();
  }

  @CallSuper
  @Override
  public void onResume()
  {
    super.onResume();
    enableButton(btnPrev, getIndex() > 0);
    enableButton(btnNext, getIndex() < getFactories().size() - 1);
  }

  public void enableButton(Button btn, boolean enabled)
  {
    if (btn == null) {
      return;
    }
    btn.setEnabled(enabled);
  }

  public int getIndex()
  {
    return mIndex;
  }

  public List<FragmentFactory> getFactories()
  {
    return mFactories;
  }

  public <X extends FragmentFactory, C extends Collection<X>> void setFactories(
    @Nonnull C c)
  {
    List<FragmentFactory> factories = getFactories();
    factories.clear();
    factories.addAll(c);
    if (isAdded() && (mIndex < 0 || mIndex >= factories.size())) {
      selectFragment(0);
    }
  }

  public void setIndex(int index)
  {
    mIndex = index;
  }

  @MainThread
  @CallSuper
  @Override
  public void onDetach()
  {
    super.onDetach();
  }

  public void setLastReloadTime(int which)
  {

  }

  public String getTitle()
  {
    return getCurrentFactory() == null ? "(null)" :
      getCurrentFactory().getTitle();
  }

  public FragmentFactory getCurrentFactory()
  {
    return getFactory(mIndex);
  }

  public FragmentFactory getFactory(Class<? extends BaseFragment> type)
  {
    for (FragmentFactory factory : mFactories) {
      if (factory.isForClass(type)) {
        return factory;
      }
    }
    return null;
  }

  public void selectFragment(final Class<? extends BaseFragment> type)
  {
    FragmentFactory factory = getFactory(type);
    if (factory == null) {
      mFactories.add(FragmentFactory.fromClass(type));
    }
    selectFragment(mFactories.indexOf(factory));
  }
}
