package cell411.ui.base;

public interface FragmentStackListener
{
  void onFragmentPushed(FragmentFrame fragmentFrame, FragmentFactory factory);
  void onFragmentPopped(FragmentFrame fragmentFrame, FragmentFactory factory);
}
