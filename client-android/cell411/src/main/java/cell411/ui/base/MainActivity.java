package cell411.ui.base;

import android.os.Bundle;

import com.safearx.cell411.R;

import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.methods.Dialogs;
import cell411.model.XEntity;
import cell411.utils.OnCompletionListener;
import cell411.utils.reflect.Reflect;

abstract public class MainActivity
  extends BaseActivity
  implements OnCompletionListener,
             FragmentStackListener
{
  long mDebounceTime = 0;
  private FragmentFrame mFragmentFrame;
  private final long mDebounceDelay = 500;

  public MainActivity(int layout)
  {
    super(layout);
  }
  public void addFragmentStackListener(FragmentStackListener listener)
  {
    getFragmentFrame().addFragmentStackListener(listener);
  }

  public abstract void closeDrawer();

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    mFragmentFrame = findViewById(R.id.id_fragment_frame);
    addFragmentStackListener(this);
    getFragmentFrame().setFragMan(getSupportFragmentManager());
  }

  abstract public void openChat(XEntity cell);

  @Nonnull
  public FragmentFrame getFragmentFrame()
  {
    return Objects.requireNonNull(mFragmentFrame);
  }

  public void popAll()
  {
    getFragmentFrame().popAll();
  }

  public final boolean pop()
  {
    synchronized (this) {
      return getFragmentFrame().pop();
    }
  }

  public void onFragmentPushed(FragmentFactory factory, boolean before)
  {
  }

  public void push(FragmentFactory factory)
  {
    push(factory, false, false);
  }

  public void push(FragmentFactory factory, boolean pop, boolean all)
  {
    synchronized (this) {
      getFragmentFrame().push(factory, pop, all);
    }
  }

  public void setDrawerIndicatorEnabled(final boolean b)
  {
  }

  boolean debounceIgnore()
  {
    long now = System.currentTimeMillis();
    if (now < mDebounceTime)
      return true;
    mDebounceTime = now + mDebounceDelay;
    return false;
  }
  @Override
  public final void onBackPressed()
  {
    if(debounceIgnore())
      Reflect.announce("ignored:  debounce");
    else if(onChildBackPressed())
      Reflect.announce("child handled");
    else if(getFragmentFrame().onBackPressed())
      Reflect.announce("fragment frame popped");
    else {
      Reflect.announce("Checking if user wants to exit.");
      Dialogs.showYesNoDialog("Do you want to exit?", this);
    }
  }

  public void done(boolean result) {
    if(result)
      super.onBackPressed();
  }
  /**
   *
   * childOnBackPressed():  give the derived class a crack at thee
   * event.
   *
   * @return true if parent class should ignore the event.
   *
   */
  public boolean onChildBackPressed()
  {
    return false;
  }

  @Override
  public void onFragmentPushed(FragmentFrame fragmentFrame,
                               FragmentFactory factory)
  {

  }

  @Override
  public void onFragmentPopped(FragmentFrame fragmentFrame,
                               FragmentFactory factory)
  {
  }
}