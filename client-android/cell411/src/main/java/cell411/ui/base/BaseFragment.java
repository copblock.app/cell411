package cell411.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.MainThread;
import androidx.fragment.app.Fragment;

import com.safearx.cell411.Cell411;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.services.LocationService;
import cell411.utils.Util;
import cell411.utils.reflect.Reflect;

public abstract class BaseFragment
  extends Fragment
  implements BaseContext
{
  private static final boolean verbose = false;

  public BaseFragment(@LayoutRes int layout)
  {
    super(layout);
  }

  @MainThread
  @CallSuper
  @Override
  public void onAttach(@Nonnull Context context)
  {
    super.onAttach(context);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull LayoutInflater inflater,
                           @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState)
  {
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
  }

  @CallSuper
  public void onResume()
  {
    super.onResume();
    hideSoftKeyboard();
    //    refresh();
  }

  @MainThread
  @Override
  public void onSaveInstanceState(@Nonnull Bundle outState)
  {
    Reflect.announce("" + this.getClass());
    super.onSaveInstanceState(outState);
  }

  @CallSuper
  public void onPause()
  {
    super.onPause();
  }

  @MainThread
  @CallSuper
  @Override
  public void onDetach()
  {
    super.onDetach();
  }

  protected LocationService getLocationService()
  {
    Cell411 app = Cell411.req();
    return app.getLocationService();
  }

  public int getColor(final int colorAccent)
  {
    return requireActivity().getColor(colorAccent);
  }

  protected <V extends View> V findViewById(final int id, boolean req)
  {
    if (req) {
      V object = findViewById(id);
      return Util.req(object);
    } else {
      return findViewById(id);
    }
  }

  public <V extends View> V findViewById(final int id)
  {
    return requireView().findViewById(id);
  }

  public String getTitle()
  {
    return Util.makeWords(getClass().getSimpleName());
  }

  @SuppressWarnings("unused")
  public void finish(final View ignoredView)
  {
    finish();
  }

  public void finish()
  {
    pop();
  }

  public boolean pop()
  {
    MainActivity activity = (MainActivity) BaseApp.req().getCurrentActivity();
    if (activity == null)
      return false;
    if(activity.getFragmentFrame().peek()==this) {
      return activity.pop();
    } else {
      return false;
    }
  }

  public BaseActivity activity()
  {
    return (BaseActivity) getActivity();
  }

  public boolean onBackPressed()
  {
    return true;
  }

  public final void push(FragmentFactory factory)
  {
    push(factory, false, false);
  }

  public final void push(FragmentFactory factory, boolean b, boolean b1)
  {
    MainActivity activity = (MainActivity) BaseApp.req().getCurrentActivity();
    if (activity != null) {
      activity.push(factory, b, b1);
    }
  }

}
