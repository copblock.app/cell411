package cell411.ui.base;

import android.content.Context;
import android.util.AttributeSet;

import com.safearx.cell411.R;

import cell411.utils.reflect.XTAG;

public class XAvatar
  extends BaseAnim
{
  public static final XTAG TAG = new XTAG();
  static int[] frames =
    new int[]{R.drawable.gif_avatar_100_0, R.drawable.gif_avatar_100_1,
      R.drawable.gif_avatar_100_2, R.drawable.gif_avatar_100_3};

  public XAvatar(Context context)
  {
    this(context, null);
  }

  public XAvatar(Context context, AttributeSet attrs)
  {
    this(context, attrs, 0);
  }

  public XAvatar(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
  }

  public int[] getFrameIds()
  {
    return frames;
  }

  public int getDuration()
  {
    return 4000;
  }
}

