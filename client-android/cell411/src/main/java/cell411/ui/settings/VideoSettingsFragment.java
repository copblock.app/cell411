package cell411.ui.settings;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.CallSuper;

import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.ui.base.ModelBaseFragment;

public class VideoSettingsFragment
  extends ModelBaseFragment
{
  public VideoSettingsFragment()
  {
    super(R.layout.fragment_video_settings);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);

  }

  @CallSuper
  @Override
  public void onResume()
  {
    super.onResume();
  }

  @CallSuper
  @Override
  public void onPause()
  {
    super.onPause();
  }
}
