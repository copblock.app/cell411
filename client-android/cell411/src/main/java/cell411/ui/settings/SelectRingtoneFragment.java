package cell411.ui.settings;

import static android.media.RingtoneManager.ACTION_RINGTONE_PICKER;
import static android.media.RingtoneManager.EXTRA_RINGTONE_EXISTING_URI;
import static android.media.RingtoneManager.EXTRA_RINGTONE_PICKED_URI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.IdRes;

import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import java.util.Arrays;
import java.util.TreeMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.android.RingtoneData;
import cell411.android.RingtoneData.Tone;
import cell411.ui.base.BaseApp;
import cell411.ui.base.ModelBaseFragment;
import cell411.utils.reflect.Reflect;

public class SelectRingtoneFragment
  extends ModelBaseFragment
{
  final Button[] mPickButtons = new Button[]{null, null, null};
  final TextView[] mTextViews = new TextView[]{null, null, null};
  final Button[] mFauxButtons = new Button[]{null, null, null};
  final PickRingtone mContract = new PickRingtone();
  final ActCallBack mCallback = new ActCallBack();
  final ActivityResultLauncher<RingtoneData.Tone> mLauncher =
    registerForActivityResult(mContract, mCallback);
  private final String[] mKeys = getKeys();
  Button mDone;
  public @Nonnull BaseApp mApp = BaseApp.req();
  int mIdx = -1;
  private View mView;

  public SelectRingtoneFragment()
  {
    super(R.layout.select_ringtone_fragment);
  }

  private String[] getKeys()
  {
    assert mApp != null;
    return mApp.getRingtoneData().getTones().keySet().toArray(new String[0]);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull final View view,
                            @Nullable final Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    mView = view;
    mDone = mView.findViewById(R.id.btn_done);
    mDone.setOnClickListener(this::onDoneButtonClicked);
    restore();
  }

  private void onDoneButtonClicked(final View view)
  {
    finish();
  }

  public void restore()
  {
    setup(0, R.id.txt_alert_ringtone, R.id.btn_alert_ringtone,
      R.id.btn_faux_alert);
    setup(1, R.id.txt_request_ringtone, R.id.btn_request_ringtone,
      R.id.btn_faux_request);
    setup(2, R.id.txt_chat_ringtone, R.id.btn_chat_ringtone,
      R.id.btn_faux_chat);
  }

  public void setup(int idx, @IdRes int txt, @IdRes int btn, @IdRes int faux)
  {
    String text = "";
    String key = mKeys[idx];
    RingtoneData data = getData();
    Tone tone = data.getTones().get(key);
    if (tone != null) {
      text = tone.getTitle();
    }

    mPickButtons[idx] = mView.findViewById(btn);
    mTextViews[idx] = mView.findViewById(txt);
    mFauxButtons[idx] = mView.findViewById(faux);
    Button pickButton, fauxButton;
    TextView textView;

    Arrays.asList(pickButton = mPickButtons[idx], textView = mTextViews[idx],
      mFauxButtons[idx]).forEach((view) -> view.setTag(idx));

    pickButton.setTag(idx);
    textView.setTag(idx);
    textView.setText(text);
    pickButton.setOnClickListener(this::onButtonClicked);
  }

  public RingtoneData getData()
  {
    return mApp.getRingtoneData();
  }

  private void onButtonClicked(final View view)
  {
    mIdx = (int) (Integer) view.getTag();
    String key = mKeys[mIdx];
    mLauncher.launch(getData().getTones().get(key));
  }

  public static class PickRingtone
    extends ActivityResultContract<RingtoneData.Tone, Uri>
  {
    @Nonnull
    @Override
    public Intent createIntent(@Nonnull Context context,
                               @Nullable Tone ringtoneData)
    {
      Intent intent = new Intent(ACTION_RINGTONE_PICKER);
      if (ringtoneData != null) {
        intent.putExtra(EXTRA_RINGTONE_EXISTING_URI, ringtoneData.getUri());
      }
      return intent;
    }

    @Override
    public Uri parseResult(int resultCode, @Nullable Intent result)
    {
      if (resultCode != Activity.RESULT_OK || result == null) {
        return null;
      }

      if (VERSION.SDK_INT >= VERSION_CODES.TIRAMISU) {
        return result.getParcelableExtra(EXTRA_RINGTONE_PICKED_URI, Uri.class);
      } else {
        //noinspection deprecation
        return result.getParcelableExtra(EXTRA_RINGTONE_PICKED_URI);
      }
    }
  }

  class ActCallBack
    implements ActivityResultCallback<Uri>
  {
    @Override
    public void onActivityResult(final Uri result)
    {
      if (result == null) {
        return;
      }
      RingtoneData data = mApp.getRingtoneData();
      data.addRingtoneData(mKeys[mIdx],result);
      restore();

    }
  }
}
