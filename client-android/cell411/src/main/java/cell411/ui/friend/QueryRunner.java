package cell411.ui.friend;

import cell411.model.XUser;
import cell411.utils.concurrent.ThreadName;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.func.Func0;
import cell411.utils.reflect.Reflect;
import com.parse.ParseQuery;

import java.util.List;

public class QueryRunner<X extends XUser> implements Runnable {
  public static final int DEFAULT_LIMIT = 20;
  static int smSerial = 0;
  final ParseQuery<X> mQuery;
  private final FriendSearchFragment mFragment;
  int mSerial = ++smSerial;
  HighWaterWatcher mHighWaterWatcher = new HighWaterWatcher();

  public QueryRunner(ParseQuery<X> query, FriendSearchFragment fragment) {
    Reflect.announce("QueryRunner #"+mSerial);
    mQuery = query;
    mQuery.setLimit(DEFAULT_LIMIT);
    mFragment = fragment;
  }

  @Override
  public void run() {
    Reflect.announce("QueryRunner #"+mSerial);
    assert !ThreadUtil.isMainThread();
    try (ThreadName ignored = new ThreadName("QueryRunner")) {
      List<X> list;
      while (mFragment.getRunner() == this) {
        mQuery.setSkip(mFragment.getUserCount());
        ThreadUtil.waitUntil(this, mHighWaterWatcher, 60000);
        if(mFragment.getRunner()!=this)
          break;
        list = mQuery.find();
        final SenderInternal internal = new SenderInternal(list);
        internal.run();
        System.out.println("Fry is back!");

        if(list.size()!=mQuery.getLimit())
          mFragment.queryComplete();
      }
    }
  }

  public ParseQuery<X> getQuery() {
    return mQuery;
  }

  private class SenderInternal implements Runnable {
    private final List<X> list;

    public SenderInternal(List<X> list) {
      this.list = list;
    }

    @Override
    public synchronized void run() {
      if (ThreadUtil.isMainThread()) {
        Reflect.announce("2:  Thread: " + Thread.currentThread());
        Reflect.announce("4:  Gonna Add Users");
        mFragment.addUsers(list);
        Reflect.announce("5:  Gonna Call Notify");
        notifyAll();
        Reflect.announce("6:  Gonna Go Home");
      } else {
        ThreadUtil.onMain(this);
        ThreadUtil.wait(this);
      }
    }
  }

  public class HighWaterWatcher implements Func0<Boolean> {
    @Override
    public Boolean apply() {
      return
        mFragment.getRunner()!=QueryRunner.this ||
        mFragment.getHighWater() +mQuery.getLimit() > mQuery.getSkip();
    }
  }
}
