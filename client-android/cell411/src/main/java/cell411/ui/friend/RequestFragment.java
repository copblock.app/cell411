package cell411.ui.friend;

import static cell411.enums.RequestType.CellJoinApprove;
import static cell411.enums.RequestType.CellJoinCancel;
import static cell411.enums.RequestType.CellJoinReject;
import static cell411.enums.RequestType.CellJoinResend;
import static cell411.enums.RequestType.FriendApprove;
import static cell411.enums.RequestType.FriendCancel;
import static cell411.enums.RequestType.FriendReject;
import static cell411.enums.RequestType.FriendResend;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cell411.ui.base.BaseApp;

import com.parse.Parse;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.enums.RequestType;
import cell411.imgstore.ImageStore;
import cell411.json.JSONObject;
import cell411.logic.LQListener;
import cell411.logic.LiveQueryService;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;
import cell411.logic.RelationWatcher;
import cell411.logic.RequestWatcher;
import cell411.logic.rel.Rel;
import cell411.methods.Dialogs;
import cell411.model.JSONCallback;
import cell411.model.XPublicCell;
import cell411.model.XRequest;
import cell411.model.XUser;
import cell411.model.util.XItem;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.ModelBaseFragment;
import cell411.ui.profile.UserFragment;
import cell411.ui.widget.RVAdapter;
import cell411.utils.Util;
import cell411.utils.ViewType;
import cell411.utils.collect.Collect;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

/**
 * Created by Sachin on 18-04-2016.
 */
public class RequestFragment
  extends ModelBaseFragment
{
  private static final XTAG TAG = new XTAG();
  static private final ItemCompare reqComp = new ItemCompare();

  static {
    XLog.i(TAG, "Loading Class");
  }

  private final RequestListAdapter mRequestListAdapter =
    new RequestListAdapter();
  private final FragmentFactory mUserFactory =
    FragmentFactory.fromClass(UserFragment.class);
  private final Rel mOwnerOf;
  private final Rel mSentToOf;
  RelationWatcher mRelationWatcher;
  private AlertDialog mDialog;
  private AlertDialog mResultDialog;
  private boolean mRequestInFlight = false;
  private final JSONCallback mCallback = new JSONCallback()
  {
    @Override
    public void done(boolean success, JSONObject result)
    {
      if (!ThreadUtil.isMainThread()) {
        ThreadUtil.onMain(() -> done(success, result));
        return;
      }
      System.out.println(mOwnerOf);
      if (mDialog != null) {
        mDialog.dismiss();
      }
      if (mResultDialog == null) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.dialog_btn_ok,
          (dialog, which) -> dialog.dismiss());
        mResultDialog = builder.create();
        mResultDialog.setOnDismissListener(dialog -> mRequestInFlight = false);
      }
      mResultDialog.setMessage(result.toString(2));
      ThreadUtil.onMain(() ->
        {
          if (mResultDialog != null)
            mResultDialog.show();
        });
    }
  };
  private TextView mTxtNoRequests;
  private final RVAdapter mAdapterObserver = new RVAdapter()
  {
    @Override
    public void onChanged()
    {
      if (mRequestListAdapter.mItems.size() > 1) {
        mTxtNoRequests.setVisibility(View.GONE);
      } else {
        mTxtNoRequests.setVisibility(View.VISIBLE);
      }
    }
  };

  public RequestFragment()
  {
    super(R.layout.fragment_friend_requests);
    RequestWatcher requestWatcher = LiveQueryService.req().getRequestWatcher();
    mOwnerOf = requestWatcher.getOwned();
    mSentToOf = requestWatcher.getReceived();
    mRequestListAdapter.registerAdapterDataObserver(mAdapterObserver);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    RecyclerView recyclerView = view.findViewById(R.id.rv_requests);
    mTxtNoRequests = view.findViewById(R.id.txt_no_requests);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    LinearLayoutManager linearLayoutManager =
      new LinearLayoutManager(getActivity());
    recyclerView.setLayoutManager(linearLayoutManager);
    mRequestListAdapter.mItems.clear();
    mRequestListAdapter.mItems.add(new XItem("", "Loading Data ..."));
    recyclerView.setAdapter(mRequestListAdapter);
    ThreadUtil.onExec(mRequestListAdapter::change);
  }

  void friendRequestAction(XRequest req, boolean ownedByMe)
  {
    String message;
    if (ownedByMe) {
      message = "Would you like to resend(yes) or cancel(no) your request?";
    } else {
      message = "Would you like to accept(yes) or reject(no) the request?";
    }
    Dialogs.showYesNoDialog("Request", message,
      success -> friendRequestAction(req, ownedByMe, success));
  }

  void friendRequestAction(XRequest req, boolean ownedByMe, boolean positive)
  {
    if (mRequestInFlight)
      return;
    mRequestInFlight = true;
    final RequestType requestType;
    if (ownedByMe) {
      if (positive) {
        if (req.getCell() == null) {
          requestType = FriendResend;
        } else {
          requestType = CellJoinResend;
        }
      } else {
        if (req.getCell() == null) {
          requestType = FriendCancel;
        } else {
          requestType = CellJoinCancel;
        }
      }
    } else {
      if (positive) {
        if (req.getCell() == null) {
          requestType = FriendApprove;
        } else {
          requestType = CellJoinApprove;
        }
      } else {
        if (req.getCell() == null) {
          requestType = FriendReject;
        } else {
          requestType = CellJoinReject;
        }
      }
    }
    String msg = Util.format("Sending %s request", requestType);
    if (mDialog == null) {
      AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
      builder.setCancelable(false);
      mDialog = builder.create();
    }
    mDialog.setMessage(msg);
    mDialog.show();
      BaseApp.getDataServer().handleRequest(requestType, req, null, mCallback);
  }

  private RelationWatcher getRelationWatcher()
  {
    if(mRelationWatcher==null) {
      LiveQueryService service = LiveQueryService.req();
      mRelationWatcher = service.getRelationWatcher();
    }
    return mRelationWatcher;
  }

  public static class RequestViewHolder
    extends RecyclerView.ViewHolder
  {
    public XUser otherUser;
    public ImageView mAvatar;
    public boolean ownedByMe;
    public TextView mUserName;
    public TextView mAction;
    public TextView txtInfo;
    public View view;
    public XPublicCell cell;

    public RequestViewHolder(View v, ViewType viewType)
    {
      super(v);
      view = v;
      switch (viewType) {
        case vtRequest:
          mAvatar = v.findViewById(R.id.avatar);
          mUserName = v.findViewById(R.id.name);
          mAction = v.findViewById(R.id.b_respond);
          break;
        case vtString:
        case vtNull:
          txtInfo = v.findViewById(R.id.name);
          break;
      }
    }
  }

  public class RequestListAdapter
    extends RecyclerView.Adapter<RequestViewHolder>
    implements LQListener<XRequest>,
               MyObserver
  {
    public final List<XItem> mItems = new ArrayList<>();
    private final XItem mOFRTitle = new XItem("", "Sent");
    private final XItem mIFRTitle = new XItem("", "Received");
    private final XItem mOCRTitle = new XItem("", "Sent Cell Reqs");
    private final XItem mICRTitle = new XItem("", "Recd Cell Reqs");
    private final List<XItem> miFReq = new ArrayList<>();
    private final List<XItem> moFReq = new ArrayList<>();
    private final List<XItem> miCReq = new ArrayList<>();
    private final List<XItem> moCReq = new ArrayList<>();
    private final List<List<XItem>> mLists =
      Collect.asList(miFReq, moFReq, miCReq, moCReq);

    public RequestListAdapter()
    {
      mItems.add(new XItem("", "Loading Data"));
    }

    @Nonnull
    public RequestViewHolder onCreateViewHolder(@Nonnull ViewGroup parent,
                                                int viewType)
    {
      View v;
      ViewType vt = ViewType.valueOf(viewType);
      switch (vt) {
        case vtRequest:
          v = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.cell_friend_request, parent, false);
          break;
        case vtNull:
        case vtString:
          v = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.cell_public_cell_title, parent, false);
          break;
        default:
          String msg = "Unexpected viewType: " + viewType;
          throw new RuntimeException(msg);
      }
      return new RequestViewHolder(v, vt);
    }

    public void onBindViewHolder(@Nonnull RequestViewHolder vh, int position)
    {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      XItem item = mItems.get(position);
      String text=null;
      switch (item.getViewType()) {
        case vtRequest:
          final XRequest req = item.getRequest();
          vh.ownedByMe = req.ownedByCurrent();
          if (vh.ownedByMe) {
            text="Resend";
            vh.otherUser = req.getSentTo();
          } else {
            text="Respond";
            vh.otherUser = req.getOwner();
          }
          vh.mAction.setText(text);
          vh.cell = req.getCell();
          if (vh.otherUser == null) {
            return;
          }

          ImageStore.req().setupImage(vh.otherUser, vh.mAvatar);

          if (vh.cell == null) {
            vh.mUserName.setText(vh.otherUser.getName());
          } else {
            vh.mUserName.setText(vh.cell.getName());
          }
          vh.mAction.setOnClickListener(
            view -> friendRequestAction(req, vh.ownedByMe));

          vh.itemView.setOnClickListener(view ->
          {
            mUserFactory.setObjectId(vh.otherUser.getObjectId());
            push(mUserFactory);
          });

          break;
        case vtString:
          text = item.getText();
          vh.txtInfo.setText(text);
          vh.txtInfo.setOnClickListener(v -> Reflect.announce("Clicked!"));
          break;
        case vtNull:
          vh.view.setVisibility(View.INVISIBLE);
          break;
      }
    }

    @Override
    public int getItemViewType(int position)
    {
      super.getItemViewType(position);
      return mItems.get(position).getViewType().ordinal();
    }

    @Override
    public int getItemCount()
    {
      return mItems.size();
    }

    @Override
    public synchronized void update(MyObservable o, Object arg)
    {
      change();
    }

    public synchronized void change()
    {
      mItems.clear();
      for (List<XItem> list : mLists) {
        list.clear();
      }
//      checkObjects(mOwnerOf, mSentToOf);

      splitList(mSentToOf, miFReq, miCReq, mIFRTitle, mICRTitle);
      splitList(mOwnerOf, moFReq, moCReq, mOFRTitle, mOCRTitle);

      mItems.clear();
      for (List<XItem> list : mLists) {
        mItems.addAll(list);
      }

      ThreadUtil.onMain(this::notifyDataSetChanged);
    }

//    private void checkObjects(Rel sentToOf, Rel toOf)
//    {
//      HashSet<String> ids = new HashSet<>();
//      ids.addAll(sentToOf.getRelatedIds());
//      ids.addAll(sentToOf.getRelatedIds());
//      HashSet<String> seen = new HashSet<>();
//      while(seen.size()<ids.size()) {
//        for(String id : ids) {
//          if(seen.contains(id))
//            continue;
//          seen.add(id);
//          ParseObject object = Parse.getObject(id);
//          System.out.println(object);
//          if(object instanceof XUser) {
//            XUser user = (XUser) object;
//            if(user.has("firstName") && user.has("lastName"))
//              continue;
//            ThreadUtil.onExec((Runnable) user::fetch);
//            // nothing to add.
//          } else if ( object instanceof XRequest ) {
//            XRequest req = (XRequest) object;
//            if(!object.has("sentTo") || req.getSentTo()==null) {
//              ThreadUtil.onExec((Runnable) object::fetch);
//              seen.remove(object.getObjectId());
//              continue;
//            }
//            if(!object.has("owner") || req.getOwner()==null) {
//              ThreadUtil.onExec((Runnable) object::fetch);
//              seen.remove(object.getObjectId());
//              continue;
//            }
//            XUser user = req.getOwner();
//            ids.add(user.getObjectId());
//            user=req.getSentTo();
//            ids.add(user.getObjectId());
//
//            ThreadUtil.onExec((Runnable) user::fetch);
//          }
//        }
//      }
//    }

    private void splitList(Rel ids, List<XItem> fList, List<XItem> cList,
                           XItem fTitle, XItem cTitle)
    {
      for (String id : ids.getRelatedIds()) {
        XRequest req = Parse.getObject(id);
        if (req == null)
          continue;
        if (req.getCell() == null) {
          fList.add(new XItem(req));
        } else {
          cList.add(new XItem(req));
        }
      }
      polish(fList, fTitle);
      polish(cList, cTitle);
    }

    private void polish(List<XItem> list, XItem title)
    {
      list.sort(reqComp);
      int count = list.size();
      String text = title.getText()+" ("+count+")";
      title = new XItem("",text);
      list.add(0, title);
    }
  }

}

