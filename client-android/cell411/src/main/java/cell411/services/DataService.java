package cell411.services;

import static cell411.enums.RequestType.CellJoinRequest;
import static cell411.enums.RequestType.FriendRequest;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Looper;

import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.callback.ParseCallback1;
import com.parse.model.ParseGeoPoint;
import com.parse.model.ParseObject;
import com.parse.model.ParseRelation;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nonnull;

import cell411.enums.RequestType;
import cell411.json.JSONObject;
import cell411.model.JSONCallback;
import cell411.model.XAddress;
import cell411.model.XChatRoom;
import cell411.model.XCity;
import cell411.model.XEntity;
import cell411.model.XPrivateCell;
import cell411.model.XPublicCell;
import cell411.model.XRequest;
import cell411.model.XUser;
import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseContext;
import cell411.utils.LocationUtil;
import cell411.utils.OnCompletionListener;
import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.XLog;
import cell411.utils.reflect.XTAG;

@SuppressWarnings("unused")
public class DataService
  extends ContextWrapper
  implements BaseContext
{
  public static final XTAG TAG = new XTAG();
  private static AtomicReference<DataService> smInstance =
    new AtomicReference<>(null);
  final HashMap<Object, XAddress> smCityCache = new HashMap<>();
  private final ArrayList<XAddress> addresses = new ArrayList<>();
  private final ArrayList<XCity> cities = new ArrayList<>();
  private final Map<String, String> smChatEntity = new HashMap<>();

  public DataService(Context app)
  {
    super(app);
    assert smInstance == null || smInstance.get() == null;
  }

  public static DataService init(BaseApp app)
  {
    smInstance = new AtomicReference<>(new DataService(app));
    return get();
  }

  public static DataService get()
  {
    return smInstance.get();
  }

  public static DataService safeGet()
  {
    return smInstance != null ? smInstance.get() : null;
  }

  public static <T extends ParseObject> List<T> findFully(ParseQuery<T> query)
  {
    ArrayList<T> res = new ArrayList<>();
    int skip = 0;
    while (true) {
      List<T> list = query.find();
      res.addAll(list);
      if (list.size() != query.getLimit()) {
        break;
      }
      skip += list.size();
      query.setSkip(skip);
    }
    return res;
  }

  public static String getResString(int resId)
  {

    DataService ds = get();
    return ds.getString(resId);
  }

  public static void removeFriend(XUser friend)
  {
    XUser current = (XUser) Parse.getCurrentUser();
    ParseRelation<XUser> r = current.getRelation("friends");
    r.remove(friend);
    ThreadUtil.onExec(current::save);
  }

  public boolean isCurrentUser(XUser owner)
  {
    if (owner.isCurrentUser()) {
      return true;
    }
    XUser current = (XUser) Parse.getCurrentUser();
    return current != null &&
      (current.getObjectId().equals(owner.getObjectId()));
  }

  public void serverIsDown()
  {
  }

  public XPublicCell getPublicCell(String id)
  {
    ParseObject result;
    synchronized (DataService.class) {

      result = get().getObject(id);
    }
    return (XPublicCell) result;
  }

  public ParseObject getObject(String objectId)
  {
    return Parse.getObject(objectId);
  }

  public void createPrivateCell(String cellName, OnCompletionListener listener)
  {
    try {
      ParseQuery<XPrivateCell> cellQuery =
        ParseQuery.getQuery(XPrivateCell.class);
      final XUser currentUser = (XUser) Parse.getCurrentUser();
      cellQuery.whereEqualTo("owner", currentUser);
      cellQuery.whereNotEqualTo("type", 5);
      cellQuery.whereEqualTo("name", cellName);
      cellQuery.setLimit(1);
      List<XPrivateCell> cells = cellQuery.find();
      if (cells.size() != 0) {
        showToast(
          "Cell " + cellName + " " + getString(R.string.already_created));
      } else {
        XPrivateCell cell = new XPrivateCell();
        cell.setOwner(currentUser);
        cell.setName(cellName);
        cell.save();
        if (listener != null) {
          listener.done(true);
        }
        showToast("created new private cell " + cellName);
      }
    } catch (Exception e) {
      if (listener != null) {
        listener.done(false);
      }
    }
  }

  public XPrivateCell getPrivateCell(String i)
  {
    ParseObject result;
    synchronized (DataService.class) {

      result = get().getObject(i);
    }
    return (XPrivateCell) result;
  }

  public void handleRequest(RequestType requestType, ParseObject object,
                            ParseCallback1 listener, JSONCallback callback)
  {
    ThreadUtil.onExec(
      new RequestHandler(requestType, object, listener, callback));
  }

  public List<XUser> getUsers(Collection<String> keys)
  {
    if (keys == null || keys.isEmpty()) {
      return new ArrayList<>();
    }
    ArrayList<XUser> dest = new ArrayList<>(keys.size());
    for (String key : keys) {
      XUser user = getUser(key);
      if (user != null) {
        dest.add(user);
      }
    }
    return dest;
  }

  //  public void handleResponse(XRequest request, boolean b, ParseCallback1
  //  listener) {
  //    RequestType rt;
  //    if (request.isCellRequest()) {
  //      if (b) {
  //        rt = RequestType.CellJoinApprove;
  //      } else {
  //        rt = RequestType.CellJoinReject;
  //      }
  //    } else {
  //      if (b) {
  //        rt = RequestType.FriendApprove;
  //      } else {
  //        rt = RequestType.FriendReject;
  //      }
  //    }
  //    onDS().post(new RequestHandler(rt, request, listener, null));
  //  }

  public XUser getUser(@Nonnull String key)
  {
    ParseObject result;
    synchronized (DataService.class) {

      result = get().getObject(key);
    }
    ParseObject object = result;
    XLog.i(TAG, "" + object);
    if (object instanceof XUser) {
      return (XUser) object;
    } else {
      XLog.i(TAG, "" + object);
      return null;
    }
  }

  public void flagUser(XUser user, boolean flagNotUnflag,
                       OnCompletionListener listener)
  {
    Runnable runnable = () ->
    {
      try {
        final XUser currentUser = (XUser) Parse.getCurrentUser();
        ParseRelation<XUser> relation = currentUser.getRelation("spamUsers");
        if (flagNotUnflag) {
          relation.add(user);
          showToast(
            getResString(R.string.blocked_successfully, user.getName()));
        } else {
          relation.remove(user);
          showToast(
            getResString(R.string.unblocked_successfully, user.getName()));
        }
        currentUser.save();
        if (listener != null) {
          ThreadUtil.onExec(()->listener.done(true));
        }
      } catch (ParseException e) {
        if (listener != null) {
          ThreadUtil.onExec(()->listener.done(true));
        }
        handleException("While blocking user", e, null, true);
      }
    };

    ThreadUtil.onExec(runnable);
  }

  public static String getResString(int resId, Object... args)
  {

    DataService ds = get();
    return ds.getString(resId, args);
  }

  public XEntity getEntity(XChatRoom xChatRoom)
  {
    ParseObject result;
    synchronized (DataService.class) {

      result = get().getObject(getEntityId(xChatRoom.getObjectId()));
    }
    return (XEntity) result;
  }

  //  public void handleRequest(RequestType requestType, XRequest req) {
  //    handleRequest(requestType, req, null, mCallback);
  //  }

  private String getEntityId(String objectId)
  {
    return smChatEntity.get(objectId);
  }

  public void setEntity(XChatRoom chatRoom, XEntity xEntity)
  {
    smChatEntity.put(chatRoom.getObjectId(), xEntity.getObjectId());
    smChatEntity.put(xEntity.getObjectId(), chatRoom.getObjectId());
  }

  public void reverseGeocode(ParseGeoPoint location, AddressListener listener)
  {
    ThreadUtil.onExec(() ->
    {
      try {
        Map<String, Object> params = new HashMap<>();
        params.put("location", location);
        params.put("type", "address");
        XAddress address = callReverseGeocode(params);
        int delay = 0;
        ThreadUtil.onMain(() -> listener.setAddress(address, null),
          (long) delay);
      } catch (Throwable error) {
        int delay = 0;
        ThreadUtil.onMain(() -> listener.setAddress(null, error), (long) delay);
      }
    });
  }

  private XAddress callReverseGeocode(Map<String, Object> params)
  {

    Map<String, Object> res = callFunction("reverseGeocode", params);
    return processGeocodeResults(res);
  }

  public Map<String, Object> callFunction(String func,
                                          Map<String, Object> params)
  {
    assert !Looper.getMainLooper().isCurrentThread();
    return ParseCloud.run(func, params);
  }

  @Nonnull
  private XAddress processGeocodeResults(Map<String, Object> res)
  {
    String address = (String) res.get("address");
    String city = (String) res.get("city");
    String state = (String) res.get("state");
    String country = (String) res.get("country");
    ParseGeoPoint minPoint = (ParseGeoPoint) res.get("minPoint");
    ParseGeoPoint maxPoint = (ParseGeoPoint) res.get("maxPoint");
    Location location =
      LocationUtil.getLocation((ParseGeoPoint) res.get("location"));
    return new XAddress(country, state, city, address, location);
  }

  public void requestAddress(ParseGeoPoint point, AddressListener listener)
  {
    Location location = LocationUtil.getLocation(point);
    callRequestGeocode(LocationUtil.getGeoPoint(location), "address", listener);
  }

  public void requestAddress(Location location, AddressListener listener)
  {
    callRequestGeocode(LocationUtil.getGeoPoint(location), "address", listener);
  }

  public void requestCity(Location location, AddressListener listener)
  {
    callRequestGeocode(LocationUtil.getGeoPoint(location), "city", listener);
  }

  public void requestCity(ParseGeoPoint location, AddressListener listener)
  {
    Objects.requireNonNull(location);
    callRequestGeocode(location, "city", listener);
  }

  void callRequestGeocode(ParseGeoPoint location, String type,
                          AddressListener listener)
  {
    Objects.requireNonNull(location);
    Objects.requireNonNull(type);
    BaseApp.req();
    if (ThreadUtil.isMainThread()) {
      ThreadUtil.onExec((() -> callRequestGeocode(location, type, listener)));
    } else {
      GeocodeResult result = syncRequestGeocode(type, location);
      callListener(listener, result.address, result.error);
    }
  }

  private void callListener(AddressListener listener, XAddress address,
                            Throwable error)
  {
    Runnable job = () -> listener.setAddress(address, error);
    ThreadUtil.onMain(job);
  }

  public void requestCity(final String text, AddressListener listener)
  {
    XAddress result;
    if (text == null) {
      return;
    }
    synchronized (smCityCache) {
      result = smCityCache.get(Util.lc(Util.trim(text)));
    }
    if (result != null) {
      if (listener != null) {
        int delay = 0;
        ThreadUtil.onMain(() -> listener.setAddress(result), (long) delay);
      }
      return;
    }

    ThreadUtil.onExec(() ->
    {
      try {
        Map<String, Object> params = new HashMap<>();
        params.put("address", text);
        params.put("type", "city");
        XAddress address = callGeocode(params);
        updateCache(text, null, address);
        if (listener != null) {
          int delay = 0;
          ThreadUtil.onMain(() -> listener.setAddress(address, null),
            (long) delay);
        }
      } catch (Throwable error) {
        if (listener != null) {
          int delay = 0;
          ThreadUtil.onMain(() -> listener.setAddress(null, error),
            (long) delay);
        }
      }
    });
  }

  private XAddress callGeocode(Map<String, Object> params)
  {

    Map<String, Object> res = callFunction("geocode", params);

    return processGeocodeResults(res);
  }

  private void updateCache(String text, ParseGeoPoint location,
                           XAddress address)
  {
    synchronized (smCityCache) {
      if (address == null) {
        return;
      }
      ParseGeoPoint newLocation = LocationUtil.getGeoPoint(address.mLocation);
      if (location != null) {
        smCityCache.put(location, address);
      }
      if (newLocation != null) {
        smCityCache.put(newLocation, address);
      }
      if (text != null) {
        smCityCache.put(Util.lc(text), address);
      }
      if (address.cityPlus().split(",").length == 3) {
        smCityCache.put(Util.lc(address.cityPlus()).trim(), address);
      }
    }
  }

  public SharedPreferences getAppPrefs()
  {
    return BaseApp.req().getAppPrefs();
  }

  public XAddress syncCity(final ParseGeoPoint point)
  {
    assert !ThreadUtil.isMainThread();
    GeocodeResult result = syncRequestGeocode("city", point);
    if (result.error == null) {
      return result.address;
    } else {
      throw Util.rethrow("requesting City", result.error);
    }
  }

  GeocodeResult syncRequestGeocode(String type, ParseGeoPoint location)
  {
    GeocodeResult result = new GeocodeResult();
    try {
      boolean city = type.equals("city");
      if (city) {
        result.address = cityCacheGet(location);
      }
      if (result.address == null) {
        Map<String, Object> params = new HashMap<>();
        params.put("location", location);
        params.put("type", type);
        result.address = callReverseGeocode(params);
        if (city) {
          updateCache(null, location, result.address);
        }
      }
    } catch (Throwable e) {
      result.error = e;
    }
    return result;
  }

  private XAddress cityCacheGet(ParseGeoPoint location)
  {
    synchronized (smCityCache) {
      return smCityCache.get(location);
    }
  }

  public interface AddressListener
  {
    default void setAddress(XAddress address, Throwable err)
    {
      if (err != null) {

        get().handleException("Geocoding", err);
      } else {
        setAddress(address);
      }
    }

    void setAddress(XAddress address);
  }

  static class CompletionWatcher
    implements Runnable
  {
    final Runnable mRunnable;
    final OnCompletionListener mListener;

    CompletionWatcher(Runnable runnable, OnCompletionListener listener)
    {
      mRunnable = runnable;
      mListener = listener;
    }

    public void run()
    {
      try {
        mRunnable.run();
        mListener.done(true);
      } catch (Exception e) {

        get().handleException("Running " + mRunnable, e);
        mListener.done(false);
      }
    }
  }

  static class GeocodeResult
  {
    Throwable error = null;
    XAddress address = null;
  }

  class RequestHandler
    implements Runnable
  {
    final RequestType mRequestType;
    final ParseObject mObject;
    final XRequest mRequest;
    final ParseCallback1 mCallback;
    private final String mFunction;
    private final JSONCallback mJSONCallback;

    RequestHandler(RequestType requestType, ParseObject object,
                   ParseCallback1 callback, JSONCallback jsonCallback)
    {
      mObject = object;
      mRequestType = requestType;
      mCallback = callback;
      mJSONCallback = jsonCallback;
      XUser user;
      XPublicCell publicCell;
      if (mRequestType.isResponse() || mRequestType.isFollowup()) {
        mRequest = (XRequest) object;
      } else if (mRequestType == FriendRequest) {
        mRequest = null;
      } else if (requestType == CellJoinRequest) {
        mRequest = null;
      } else {
        throw new RuntimeException("Unexpected value");
      }
      if (mRequestType.isResponse()) {
        mFunction = "sendRequestResponse";
      } else {
        mFunction = "sendRequest";
      }
    }

    @Override
    public void run()
    {
      Map<String, Object> params = new HashMap<>();
      params.put("type", mRequestType);
      params.put("objectId", mObject.getObjectId());
      try {
        Map<String, Object> result = ParseCloud.run(mFunction, params);
        XLog.i(TAG, "result: " + result);
        if (mCallback != null) {
          mCallback.done(null);
        }
        if (mJSONCallback != null) {
          JSONObject object = new JSONObject(result);
//          System.out.println(object.toString(2));
          mJSONCallback.done(true, object);
        }
      } catch (ParseException pe) {
        String doing =
          Util.format("sending %s type %s", mFunction, mRequestType.toString());
        handleException(doing, pe, null, false);
        if (mCallback != null) {
          ThreadUtil.onMain(() -> mCallback.done(pe));
        }
        if (mJSONCallback != null) {
          JSONObject result = new JSONObject();
          result.put("success", false);
          result.put("message", "Exception: " + pe.getMessage());
          ThreadUtil.onMain(() -> mJSONCallback.done(false, result));
        }
      } catch (Throwable t) {
        t.printStackTrace();
        if (mCallback != null) {
          mCallback.done(
            new ParseException(ParseException.OTHER_CAUSE, t.toString()));
        }
        if (mJSONCallback != null) {
          JSONObject result = new JSONObject();
          result.put("success", false);
          result.put("message", "Exception: " + t.getMessage());
          ThreadUtil.onMain(() -> mJSONCallback.done(false, result));
        }
      }
    }

    public void complete(boolean success)
    {
      if (success) {
        XUser currentUser = (XUser) Parse.getCurrentUser();
        switch (mRequestType) {
          case FriendRequest:
          case CellJoinCancel:
          case CellJoinReject:
          case CellJoinResend:
          case CellJoinRequest:
          case CellRecruitRequest:
          case FriendReject:
            showAlertDialog("alert", "message sent");
            break;
          case FriendApprove: {
            showAlertDialog("alert", "Friend Added");
            break;
          }
          case CellJoinApprove: {
            showAlertDialog("alert", "Cell Join Approved");
            break;
          }
        }
        mCallback.done(null);
      } else {
        mCallback.done(
          new ParseException(ParseException.OTHER_CAUSE, "Failed"));
      }
    }
  }
}


