package cell411.services;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.Location;

import com.parse.model.ParseGeoPoint;

import java.util.Set;

import javax.annotation.Nonnull;

import cell411.ui.base.BaseContext;
import cell411.utils.collect.ValueObserver;

public interface LocationService
  extends BaseContext,
          OnSharedPreferenceChangeListener
{

  void init();

  void loadLocation();

  void storeLocation();

  long locationAge();

  ParseGeoPoint getParseGeoPoint();

  @Nonnull
  Location getLocation();

  @Override
  void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                 String key);

  void addObserver(ValueObserver<Location> locationObserver);

  Set<String> getPermRequests();

  void removeObserver(ValueObserver<Location> locationChanged);
}
