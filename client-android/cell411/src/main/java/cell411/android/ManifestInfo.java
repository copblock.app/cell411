package cell411.android;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cell411.ui.base.BaseApp;
import cell411.utils.collect.LazyObject;
import cell411.utils.Util;
import cell411.utils.reflect.XTAG;

/**
 * A utility class for retrieving app metadata such as the app name, default
 * icon, whether or not
 * the app declares the correct permissions for push, etc.
 */
@SuppressWarnings("deprecation")
public class ManifestInfo
{
  private static final LazyObject<ManifestInfo> mInstance =
    LazyObject.fromSupplier(ManifestInfo.class, ManifestInfo::new);

  private static final int GET_INTENT_FILTERS =
    android.content.pm.PackageManager.GET_INTENT_FILTERS;
  private static final XTAG TAG = new XTAG();

  private final Object lock = new Object();
  private final char LINE_SEPARATOR = '\n';
  private final PackageManager mPackageManager;
  private int versionCode = -1;
  private String versionName = null;
  private int iconId = 0;
  private String displayName = null;

  ManifestInfo()
  {
    mPackageManager = BaseApp.req().getPackageManager();
    Objects.requireNonNull(mPackageManager);
  }

  public static ManifestInfo req()
  {
    return Objects.requireNonNull(opt());
  }

  public static ManifestInfo opt()
  {
    return Util.getRef(mInstance);
  }

  /**
   * Returns the version code for this app, as specified by the
   * android:versionCode attribute in the
   * <manifest> element of the manifest.
   */
  public int getVersionCode()
  {
    if (versionCode != -1)
      return versionCode;
    synchronized (lock) {
      if (versionCode == -1) {
        try {
          String packageName = BaseApp.req().getPackageName();
          PackageManager packageManager = getPackageManager();
          PackageInfo packageInfo =
            packageManager.getPackageInfo(packageName, 0);
          versionCode = packageInfo.versionCode;
        } catch (NameNotFoundException e) {
        }
      }
    }

    return versionCode;
  }

  private PackageManager getPackageManager()
  {
    return mPackageManager;
  }

  /**
   * Returns the version name for this app, as specified by the
   * android:versionName attribute in the
   * <manifest> element of the manifest.
   */
  public String getVersionName()
  {
    synchronized (lock) {
      if (versionName == null) {
        try {
          versionName =
            getPackageManager().getPackageInfo(BaseApp.req().getPackageName(),
              0).versionName;
        } catch (NameNotFoundException e) {
          versionName = "unknown";
        }
        if (versionName == null) {
          // Some contexts, such as instrumentation tests can always have
          // this value
          // return as null. We will change to "unknown" for this case as
          // well, so that
          // an exception isn't thrown for adding a null header later.
          versionName = "unknown";
        }
      }
    }

    return versionName;
  }

  /**
   * Returns the display name of the app used by the app launcher, as
   * specified by the android:label
   * attribute in the <application> element of the manifest.
   */
  public String getDisplayName(Context context)
  {
    synchronized (lock) {
      if (displayName == null) {
        ApplicationInfo appInfo = context.getApplicationInfo();
        displayName =
          context.getPackageManager().getApplicationLabel(appInfo).toString();
      }
    }

    return displayName;
  }

  /**
   * Returns the default icon id used by this application, as specified by
   * the android:icon
   * attribute in the <application> element of the manifest.
   */
  public int getIconId()
  {
    synchronized (lock) {
      if (iconId == 0) {
        iconId = BaseApp.req().getApplicationInfo().icon;
      }
    }
    return iconId;
  }

  /* package */
  @SuppressLint("WrongConstant")
  public List<ResolveInfo> getIntentReceivers(String... actions)
  {
    Context context = BaseApp.req();
    String packageName = context.getPackageName();
    List<ResolveInfo> list = new ArrayList<>();

    for (String action : actions) {
      Intent intent = new Intent(action);
      List<ResolveInfo> brs = queryBroadcastReceivers(action);
      list.addAll(brs);
    }

    for (int i = list.size() - 1; i >= 0; --i) {
      String receiverPackageName = list.get(i).activityInfo.packageName;
      if (!receiverPackageName.equals(packageName)) {
        list.remove(i);
      }
    }
    return list;
  }

  /**
   * Returns a list of ResolveInfo objects corresponding to the
   * BroadcastReceivers with Intent
   * Filters
   * specifying the given action within the app's package.
   */
  public List<ResolveInfo> queryBroadcastReceivers(String action)
  {
    Intent intent = new Intent(action);
    Context context = BaseApp.req();
    PackageManager pm = context.getPackageManager();

    List<ResolveInfo> res =
      pm.queryBroadcastReceivers(intent, GET_INTENT_FILTERS);
    for (ResolveInfo item : res) {
      System.out.println(item);
    }
    return res;
  }

  /**
   * @return A {@link Bundle} if meta-data is specified in AndroidManifest,
   * otherwise null.
   */
  public Bundle getApplicationMetadata(Context context)
  {
    ApplicationInfo info =
      getApplicationInfo(context, PackageManager.GET_META_DATA);
    if (info != null) {
      return info.metaData;
    }
    return null;
  }

  private ApplicationInfo getApplicationInfo(Context context, int flags)
  {
    try {
      return context.getPackageManager()
        .getApplicationInfo(context.getPackageName(), flags);
    } catch (NameNotFoundException e) {
      return null;
    }
  }
}

