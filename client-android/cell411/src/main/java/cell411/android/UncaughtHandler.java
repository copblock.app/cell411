package cell411.android;

import android.os.Handler;
import android.os.Looper;

import javax.annotation.Nonnull;

import cell411.ui.base.BaseApp;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.except.BackgroundException;
import cell411.utils.except.ExceptionHandler;

public class UncaughtHandler
  implements Thread.UncaughtExceptionHandler,
             Runnable,
             ExceptionHandler
{
  private final Thread.UncaughtExceptionHandler mSystemUncaughtHandler;

  private UncaughtHandler()
  {
    mSystemUncaughtHandler = Thread.getDefaultUncaughtExceptionHandler();
  }

  public static void startCatcher()
  {
    Handler handler = new Handler(Looper.getMainLooper());
    handler.post(new UncaughtHandler());
  }

  public void uncaughtException(Thread thread, @Nonnull final Throwable e)
  {
    final int tid = 0;// = Process.myTid();
    final String threadName = thread.getName();
    ThreadUtil.onMain((Runnable) () ->
    {
      throw new BackgroundException(e, tid, threadName);
    });
  }

  @Override
  public void run()
  {
    Thread thread = Thread.currentThread();
    String name = thread.getName();
    try {
      thread.setName("UI Thread");
      // the following handler is used to catch exceptions thrown in
      // background threads
      Thread.setDefaultUncaughtExceptionHandler(this);

      while (!BaseApp.req().isDone()) {
        try {
          Looper.loop();
          Thread.setDefaultUncaughtExceptionHandler(mSystemUncaughtHandler);
          throw new RuntimeException("Main thread loop unexpectedly exited");
        } catch (BackgroundException e) {
          handleException("Running main loop", e.getCause());
        } catch (Throwable e) {
          handleException("Running main loop", e);
        }
      }
    } finally {
      thread.setName(name);
    }
  }
}
