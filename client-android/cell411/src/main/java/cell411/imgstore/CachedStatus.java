package cell411.imgstore;

public class CachedStatus
{

  public static final CachedStatus NEW_REQUEST;
  public static final CachedStatus SUCCESS;
  public static final CachedStatus CANCELED;

  static {
    NEW_REQUEST = new CachedStatus(FailType.NEW_REQUEST);
    SUCCESS = new CachedStatus(FailType.SUCCESS);
    CANCELED = new CachedStatus(FailType.CANCELED);
  }

  private final FailType type;

  private final Throwable cause;

  public CachedStatus(FailType canceled)
  {
    this(canceled, null);
  }

  public CachedStatus(FailType type, Throwable cause)
  {
    this.type = type;
    this.cause = cause;
  }

  /**
   * @return {@linkplain FailType Fail type}
   */
  public FailType getType()
  {
    return type;
  }

  /**
   * @return Thrown exception/error, can be <b>null</b>
   */
  public Throwable getCause()
  {
    return cause;
  }

}