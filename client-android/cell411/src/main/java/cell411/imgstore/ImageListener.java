package cell411.imgstore;

import android.graphics.Bitmap;

import java.net.URL;

public interface ImageListener
{
  void onLoadingFailed(String imageUri, CachedStatus cachedStatus);

  void onLoadingComplete(URL imageUri, Bitmap loadedImage);
}
