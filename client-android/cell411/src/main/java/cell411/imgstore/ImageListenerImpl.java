package cell411.imgstore;

import android.graphics.Bitmap;

import androidx.annotation.CallSuper;

import java.net.URL;

import cell411.utils.UrlUtils;

public class ImageListenerImpl
  implements ImageListener
{
  @CallSuper
  public void onLoadingStarted(String s)
  {
  }

  @CallSuper
  public void onLoadingFailed(String s, CachedStatus cachedStatus)
  {
  }

  @CallSuper
  public void onLoadingComplete(URL s, Bitmap bitmap)
  {
    UrlUtils.getFileName(s);
  }

  @CallSuper
  public void onLoadingCancelled(String s)
  {
    UrlUtils.getFileName(s);
  }

}
