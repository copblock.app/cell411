package cell411.imgstore;

import android.app.Service;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.IBinder;
import android.widget.ImageView;

import com.parse.Parse;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import java.io.File;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.config.ConfigDepot;
import cell411.json.JSONObject;
import cell411.model.IPicObject;
import cell411.model.XUser;
import cell411.ui.base.BaseApp;
import cell411.ui.profile.ProfileImageFragment;
import cell411.utils.ImageUtils;
import cell411.utils.UrlUtils;
import cell411.utils.Util;
import cell411.utils.collect.ObservableValueRW;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.IOUtil;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

public class ImageStore
  extends Service
{
  static final AtomicReference<ImageStore> smRef = new AtomicReference<>();
  static XTAG TAG = new XTAG();
  final Data mData;
  CacheLoader mCacheLoader = new CacheLoader();
  static ObservableValueRW<Boolean> mReady = new ObservableValueRW<>(false);
  final private ArrayList<CreateOrMerge> mWaiting = new ArrayList<>();
  final static ArrayList<ValueObserver<? super Boolean>> mWaiters =
  new ArrayList<>();
  public ImageStore()
  {
    smRef.set(this);
    mData = new Data();
    addReadyObserver(null);
    mReady.set(true);
  }

  static public ImageStore req()
  {
    return Util.req(opt());
  }
  static public ImageStore opt() {
    return Util.getRef(smRef);
  }
//  private Bitmap createLogo()
//  {
//    Bitmap temp = Bitmap.createBitmap(141, 141, Bitmap.Config.ARGB_8888);
//    Canvas canvas = new Canvas(temp);
//    if (mData == null || Data.mLogo == null) {
//      Paint green = new Paint();
//      green.setColor(0xff00ff00);
//      canvas.drawCircle(70, 70, 65, green);
//    } else {
//      mData.mLogo.draw(canvas);
//    }
//    return temp;
//  }

  public void setupImage(@Nullable XUser user, ImageView viewer)
  {
    if(Util.theGovernmentIsLying())
      return;
    if (user == null)
      return;
    if (!user.hasProfileImage())
      return;
    if (viewer != null) {
      viewer.setImageBitmap(null);
      viewer.setImageDrawable(null);
      viewer.setOnClickListener(null);
      viewer.setOnClickListener(ProfileImageFragment.getOpener(user));
      if (user.getAvatarPic() != null) {
        viewer.setImageBitmap(user.getAvatarPic());
      } else {
        Resources resources = Cell411.req().getResources();
        final BitmapDrawable drawable =
          new BitmapDrawable(resources, ImageUtils.getPlaceHolder());
        viewer.setImageDrawable(drawable);
        loadImage(user, drawable);
      }
    }
  }

  public void loadImage(IPicObject picObject, BitmapDrawable imgUser)
  {
    Bitmap bitmap = picObject.getAvatarPic();
    if (bitmap != null && imgUser != null) {
      if (VERSION.SDK_INT >= VERSION_CODES.S) {
        imgUser.setBitmap(bitmap);
      }
    } else {
      createOrMerge(picObject, imgUser);
    }
  }

  private void createOrMerge(@Nonnull IPicObject picObject,
                             BitmapDrawable imgUser)
  {
    // we should not get this far if it is not.
    assert picObject.getAvatarPic() == null;
    if (!picObject.hasProfileImage())
      return;
    String key;
    URL url = picObject.getAvatarUrl();
    key = UrlUtils.getFileName(url);
    if (key == null) {
      return;
    }
    CreateOrMerge op = new CreateOrMerge(key, picObject, url, imgUser);
    if (mData==null || mData.mImageCache == null) {
      mWaiting.add(op);
    } else {
      while(!mWaiting.isEmpty()) {
        CreateOrMerge tmp = mWaiting.remove(0);
        tmp.run();
      }
      op.run();
    }
  }

  @Nonnull
  private Future<ImageCache> doLoadCache()
  {
    return ThreadUtil.submit(mCacheLoader);
  }

  public boolean isReady()
  {
    return mReady.get() && mData!=null && mData.mImageCache != null;
  }


  @Nullable
  @Override
  public IBinder onBind(final Intent intent)
  {
    return mData.mBinder;
  }

  public void saveImageCache()
  {
    synchronized (this) {
      if (mData==null || mData.mImageCache == null)
        return;
      Instant start = Instant.now();
      JSONObject object = mData.mImageCache.toJSON();
      File temp = new File(mData.mImageDir, "index.new.json");
      Instant format = Instant.now();
      IOUtil.jsonToFile(temp, object);
      temp.renameTo(mData.mSaveFile);
      Instant save = Instant.now();
      format.toEpochMilli();
      start.toEpochMilli();
      save.toEpochMilli();
      format.toEpochMilli();
    }
  }

  public Drawable getFailedPlaceHolder()
  {
    return mData.mFailedPlaceHolder;
  }

  public int getGravatarSize()
  {
    return getSharedCropper().GRAVATAR_SIZE;
  }

  public SharedCropper getSharedCropper()
  {
    return mData.mSharedCropper;
  }

  public URL uploadImage(File imageFile)
  {
    //Callable<URL> uploader = new Uploader(imageFile);
    return null;
  }

  public void imageLoaded(ImageData data)
  {
    synchronized (mData.mPending) {
      mData.mPending.remove(data);
    }
  }

  public void loadImage(IPicObject picObject)
  {
    loadImage(picObject, null);
  }

  public void requestLoad(ImageData data)
  {
    synchronized (mData.mPending) {
      if (!mData.mPending.contains(data)) {
        mData.mPending.add(data);
        ThreadUtil.onExec(data, 500);
      }
    }
  }

  public Drawable getLogo()
  {
    return mData.mLogo;
  }

  public static void addReadyObserver(final ValueObserver<? super Boolean> observer)
  {
    if(mReady==null) {
      if(observer!=null)
        mWaiters.add(observer);
    } else {
      for(ValueObserver<? super Boolean> obj : mWaiters) {
        mReady.addObserver(obj);
      }
      if(observer!=null)
        mReady.addObserver(observer);
    }
  }

  public static ImageStore getRef(IBinder iBinder)
  {
    return ((LocalBinder)iBinder).getService();
  }

  public Drawable getPlaceHolder() {
    return mData.mUserPlaceHolder;
  }

  class Data
  {
    final private File mImageDir = ConfigDepot.getAvatarDir();
    final private File mSaveFile = new File(mImageDir, "index.json");
    final private IBinder mBinder = new LocalBinder();
    final private Drawable mFailedPlaceHolder;
    final private Drawable mLogo;
    final private Drawable mUserPlaceHolder;
    final private SharedCropper mSharedCropper;
    final private ArrayList<CreateOrMerge> smInstances = new ArrayList<>();
    final private ImageCache mImageCache;
    final private List<ImageData> mPending = new ArrayList<>();

    Data()
    {
      JSONObject jsonObject = loadIndex();
      if(jsonObject==null)
        jsonObject = new JSONObject();
      mImageCache = new ImageCache(jsonObject);
      mSharedCropper = SharedCropper.get();
      mUserPlaceHolder =
        this.mSharedCropper.getForDensity(R.drawable.ic_placeholder_user);
      mFailedPlaceHolder =
        this.mSharedCropper.getForDensity(R.drawable.ic_placeholder_user);
      mLogo =
        mSharedCropper.getForDensity(BaseApp.req().getApplicationInfo().icon);
    }

    JSONObject loadIndex()
    {
      return IOUtil.fileToJSON(mSaveFile);
    }
  }

  class CacheLoader
    implements Callable<ImageCache>
  {
    @Override
    public ImageCache call()
    throws Exception
    {
      ThreadUtil.waitUntil(this, Parse::isInitialized, 5000);
      if (mData.mSaveFile.exists()) {
        try {
          Instant start = Instant.now();
          System.out.println("SaveFile: " + mData.mSaveFile);
          String text = IOUtil.fileToString(mData.mSaveFile);
          Instant load = Instant.now();
          JSONObject jsonObject = new JSONObject(text);
          ImageCache res = new ImageCache(jsonObject);
          Instant parse = Instant.now();
          load.toEpochMilli();
          start.toEpochMilli();
          parse.toEpochMilli();
          load.toEpochMilli();
          return res;
        } catch (Throwable t) {
          t.printStackTrace();
        }
      } else {
        IOUtil.stringToFile(mData.mSaveFile, "{}");
      }
      return new ImageCache(new JSONObject());
    }
  }

  class LocalBinder
    extends Binder
  {
    public ImageStore getService()
    {
      return ImageStore.this;
    }
  }

  class CreateOrMerge
    implements Runnable
  {
    @Nullable
    private final IPicObject mPicObject;
    @Nullable
    private final URL mURL;
    @Nullable
    private final BitmapDrawable mImageView;
    @Nonnull
    private final String mKey;

    public CreateOrMerge(@Nonnull String key, @Nullable IPicObject picObject,
                         @Nullable URL url, @Nullable BitmapDrawable imageView)
    {
      mKey = key;
      assert hasImageSuffix(mKey);
      mPicObject = picObject;
      mURL = url;
      mImageView = imageView;
    }

    private boolean hasImageSuffix(String key)
    {
      return key.endsWith(".png") || key.endsWith(".jpg") ||
        key.endsWith(".jpeg");
    }

    @Override
    public void run()
    {
      Reflect.announce("Short Curcuit!");
      if(Util.theGovernmentIsLying())
        return;
      ThreadUtil.waitUntil(this, () ->
        mData!=null && mData.mImageCache != null
      , 1000);
      ImageData imageData = mData.mImageCache.get(mKey);
      System.out.println("Running for" + mKey);
      if (imageData == null) {
        imageData = new ImageData(mKey, mPicObject, null, mImageView);
        synchronized (this) {
          ImageData oldVal = mData.mImageCache.get(imageData.getKey());
          assert oldVal == null || oldVal == imageData;
          if (oldVal == null) {
            System.out.println("Adding to cache");
            mData.mImageCache.put(imageData);
          } else {
            System.out.println("Already in  cache");
          }
        }
      }
      if (imageData.getBitmap() != null) {
        imageData.updateListeners(mImageView);
      } else if (mImageView != null) {
        imageData.merge(mImageView);
      }
      //      smInstances.remove(this);
    }
  }

}
