package cell411.methods;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import com.parse.ParseException;
import com.parse.model.ParseUser;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.model.XUser;
import cell411.ui.base.BaseActivity;
import cell411.ui.base.BaseApp;
import cell411.ui.base.DialogShower;
import cell411.ui.base.EnterTextDialog;
import cell411.utils.OnCompletionListener;
import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.io.PrintString;
import cell411.utils.reflect.XTAG;

public class Dialogs
{
  private static final XTAG TAG = new XTAG();
  static Dialogs smInstance = new Dialogs();
  static List<AlertDialog> mShowing = new ArrayList<>();

  //  static {
  //    XLog.i(TAG, "loading class");
  //  }

  private final boolean mStopWithTheFuckingDialogs = false;

  public static void showSessionExpiredAlertDialog(
    OnCompletionListener listener)
  {
    Runnable runnable = createSessionExpiredAlertDialog(listener);
    ThreadUtil.onMain(runnable);
  }

  public static DialogShower createSessionExpiredAlertDialog(
    OnCompletionListener listener)
  {
    return createAlertDialog("Session Expired",
      "We are sorry, you have been logged out, please log in again.", null,
      listener);
  }

  public static DialogShower createAlertDialog(String title, String message,
                                               DialogInterface.OnDismissListener listener,
                                               OnCompletionListener listener2)
  {
    return new AlertDialogShower(title, message, listener, listener2);
  }

  public static boolean isDialogShowing()
  {
    return !getShowing().isEmpty();
  }

  public synchronized static List<AlertDialog> getShowing()
  {
    return mShowing;
  }

  public static DialogShower createYesNoDialog(String message,
                                               OnCompletionListener listener)
  {
    return createYesNoDialog(null, message, listener);
  }

  public static DialogShower createYesNoDialog(String title, String message,
                                               OnCompletionListener listener)
  {
    return new YesNoShower(listener, title, message);

  }

  public static void showExceptionDialog(Throwable e, String what,
                                         OnCompletionListener listener)
  {
    Runnable runnable = createExceptionDialog(e, what, listener);
    ThreadUtil.onMain(runnable);
  }

  public static DialogShower createExceptionDialog(Throwable e, String what,
                                                   OnCompletionListener listener)
  {
    e.printStackTrace();
    if (get() != null && get().mStopWithTheFuckingDialogs) {
      return null;
    }
    PrintString m = new PrintString();
    String title = "Error";
    m.p("We have encountered ");
    if (e instanceof ParseException) {
      title = "Database Error";
      m.p("a database");
    } else {
      m.p("an");
    }
    m.p(" error, while ");
    m.p(what);
    m.p(".  This may be a transient problem.  " +
      "If it continues, please contact us." + "\n");
    Integer code =
      Util.castAndCall(e, ParseException.class, ParseException::getCode);
    if (code != null) {
      m.pl("ParseException code: " + code);
    }
    m.pl("\n");
    e.printStackTrace(m);
    return createAlertDialog(title, m.toString(), null, listener);
  }

  private static Dialogs get()
  {
    return smInstance;
  }

  public static DialogShower createAlertDialog(String message)
  {
    return createAlertDialog(null, message, null, null);
  }

  public static DialogShower createAlertDialog(@Nonnull String message,
                                               @Nullable
                                               DialogInterface.OnDismissListener listener)
  {
    return createAlertDialog(null, message, listener, null);
  }

  public static void showEnterTextDialog(String title, String hint,
                                         String initVal,
                                         OnCompletionListener listener)
  {
    Runnable runnable = createEnterTextDialog(title, hint, initVal, listener);
    ThreadUtil.onMain(runnable);
  }

  public static DialogShower createEnterTextDialog(String title, String hint,
                                                   String initVal,
                                                   OnCompletionListener listener)
  {
    return new DialogShower()
    {
      @Override
      protected AlertDialog createDialog()
      {
        EnterTextDialog dialog = new EnterTextDialog();
        dialog.setTitle(title);
        dialog.setHint(hint);
        dialog.setInitVal(initVal);
        dialog.setOnDismissListener(d ->
        {
          mText = dialog.getAnswer();
          if (listener != null) {
            listener.done(true);
          }
        });
        return dialog;
      }
    };
  }

  public static DialogShower createEnterTextDialog(@StringRes int title,
                                                   @StringRes int hint,
                                                   String initVal,
                                                   OnCompletionListener listener)
  {
    return createEnterTextDialog(Util.getString(title), Util.getString(hint),
      initVal, listener);
  }

  public static void showYesNoDialog(final String title,
                                     final OnCompletionListener listener)
  {
    showYesNoDialog(title, null, listener);
  }

  public static void showYesNoDialog(final String title, String message,
                                     final OnCompletionListener listener)
  {
    Runnable runnable = createYesNoDialog(title, message, listener);
    ThreadUtil.onMain(runnable);
  }

  public static void realShowAlertDialog(final String message,
                                         final OnCompletionListener listener)
  {
    Runnable runnable = createAlertDialog(message, listener);
    ThreadUtil.onMain(runnable);
  }

  public static DialogShower createAlertDialog(@Nonnull String message,
                                               @Nullable
                                               OnCompletionListener listener)
  {
    return createAlertDialog(null, message, null, listener);
  }

  public static DialogShower showNoButtonDialog(String title, String message,
                                                OnCompletionListener listener)
  {
    DialogShower noButtonDialog =
      createNoButtonDialog(title, message, null, listener);
    ThreadUtil.onMain(noButtonDialog);
    return noButtonDialog;
  }

  public static DialogShower createNoButtonDialog(String title, String message,
                                                  DialogInterface.OnDismissListener listener,
                                                  OnCompletionListener listener2)
  {
    return new DialogShower()
    {
      @Override
      protected AlertDialog createDialog()
      {
        AlertDialog.Builder builder =
          new AlertDialog.Builder(mActivity, getThemeRes());
        if (!Util.isNoE(title)) {
          builder.setTitle(title);
        }
        if (!Util.isNoE(message)) {
          builder.setMessage(message);
        }

        //        builder.setPositiveButton(R.string.dialog_btn_ok, this);
        final AlertDialog dialog = builder.create();
        dialog.setCancelable(getCancelable());
        dialog.setOnDismissListener(dialog1 ->
        {
          if (listener != null) {
            listener.onDismiss(dialog1);
          }
          if (listener2 != null) {
            listener2.done(true);
          }
          getShowing().removeIf((d) -> d == dialog1);
        });
        return dialog;
      }
    };
  }

  public static DialogShower createNoButtonDialog(String title,
                                                  final String message,
                                                  OnCompletionListener onCompletionListener)
  {
    return createNoButtonDialog(title, message, null, null);
  }

  public static void onUIThread(Runnable runnable, long i)
  {
    ThreadUtil.onMain(runnable,i);
  }

  public static class YesNoDialog
    extends AlertDialog
  {
    Boolean mAnswer = null;

    protected YesNoDialog(OnCompletionListener listener)
    {
      super(BaseApp.req().getCurrentActivity(),
        android.R.style.Theme_Material_Dialog_Alert);
      AlertDialog mDialog = this;
      setOnDismissListener(dialog ->
      {
        listener.done(mAnswer);
        getShowing().removeIf((d) -> d == mDialog);
      });
    }

    public void onButtonClick(DialogInterface dialog, int which)
    {
      assert dialog == this;
      switch (which) {
        case BUTTON_POSITIVE:
        case BUTTON_NEGATIVE:
          mAnswer = which == BUTTON_POSITIVE;
          break;
        default:
          mAnswer = null;
          break;
      }
    }
  }

  private static class YesNoShower
    extends DialogShower
  {
    private final OnCompletionListener mListener;
    private final String mTitle;
    private final String mMessage;

    public YesNoShower(OnCompletionListener listener, String title,
                       String message)
    {
      mListener = listener;
      mTitle = Util.isNoE(title) ? "Query" : title;
      mMessage = message;
    }

    @Override
    protected AlertDialog createDialog()
    {
      YesNoDialog yesNoDialog = new YesNoDialog(mListener);
      yesNoDialog.setCancelable(false);
      yesNoDialog.setButton(BUTTON_POSITIVE, "Yes", yesNoDialog::onButtonClick);
      yesNoDialog.setButton(BUTTON_NEGATIVE, "No", yesNoDialog::onButtonClick);
      yesNoDialog.setTitle(mTitle);
      yesNoDialog.setMessage(mMessage);
      yesNoDialog.setOnDismissListener(dialog ->
      {
        Dialogs.getShowing().removeIf((dlg) -> dlg == yesNoDialog);
        mListener.done(yesNoDialog.mAnswer);
      });
      return yesNoDialog;
    }
  }

  private static class AlertDialogShower
    extends DialogShower
  {
    private final String mTitle;
    private final String mMessage;
    private final DialogInterface.OnDismissListener mListener;
    private final OnCompletionListener mListener2;

    public AlertDialogShower(String title, String message,
                             DialogInterface.OnDismissListener listener,
                             OnCompletionListener listener2)
    {
      mTitle = title;
      mMessage = message;
      mListener = listener;
      mListener2 = listener2;
    }

    @Override
    protected AlertDialog createDialog()
    {
      AlertDialog.Builder builder =
        new AlertDialog.Builder(mActivity, getThemeRes());
      builder.setTitle(mTitle);
      builder.setMessage(mMessage);

      builder.setPositiveButton(R.string.dialog_btn_ok, this);
      final AlertDialog dialog = builder.create();
      dialog.setCancelable(getCancelable());
      dialog.setOnDismissListener(dialog1 ->
      {
        if (mListener != null) {
          mListener.onDismiss(dialog1);
        }
        if (mListener2 != null) {
          mListener2.done(true);
        }
        getShowing().removeIf((d) -> d == dialog1);
      });
      return dialog;
    }
  }

  static ArrayList<ExtraDismissListener> mDismissListeners = new ArrayList<>();

  public static void showForgotPasswordDialog(BaseActivity activity,
                                              OnCompletionListener listener)
  {
    new ForgotPasswordDialog(activity, listener).run();
  }

  @SuppressWarnings("deprecation")
  public static void showConfirmDeletionAlertDialog(Activity activity)
  {
    AlertDialog.Builder alert = new AlertDialog.Builder(activity);
    alert.setMessage(activity.getString(R.string.dialog_msg_delete_my_account));
    alert.setNegativeButton(R.string.dialog_btn_no, (dialog, which) ->
    {
    });
    alert.setPositiveButton(R.string.dialog_btn_yes, (dialogInterface, i) ->
    {
      final ProgressDialog dialog = new ProgressDialog(activity);
      dialog.setMessage(
        activity.getString(R.string.dialog_msg_deleting_account));
      dialog.setCancelable(false);
      dialog.show();
      XUser user = XUser.reqCurrentUser();
      user.deleteInBackground();
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  //  public static void showQRCodeDialog(BaseActivity activity)
  //  {
  //    AlertDialog.Builder alert = new AlertDialog.Builder(activity);
  //    alert.setTitle(R.string.dialog_title_qr_code);
  //    LayoutInflater inflater =
  //      (LayoutInflater) activity.getSystemService(Service
  //      .LAYOUT_INFLATER_SERVICE);
  //    View view = inflater.inflate(R.layout.layout_qrcode, null);
  //    final LinearLayout linearLayout = view.findViewById(R.id.ll_qrcode);
  //    final ImageView imgQRCode = view.findViewById(R.id.img_qrcode);
  //    TextView txtEmail = view.findViewById(R.id.txt_email);
  //    XUser currentUser = XUser.getCurrentUser();
  //    txtEmail.setText(currentUser.getEmail());
  //    //Find screen size
  //    //Encode with a QR Code image
  //    int smallerDimension =
  //      Math.min(ImageUtils.getScreenWidth(), ImageUtils.getScreenHeight())
  //      * 3 / 4;
  //    QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(currentUser
  //    .getUsername(),
  //    smallerDimension);
  //    Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
  //    imgQRCode.setImageBitmap(bitmap);
  //    alert.setView(view);
  //    alert.setNegativeButton(R.string.dialog_btn_cancel, (dialog, arg1) ->
  //    dialog.dismiss());
  //    alert.setPositiveButton(R.string.dialog_btn_share, (dialog, which) -> {
  //      linearLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View
  //      .MeasureSpec.UNSPECIFIED),
  //        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
  //      linearLayout.layout(0, 0, linearLayout.getMeasuredWidth(),
  //      linearLayout
  //      .getMeasuredHeight());
  //      linearLayout.setDrawingCacheEnabled(true);
  //      linearLayout.buildDrawingCache(true);
  //      Bitmap finalBitmap = Bitmap.createBitmap(linearLayout
  //      .getDrawingCache());
  //      linearLayout.destroyDrawingCache();
  //      Intent share = new Intent(Intent.ACTION_SEND);
  //      share.setType("image/jpeg");
  //      ByteArrayOutputStream bytes = new ByteArrayOutputStream();
  //      assert finalBitmap != null;
  //      finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
  //      File cacheDir;
  //      // Make sure external shared storage is available
  //      if (Environment.MEDIA_MOUNTED.equals(Environment
  //      .getExternalStorageState())) {
  //        // We can read and write the media
  //        cacheDir = activity.getExternalFilesDir(null);
  //      } else {
  //        // Load another directory, probably local memory
  //        cacheDir = activity.getFilesDir();
  //      }
  //      File photoFile = new File(cacheDir + File.separator + "cell411.jpg");
  //      if (!IOUtil.createNewFile(photoFile))
  //        return;
  //      try (FileOutputStream fo = new FileOutputStream(photoFile)) {
  //        fo.write(bytes.toByteArray());
  //      } catch (IOException e) {
  //        e.printStackTrace();
  //      }
  //      Uri photoURI =
  //        FileProvider.getUriForFile(activity, "app.copblock.dev",
  //          photoFile);
  //
  //      share.putExtra(Intent.EXTRA_STREAM, photoURI);
  //      share.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string
  //      .share_qr_code_subject));
  //      share.putExtra(Intent.EXTRA_TEXT, activity.getString(R.string
  //      .share_qr_code_desc));
  //      activity.startActivity(Intent.createChooser(share, activity
  //      .getString(R.string
  //      .share_qr_code_title)));
  //    });
  //    androidx.appcompat.app.AlertDialog dialog = alert.create();
  //    dialog.show();
  //  }

  //  public static void showLogoutAlertDialog()
  //  {
  //    onUIThread(createLogoutAlertDialog());
  //  }

  //  public static DialogShower createLogoutAlertDialog()
  //  {
  //    return createYesNoDialog(getString(R.string.dialog_logout_message),
  //    success ->
  //    {
  //      if (success)
  //      {
  //        ParseUser.logOut();
  //      }
  //    });
  //    DialogShower shower = new DialogShower() {
  //      protected AlertDialog createDialog(){
  //        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
  //        alert.setCancelable(false);
  //        LayoutInflater inflater =
  //          (LayoutInflater) activity.getSystemService(Service
  //          .LAYOUT_INFLATER_SERVICE);
  //        View view = inflater.inflate(R.layout.dialog_logout, null);
  //        alert.setView(view);
  //        alert.setNegativeButton(R.string.dialog_btn_cancel, Util
  //        .nullClickListener());
  //        alert.setPositiveButton(R.string.dialog_btn_logout, new
  //        DialogInterface
  //        .OnClickListener() {
  //          @Override
  //          public void onClick(final DialogInterface dialog, final int
  //          which) {
  //            ParseUser.logOut();
  //          }
  //        });
  //        return alert.create();
  //      };
  //    };
  //  }

  public void showAlertDialog(final String message,
                              OnCompletionListener listener)
  {
    Runnable runnable = createAlertDialog(message);
    ThreadUtil.onMain(runnable);
  }

  static class ForgotPasswordDialog
  {
    final BaseActivity mActivity;
    final DialogShower smShower;
    Listener mListener = new Listener();

    OnCompletionListener mOuterListener;

    ForgotPasswordDialog(final BaseActivity activity,
                         final OnCompletionListener listener)
    {
      mActivity = activity;
      mOuterListener = listener;
      smShower = createEnterTextDialog(R.string.dialog_title_forgot_password,
        R.string.dialog_message_tap_submit, "dev1@copblock.app", mListener);
    }

    public void run()
    {
      ThreadUtil.onMain(smShower);
    }

    class Listener
      implements OnCompletionListener
    {

      @Override
      public void done(final boolean success)
      {
        if (success) {
          BaseApp.req();

          ThreadUtil.onExec(() ->
          {
            ParseUser.requestPasswordReset(smShower.getText());
            BaseApp.req();
            ThreadUtil.onMain(() -> mOuterListener.done(true));
          });
        } else {
          mOuterListener.done(false);
        }
      }
    }
  }

  static class ExtraDismissListener
    implements DialogInterface.OnDismissListener
  {
    final DialogInterface.OnDismissListener mListener;
    final OnCompletionListener mCompletionListener;

    ExtraDismissListener(DialogInterface.OnDismissListener listener,
                         OnCompletionListener completionListener)
    {
      mListener = listener;
      mCompletionListener = completionListener;
      mDismissListeners.add(this);
    }

    public void onDismiss(DialogInterface dialog)
    {
      if (mCompletionListener != null) {
        mCompletionListener.done(success());
      }
      if (mListener != null) {
        mListener.onDismiss(dialog);
      }
      mDismissListeners.remove(this);
    }

    boolean success()
    {
      return true;
    }
  }
}


