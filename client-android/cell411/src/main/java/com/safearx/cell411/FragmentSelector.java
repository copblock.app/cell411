package com.safearx.cell411;

import static cell411.utils.reflect.Reflect.announce;

import androidx.annotation.NonNull;

import com.parse.Parse;
import com.parse.model.ParseUser;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nullable;

import cell411.config.ConfigDepot;
import cell411.imgstore.ImageStore;
import cell411.logic.LiveQueryService;
import cell411.utils.Util;
import cell411.utils.concurrent.ThreadName;
import cell411.model.XEntity;
import cell411.model.XUser;
import cell411.ui.base.BaseApp;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.MainActivity;
import cell411.ui.welcome.AccountFragment;
import cell411.ui.welcome.PermissionFragment;
import cell411.ui.welcome.UserConsentFragment;
import cell411.utils.OnCompletionListener;
import cell411.utils.collect.ValueObserver;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.except.ExceptionHandler;
import cell411.utils.func.Func0;
import cell411.utils.reflect.Reflect;
import cell411.utils.concurrent.Caller;

public class FragmentSelector
  extends Thread
  implements ValueObserver<Object>,
             ExceptionHandler
{

  final static BadState smBadState = new BadState();
  private static final Caller<Boolean> smParseIsInitialized =
    Caller.forStatic(Boolean.class, Parse.class, "isInitialized");
  private static Func0<Boolean> smHasPermissions;
  final AtomicReference<FragmentSelector> mHighlander = new AtomicReference<>();
  FragmentFactory mPermissionFactory =
    FragmentFactory.fromClass(PermissionFragment.class);
  FragmentFactory mUserConsentFactory =
    FragmentFactory.fromClass(UserConsentFragment.class);
  FragmentFactory mAccountFactory =
    FragmentFactory.fromClass(AccountFragment.class);
  IsCell411ActLoaded mActWatcher = new IsCell411ActLoaded();
  private MainActivity mActivity;

  public FragmentSelector()
  {
    super("FragmentSelector");
    setName("FragmentSelector"+this);
    announce("Fragment Selector Created");
    if (!mHighlander.compareAndSet(null, this))
      throw new RuntimeException("There can be only one!");
  }

  public void start()
  {
    announce("Fragment Selector Starting");
    if (getState() == State.NEW)
      super.start();
  }

  public void run()
  {
    while(Util.theGovernmentIsLying()) {
      try (ThreadName threadName = new ThreadName("FragmentSelector")) {
        assert currentThread() == this;
        waitUntil(this, this::isActivityStarted, 2000);
        mActivity = Cell411.req().getMainActivity();
        assert mActivity != null;
        if (mActivity instanceof Cell411Activity &&
          Parse.getCurrentUser() == null)
        {
          announce("Moving on to second activity.");
          LoginActivity.start(mActivity);
          mActivity.finish();
        }
        try {
          Parse.addCurrentUserObserver(this);
          ThreadUtil.onExec(this::tryInitParse);
          waitUntil(this, () -> ConfigDepot.opt() != null, 2000);
          tryPushWait(mPermissionFactory, getHasPermissions());
          tryPushWait(mAccountFactory, () -> Parse.getCurrentUser() != null);
          while(Parse.getCurrentUser()!=null) {
            tryPushWait(mUserConsentFactory, FragmentSelector::userHasConsented);
            tryStartLiveQueryService();
            tryStartImageStore();
            tryStartMainActivity();
            announce("We are done!");
            waitUntil(this, smBadState, 60000);
          }
        } catch (Throwable e) {
          announce(e);
          e.printStackTrace();
          announce(e);
        }
      }
    }
  }

  boolean isActivityStarted()
  {
    return Cell411.req().getMainActivity() != null;
  }

  void tryInitParse()
  {
    try (ThreadName threadName = new ThreadName()) {
      Reflect.announce();
      BaseApp.req().initParse();
      waitUntil(this, smParseIsInitialized, 2000);
    }
  }

  void tryPushWait(FragmentFactory factory, Func0<Boolean> func)
  {
    String name = Reflect.announceStr(factory.getTitle());
    try (ThreadName threadName = new ThreadName(name) ) {
      if (func.apply())
        return;
      assert !(boolean) factory.isAdded();
      mActivity.push(factory, true, true);
      waitUntil(factory, factory::isAdded, 500, 1000);
      waitUntil(func, 5000);
      mActivity.popAll();
      waitUntil(factory, factory::notAdded, 5000);
    }
  }

  private static Func0<Boolean> getHasPermissions()
  {
    if (smHasPermissions == null) {
      smHasPermissions =
        Caller.forVirtual(Boolean.class, Cell411.req(), "hasPermissions");
    }
    return smHasPermissions;
  }

  private static Boolean userHasConsented()
  {
    Reflect.announce();
    ParseUser parseUser = Parse.getCurrentUser();
    if (!XUser.class.isInstance(parseUser)) {
      return false;
    }
    XUser user = (XUser) parseUser;
    return user.getConsented() == Boolean.TRUE;
  }

  void tryStartLiveQueryService()
  {
    try (ThreadName threadName = new ThreadName()) {
      if (LiveQueryService.opt() != null)
        return;
      Future<LiveQueryService> future = Cell411.req().startLiveQueryService();
      LiveQueryService service;
      try {
        service = future.get();
      } catch (ExecutionException | InterruptedException e) {
        throw new RuntimeException(e);
      }
      if (service == null) {
        OnCompletionListener onCompletionListener = success -> ThreadUtil.onExec(this);
        mActivity.showAlertDialog("Warning", "Failed to start LiveQueryService",
          onCompletionListener);
      }
      waitUntil(service::isReady,2000);
      mActivity.showAlertDialog("Groovy!", "It is up and running!");
    }
  }

  private void tryStartMainActivity()
  {
    try (ThreadName threadName = new ThreadName()) {
      ParseUser user = Parse.getCurrentUser();
      if ((user == null) != (mActivity instanceof LoginActivity)) {
        ThreadUtil.onMain(() ->
        {
          if (user == null) {
            LoginActivity.start(mActivity);
          } else {
            Cell411Activity.start(mActivity);
          }
        });
        waitUntil(this, mActWatcher, 2000);
      }
    }
  }

  private void waitUntil(Object object, Func0<Boolean> func, long interval,
                         long timeout)
  {
    ThreadUtil.waitUntil(object, func, interval,timeout);
  }
  private void waitUntil(Object object, Func0<Boolean> func, long time)
  {
    ThreadUtil.waitUntil(object, func, time, 0);
  }

  private void waitUntil(Func0<Boolean> func, long interval, long timeout)
  {
    waitUntil(this, func, interval, timeout);
  }
  private void waitUntil(Func0<Boolean> func, long interval)
  {
    waitUntil(this, func, interval, 0);
  }

  public void openChat(XEntity cell)
  {
    announce();
  }

  void tryStartImageStore()
  {
    try (ThreadName threadName = new ThreadName()) {
      while (true) {
        try {
          if (ImageStore.opt() != null)
            return;
          ThreadUtil.onMain(() -> mActivity.setDrawerIndicatorEnabled(false));
          Future<ImageStore> future = Cell411.req().startImageStore();
          ImageStore service = future.get();
          if (service == null) {
            throw new NullPointerException("Service is null");
          }
          ImageStore.addReadyObserver(this);
          waitUntil(this, service::isReady, 2000);
          return;
        } catch (Throwable e) {
          mActivity.handleException("staring live query service", e);
        }
      }
    }

  }

  @Override
  public void onChange(@Nullable Object newValue, @Nullable Object oldValue)
  {
    announce();
    ThreadUtil.onExec(() -> ThreadUtil.notify(this, true));
  }

  static class BadState
    implements Func0<Boolean>
  {
    @Override
    public Boolean apply()
    {
      if (Parse.getCurrentUser() == null) {
        announce("State is Bad (No User)");
        return true;
      }
      if (Cell411.req().getMainActivity() instanceof Cell411Activity) {
        return false;
      } else {
        announce("State is Bad (Wrong Activity)");
        return true;
      }
    }

    @NonNull
    public String toString()
    {
      return getClass().getSimpleName();
    }
  }

  static class IsCell411ActLoaded
    implements Func0<Boolean>
  {
    @Override
    public Boolean apply()
    {
      Reflect.announce();
      Cell411 cell411 = Cell411.req();
      MainActivity mainActivity = cell411.getMainActivity();
      return (mainActivity instanceof Cell411Activity);
    }

    @NonNull
    public String toString()
    {
      return getClass().getSimpleName();
    }
  }
}
