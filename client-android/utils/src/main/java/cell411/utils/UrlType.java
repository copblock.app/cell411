package cell411.utils;

import java.net.URL;

public enum UrlType {
  http,
  https,
  file,
  ws,
  wss;

  public static UrlType forUrl(URL url) {
    return forProto(url.getProtocol());
  }

  private static UrlType forProto(String protocol) {
    switch (protocol) {
      case "http":
        return http;
      case "https":
        return https;
      case "ws":
        return ws;
      case "wss":
        return wss;
      case "file":
        return file;
      default:
        throw new RuntimeException("Unexpected Protocol: " + protocol);
    }
  }
}
