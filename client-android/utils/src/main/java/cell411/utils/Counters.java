package cell411.utils;

import java.util.Iterator;

public class Counters {

  static int countNulls(Iterator<Object> iterator) {
    int count = 0;
    while (iterator.hasNext())
      if (iterator.next() == null)
        count++;
    return count;
  }

  static int countNulls(Object... o) {
    int count = 0;
    for (Object value : o)
      if (value == null)
        count++;
    return count;
  }

  static int sumLengths(CharSequence... o) {
    int length = 0;
    int last = 0;
    for (CharSequence value : o) {
      length += value.length();
      if (length < last)
        throw new ArrayIndexOutOfBoundsException("Integer Overflow");
      last = length;
    }
    return length;
  }
}
