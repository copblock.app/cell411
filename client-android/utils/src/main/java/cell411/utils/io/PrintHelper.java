package cell411.utils.io;

import java.util.Arrays;

import cell411.utils.Util;

public interface PrintHelper
{

  String smTrue = "true\n";
  String smNull = "null\n";
  String smFalse = "false\n";
  byte[] smBuf = new byte[256];

  void write(int ch);
  default void write(byte[] bs) {
    write(bs,0,bs.length,false);
  }
  default void write(int ch,boolean nl) {
    write(ch);
    if(nl)
      write(10);
  }
  default void write(byte[] bs,boolean nl) {
    write(bs);
    if(nl)
      write(10);
  }
  default void write(byte[] bs, int off, int len)
  {
    for(int i=0;i<len;i++)
      write(bs[off+i]);
  }
  default void write(byte[] bs,int off,int len,boolean nl) {
    PrintHelper.this.write(bs,off,len);
    if(nl)
      write(10);
  }
  default void write(String str) {
    byte[] bs=str.getBytes();
    write(bs,0,bs.length,false);
  }
  static byte[] getBytes(final String str)
  {
    return (str==null ? "null" : str).getBytes();
  }


  default void close()
  {
  }



  default void flush()
  {
  }


  default void print(char c)
  {
    write(c, false);
  }

  default void print(char[] v)
  {
    for (final char c : v) {
      write(c, false);
    }
  }

  default void print(double v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default void print(float v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default void print(int v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default void print(long v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default void print(String s)
  {
    write(s);
  }

  default void println()
  {
    write(10, false);
  }

  default void println(char x)
  {
    write( x, false);
    write( 10, false);
  }

  default void println(int x)
  {
    byte[] bs = Integer.toString(x).getBytes();
    write(bs, true);
  }

  default void println(long x)
  {
    byte[] bs = Long.toString(x).getBytes();
    write(bs, true);
  }

  default void println(float x)
  {
    byte[] bs = Float.toString(x).getBytes();
    write(bs, true);
  }

  default void println(double x)
  {
    byte[] bs = Double.toString(x).getBytes();
    write(bs, true);
  }

  default void println(char[] x)
  {
    for (final char c : x) {
      write( c, false);
    }
    write( 10, false);
  }

  default PrintHelper p(String fmt, Object... args)
  {
    if (args.length > 0) {
      return p(Util.format(fmt, args));
    } else {
      return p(fmt);
    }
  }

  default PrintHelper p(Object o)
  {
    print(o);
    return this;
  }

  default void print(Object v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default PrintHelper pl(String fmt, Object... args)
  {
    if (args.length > 0) {
      return pl(Util.format(fmt, args));
    } else {
      return pl(fmt);
    }
  }

  default PrintHelper pl(Object o)
  {
    PrintHelper.this.println(o);
    return this;
  }

  default void println(Object x)
  {
    byte[] bs = String.valueOf(x).getBytes();
    PrintHelper.this.write(bs, true);
  }

  default PrintHelper pl()
  {
    PrintHelper.this.println("");
    return this;
  }

  default PrintHelper printf(String format, Object... args)
  {
    PrintHelper.this.write(Util.format(format,args));
    return this;
  }
  default PrintHelper p(int i, String name)
  {
    System.out.println("name: "+name+" i="+i);
    assert name.length() < i - 4;
    assert i < smBuf.length;
    byte[] bytes = name.getBytes();
    System.arraycopy(bytes, 0, smBuf, 0, bytes.length);
    Arrays.fill(smBuf, bytes.length, i, (byte) '.');
    return this;
  }

  default PrintHelper pfl(String fmt, Object ...args) {
    pl(Util.format(fmt,args));
    return this;
  }
  default PrintHelper pf(String fmt, Object ...args) {
    p(Util.format(fmt,args));
    return this;
  }
}
