package cell411.utils.io;

import android.util.Log;

import java.io.OutputStream;

import cell411.utils.Util;
import cell411.utils.reflect.XTAG;

public class TimeLog
  extends SimplePrintStream
  implements PrintHelper
{
  private static final XTAG TAG = new XTAG();
  long mStart = System.currentTimeMillis();
  public TimeLog(OutputStream out) {
    super(out);
  }

  @Override
  public String startLine()
  {
    long now = System.currentTimeMillis();
    return Util.format("%10d: ",now-mStart);
  }

  @Override
  protected void finalize()
  throws Throwable
  {
    Log.i(TAG.toString(),out.toString());
    super.finalize();
  }
}
