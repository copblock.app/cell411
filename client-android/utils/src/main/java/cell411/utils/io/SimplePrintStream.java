package cell411.utils.io;

import javax.annotation.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.annotation.Nonnull;

public class SimplePrintStream
  extends  PrintStream
  implements PrintHelper,
             Closeable,
             Flushable,
             Appendable
{
  private static final TrapStream smTrap = new TrapStream();
  static Charset smCharset = Charset.defaultCharset();
  private boolean smMissedMyConstructor=true;
  protected final OutputStream out;

  HashMap<Thread,ByteArrayOutputStream> mCopy = new HashMap<>();
  class Temp extends ThreadLocal<ByteArrayOutputStream>  {
    @Nullable
    @Override
    protected ByteArrayOutputStream initialValue()
    {
      return new ByteArrayOutputStream();
    }

    @Nullable
    @Override
    public ByteArrayOutputStream get()
    {
      ByteArrayOutputStream res = super.get();
      mCopy.put(Thread.currentThread(),res);
      return res;
    }
  }
  Temp mBuffers = new Temp();

//  ThreadLocal<ByteArrayOutputStream> mBuffers = ThreadLocal.withInitial(
 //   Suppliers.supplier(ByteArrayOutputStream.class)
  //);
  int nw = 0;
  @Override
  public void write(final int b)
  {
    try {
      ByteArrayOutputStream stream = mBuffers.get();
      assert stream!=null;
      if(stream.size()==0)
        out.write(startLine().getBytes());
      stream.write(b);
      if(b==10){
        out.write(stream.toByteArray());
        stream.reset();
      }
      nw++;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void write(final byte[] buf, final int off, final int len)
  {
    for(int i=0;i<len;i++) {
      write(buf[off+i]);
    }
  }

  private byte[] splitBuf(byte[] buf, int off, int len) {
    byte[] res = new byte[len];
    System.arraycopy(buf, off, res, 0, len);
    return res;
  }

  @Override
  public void write(final byte[] buf)
  {
    write(buf,0,buf.length);
//    int p=0;
//    for(int i=0;i<buf.length;i++) {
//      if(buf[i]==10){
//        if(i>p) {
//          write(buf, p, i - p);
//        }
//        write('\n');
//      }
//    }
//    for (byte b : buf)
//      write(b);
  }

  {
    checkOut();
  }

  private void checkOut()
  {
    assert super.out == smTrap;
  }

  public SimplePrintStream(final OutputStream out)
  {
    this(out, true, smCharset);
  }

  public SimplePrintStream(final OutputStream out, final boolean autoFlush,
                           final Charset charset)
  {
    super(smTrap,autoFlush);
    this.out =out;
    // I'm making sure that nobody bypasses this constructor.
    if(smMissedMyConstructor)
      smMissedMyConstructor=false;
    else
      throw new RuntimeException("Who touched my garbage?");
  }

  public SimplePrintStream(final OutputStream out, final boolean autoFlush)
  {
    this(out, autoFlush, smCharset);
  }

  public SimplePrintStream(final OutputStream out, final boolean autoFlush,
                           final String encoding)
  {
    this(out, autoFlush, Charset.forName(encoding));
  }

  public SimplePrintStream(final String fileName)
  {
    this(streamFor(fileName), true, smCharset);
  }

  private static OutputStream streamFor(final String name)
  {
    return IOUtil.newOutputStream(name);
  }

  public SimplePrintStream(final String fileName, final String csn)
  {
    this(IOUtil.newOutputStream(fileName),true,csn);
  }

  public SimplePrintStream(final String fileName, final Charset charset)
  {
    this(IOUtil.newOutputStream(fileName),true,charset);
  }

  public SimplePrintStream(final File file)
  {
    this(IOUtil.newOutputStream(file),true,smCharset);
  }

  public SimplePrintStream(final File file, final String csn)
  {
    this(IOUtil.newOutputStream(file),true,Charset.forName(csn));
  }

  public SimplePrintStream(final File file, final Charset charset)
  {
    this(IOUtil.newOutputStream(file),true,charset);
  }
  public String startLine()
  {
    return "";
  }

  @Override
  public void write(final int ch, final boolean nl)
  {
    PrintHelper.super.write(ch, nl);
  }

  @Override
  public void write(final byte[] bs, final boolean nl)
  {
    PrintHelper.super.write(bs, nl);
  }

  @Override
  public void write(final byte[] bs, final int off, final int len,
                    final boolean nl)
  {
    PrintHelper.super.write(bs, off, len, nl);
  }

  @Override
  public void write(final String str)
  {
    PrintHelper.super.write(str);
  }

  @Override
  public SimplePrintStream p(final String fmt, final Object... args)
  {
    return (SimplePrintStream) PrintHelper.super.p(fmt, args);
  }

  @Override
  public SimplePrintStream p(final Object o)
  {
    return (SimplePrintStream) PrintHelper.super.p(o);
  }

  @Override
  public SimplePrintStream pl(final String fmt, final Object... args)
  {
    return (SimplePrintStream) PrintHelper.super.pl(fmt, args);
  }

  @Override
  public SimplePrintStream pl(final Object o)
  {
    return (SimplePrintStream) PrintHelper.super.pl(o);
  }

  @Override
  public SimplePrintStream pl()
  {
    return (SimplePrintStream) PrintHelper.super.pl();
  }

  @Override
  public SimplePrintStream p(final int i, final String name)
  {
    return (SimplePrintStream) PrintHelper.super.p(i, name);
  }

  @Override
  public SimplePrintStream pfl(final String fmt, final Object... args)
  {
    return (SimplePrintStream) PrintHelper.super.pfl(fmt, args);
  }

  @Override
  public SimplePrintStream pf(final String fmt, final Object... args)
  {
    return (SimplePrintStream) PrintHelper.super.pf(fmt, args);
  }



  public void flush()
  {
  }

  public void print(boolean b)
  {
    PrintHelper.super.print(b);
  }

  public void print(char c)
  {
    PrintHelper.super.print(c);
  }

  public void print(int i)
  {
    PrintHelper.super.print(i);
  }

  public void print(long l)
  {
    PrintHelper.super.print(l);
  }

  public void print(float f)
  {
    PrintHelper.super.print(f);
  }

  public void print(double d)
  {
    PrintHelper.super.print(d);
  }

  public void print(char[] s)
  {
    PrintHelper.super.print(s);
  }

  public void print(String s)
  {
    PrintHelper.super.print(s);
  }

  public void print(Object obj)
  {
    PrintHelper.super.print(obj);
  }

  public void println()
  {
    PrintHelper.super.println();
  }

  public void println(boolean x)
  {
    PrintHelper.super.println(x);
  }

  public void println(char x)
  {
    PrintHelper.super.println(x);
  }

  public void println(int x)
  {
    PrintHelper.super.println(x);
  }

  public void println(long x)
  {
    PrintHelper.super.println(x);
  }

  public void println(float x)
  {
    PrintHelper.super.println(x);
  }

  public void println(double x)
  {
    PrintHelper.super.println(x);
  }

  public void println(char[] x)
  {
    PrintHelper.super.println(x);
  }


  byte[] getBytes(String str, boolean nl)
  {
    if(nl)
      str=str+"\n";
    return (str==null ? "null" : str).getBytes();
  }
  public void println(String x)
  {
    write(getBytes(x,true));
  }

  public void println(Object x)
  {
    PrintHelper.super.println(x);
  }

  public SimplePrintStream printf(String format, Object... args)
  {
    write(String.format(format,args));
    return this;
  }

  public SimplePrintStream printf(Locale l, String format, Object... args)
  {
    write(String.format(format,args));
    return this;
  }

  public SimplePrintStream format(String format, Object... args)
  {
    write(String.format(format,args));
    return this;
  }

  public SimplePrintStream format(Locale l, String format, Object... args)
  {
    write(String.format(l,format,args));
    return this;
  }

  @Nonnull
  public SimplePrintStream append(CharSequence csq)
  {
    write(String.valueOf(csq));
    return this;
  }

  @Nonnull
  public PrintStream append(CharSequence csq, int start, int end)
  {
    write(String.valueOf(csq).substring(start,end));
    return this;
  }

  @Nonnull
  public PrintStream append(char c)
  {
    write(c);
    return this;
  }

  private static class TrapStream
    extends OutputStream
  {
    @Override
    public void write(int b)
    {
      throw new RuntimeException("It's a trap!");
    }

    @Override
    public void write(final byte[] b)
    {
      throw new RuntimeException("It's a trap!");
    }

    @Override
    public void write(final byte[] b, final int off, final int len)
    {
      throw new RuntimeException("It's a trap!");
    }
  }
}
