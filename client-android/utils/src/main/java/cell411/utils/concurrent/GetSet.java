package cell411.utils.concurrent;

import cell411.utils.Util;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;

public class GetSet<X>
{
  private final Object mObject;
  private final Class<X> mType;
  private final Field mField;

  public GetSet(Object object, Field field, Class<X> type)
  {
    mObject = object;
    mType = type;
    mField = field;
  }

  public static <X>
  GetSet<X> getGetSet(Object obj, String name, Class<X> type) {
    try {
      Class<?> owningType = obj.getClass();
      Field field = owningType.getField(name);
      return new GetSet<>(obj, field, type);
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }

  public X get()
  {
    try {
      return mType.cast(mField.get(mObject));
    } catch (IllegalAccessException e) {
      throw Util.rethrow(e);
    }
  }

  public void set(X res)
  {
    try {
      mField.set(mObject, res);
    } catch (IllegalAccessException e) {
      throw Util.rethrow(e);
    }
  }

  @Nonnull
  public String toString()
  {
    return "GetSet<" + mType.getSimpleName() + ">." + mField.getName();
  }

  public String getName()
  {
    return mField.getName();
  }

  @SuppressWarnings("unchecked")
  public Class<? super X> getType()
  {
    return (Class<? super X>) mField.getType();
  }
}
