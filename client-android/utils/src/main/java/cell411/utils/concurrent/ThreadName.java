package cell411.utils.concurrent;

import cell411.utils.reflect.Reflect;

public class ThreadName
  implements AutoCloseable
{
  Thread mThread = Thread.currentThread();
  String mOldName = mThread.getName();

  public ThreadName() {
    this(Reflect.currentMethodName(1));
  }
  public ThreadName(String name)
  {
    mThread.setName(name);
  }

  @Override
  protected void finalize()
  {
    resetName();
  }

  private void resetName()
  {
    if (mOldName != null) {
      String oldName = mOldName;
      mOldName = null;
      mThread.setName(oldName);
      mThread = null;
    }
  }

  @Override
  public void close()
  {
    resetName();
  }
}
