package cell411.utils.concurrent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import cell411.utils.reflect.Reflect;

public class RunnableMethod<X>
  implements Runnable,
             RunOrCall<X>
{
  Object mTarget;
  Method mMethod;
  Object[] mArgs;

  public RunnableMethod(Object target, Method method, Object[] args)
  {
    mTarget = target;
    mMethod = method;
    mArgs = args;
    assert((mTarget==null)==(isStatic(mMethod)));
  }
  static boolean isStatic(Method method) {
    return Reflect.isStatic(method);
  }

  public static <X>
  RunnableMethod<X> forVirtual(
    Object target, String name,
    Object ... args)
  {
    Method method = Reflect.getMethod(null,
    target, name, args);
    return new RunnableMethod<>(target,method,args);
  }

  boolean isStatic() {
    return isStatic(mMethod);
  }
  @Override
  public void run()
  {
    try {
//      System.out.println("Calling "+mMethod);
//      System.out.println("   args: "+ Reflect.argsToString(mArgs));
      mMethod.invoke(mTarget, mArgs);
    } catch (IllegalAccessException | InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public X call()
  throws Exception
  {
    run();
    return null;
  }
}
