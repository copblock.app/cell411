package cell411.utils.concurrent;

public class RunOrCallRun<X>
  implements RunOrCall<X>
{
  final Runnable mRunnable;
  final X mResult;

  public RunOrCallRun(Runnable runnable, X result)
  {
    mRunnable = runnable;
    mResult = result;
  }

  @Override
  public void run()
  {
    if(mRunnable!=null)
      mRunnable.run();
  }

  @Override
  public X call()
  {
    mRunnable.run();
    return mResult;
  }
}
