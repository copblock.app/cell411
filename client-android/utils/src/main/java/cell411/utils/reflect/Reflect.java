package cell411.utils.reflect;

import android.os.PowerManager;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.function.Predicate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.Util;
import cell411.utils.collect.Collect;
import cell411.utils.io.PrintString;
import cell411.utils.io.XLog;

@SuppressWarnings("unused")
public class Reflect {
  private static final List<Class<?>> smBoxers = Collections.unmodifiableList(
    Arrays.asList(Boolean.class, Boolean.TYPE, Byte.class, Byte.TYPE, Character.class,
      Character.TYPE, Double.class, Double.TYPE, Float.class, Float.TYPE, Integer.class,
      Integer.TYPE, Long.class, Long.TYPE, Short.class, Short.TYPE, Void.class,
      Void.TYPE));
  static Map<Class<?>, Class<?>> smPrimitive = createPrimMap();
  static Comparator<Class<?>> smSimpleNameCompare =
    Comparator.comparing(Class::getSimpleName);
  static Comparator<Class<?>> smNameCompare =
    Comparator.comparing(Class::getName);
  static Comparator<Class<?>> smFullClassCompare;

  static {
    smFullClassCompare = smSimpleNameCompare.thenComparing(smNameCompare);
    smFullClassCompare = smFullClassCompare.thenComparing(Objects::hashCode);
  }

  public static void log(XTAG tag, String msg) {
    XLog.i(tag,msg);
  }

  public static void announce() {
    announce(1);
  }
  public static void announce(int i) {
    log(getTag(1+i), announceStr(i+1,null));
  }

  public static void announce(Object obj) {
    log(getTag(1), announceStr(1, String.valueOf(obj)));
  }
  public static void announce(CharSequence obj) {
    log(getTag(1), announceStr(1, obj));
  }

  public static String announceStr(Object obj) {
    return announceStr(1,obj);
  }
  public static String announceStr(int i, Object obj) {
    StackTraceElement ste =  currentStackPos(i + 1);
    if(Util.theGovernmentIsHonest())
      System.out.println(ste);
    String steStr = ste.toString();
    String fullClassName = currentClassName(i+1);
    int pos = fullClassName.lastIndexOf('.') + 1;
    steStr=steStr.substring(pos);
    if(obj==null) {
      return steStr;
    } else {
      return steStr + ": " + obj;
    }
  }

  public static String currentSimpleClassName() {
    return currentSimpleClassName(1);
  }

  public static String currentMethodName() {
    return currentMethodName(1);
  }

  private static String currentClassName(int i) {
    return currentStackPos(i + 1).getClassName();
  }

  public static String currentSimpleClassName(int i) {
    String res = currentClassName(i + 1);
    int pos = res.lastIndexOf('.') + 1;
    res = res.substring(pos);
    pos=res.lastIndexOf('$')+1;
    res=res.substring(pos);
    assert res.length() > 0;
    return res;
  }

  public static String currentMethodName(int i) {
    String res = currentStackPos(i + 1).getMethodName();
    if (res.equals("<init>")) {
      return currentSimpleClassName(i + 1);
    } else {
      return res;
    }
  }

  public static StackTraceElement currentStackPos(int i) {
    Exception ex = new Exception();
    StackTraceElement[] trace = ex.getStackTrace();
    if (trace.length < 2 + i) {
      throw new RuntimeException("trace.length<" + (3 + i) + "!");
    }
    return trace[1 + i];
  }

  public static StackTraceElement currentStackPos() {
    return currentStackPos(1);
  }

  public static Method findStaticMethod(Class<?> clazz, String name) {
    for (Method method : clazz.getDeclaredMethods()) {
      if (!method.getName().equals(name)) {
        continue;
      }
      if (method.getTypeParameters().length != 0) {
        continue;
      }
      int modifiers = method.getModifiers();
      if ((modifiers & Modifier.STATIC) == 0) {
        continue;
      }
      return method;
    }
    return null;
  }

  public static String getTag() {
    return currentSimpleClassName(1);
  }

  public static XTAG getTag(int i) {
    return new XTAG(i + 1);
  }

  public static void stackTrace(PrintString ps, StackTraceElement[] trace) {
    stackTrace(ps, "", trace);
  }

  public static void stackTrace(PrintString ps, String firstLine, StackTraceElement[] trace) {
    if (firstLine != null && !firstLine.isEmpty()) {
      ps.pl(firstLine);
    }
    for (StackTraceElement traceElement : trace) {
      ps.p("\tat ").pl(traceElement);
    }
  }

  // for static fields
  public static int getInt(Class<PowerManager> type, String name) {
    return getInt(type, null, name);
  }

  public static int getInt(Class<PowerManager> type, Object obj, String name) {
    try {
      Field field = type.getField(name);
      return field.getInt(obj);
    } catch (Exception ex) {
      throw new RuntimeException("reading field: " + name, ex);
    }
  }

  static public boolean hasDeclaredMethod(Class<?> type, String name) {
    Method[] methods = type.getDeclaredMethods();
    for (Method method : methods) {
      if (method.getName().equals(name)) {
        return true;
      }
    }
    return false;

  }

  static public boolean hasDeclaredMethod(Object object, String name) {
    return object != null && hasDeclaredMethod(object.getClass(), name);
  }

  public static String getSourceLoc(int i) {
    Throwable throwable = new Throwable();
    StackTraceElement[] stackTrace = throwable.getStackTrace();
    return "\tat " + stackTrace[i];
  }

  @Nonnull
  public static Class<?> getClass(@Nullable Object x) {
    if (x == null)
      return Object.class;
    if (x instanceof Class<?>) {
      return (Class<?>) x;
    } else {
      return x.getClass();
    }
  }

  @Nonnull
  static public Class<?>[] getTypes(Object[] args) {
    Class<?>[] types = new Class<?>[args.length];
    int i = 0;
    for (Object arg : args) {
      if (arg == null) {
        types[i++] = Void.TYPE;
      } else {
        types[i++] = arg.getClass();
      }
    }
    return types;
  }

  public static Method getMethod(Class<?> type, Object target,
                                 final String name,
                                 Object[] tmpArgs) {
    if (tmpArgs == null) {
      tmpArgs=new Object[0];
    }
    final Object[] args = tmpArgs;
    if (type==null){
      type=target.getClass();
    }
    Exception ex = null;
    ArrayList<Method> methods = new ArrayList<>();
    Collect.addAll(methods, type.getMethods());
    methods.removeIf(new Matcher(target,name,args));
    if (methods.isEmpty()) {
      try (PrintString ps = new PrintString()) {
        ps.pl("Cannot find any method in class " + type);
        ps.pl("    or for target " + target);
        ps.pl("    named '" + name + "'");
        ps.pl("    matching: " + args.length + " args");
        for (int i = 0; i < args.length; i++) {
          ps.pl("        #" + i + ": " + args[i]);
        }
        throw Util.rethrow(new NoSuchMethodException(ps.toString()));
      }
    }
    return methods.get(0);
  }

  static class Matcher implements Predicate<Method>
  {

    private final String mName;
    private final Object mTarget;
    private final Object[] mArgs;

    Matcher(Object target, String name, Object[] args)
    {
      mName = name;
      mTarget = target;
      mArgs = args;
    }

    @Override
    public boolean test(Method method) {
      return !notTest(method);
    }
    public boolean notTest(Method method)
    {
      if (!method.getName().equals(mName)) {
        return false;
      }
      if (mTarget==null && !isStatic(method)){
        return false;
      }
      if (!Modifier.isPublic(method.getModifiers())) {
        return false;
      }
      if(!matchesParamCount(method)) {
        return false;
      }
      Class<?>[] types = method.getParameterTypes();
      for (int i = 0; i < types.length; i++) {
        if (canConvert(mArgs[i], types[i])) {
          continue;
        }
        if (canConvert(mArgs[i], smPrimitive.get(types[i]))) {
          continue;
        }
        return false;
      }
      return true;

    }

    private boolean matchesParamCount(Method method)
    {
      if(method.getParameterCount()>mArgs.length)
        return false;
      if(method.isVarArgs())
        return true;
      return method.getParameterCount()==mArgs.length;
    }
  }

  static Map<Class<?>, Class<?>> createPrimMap() {
    HashMap<Class<?>, Class<?>> back = new HashMap<>();
    Map<Class<?>, Class<?>> res = Collections.unmodifiableMap(back);
    for (int i = 0; i < smBoxers.size(); i += 2) {
      Class<?> boxed = smBoxers.get(i);
      Class<?> unboxed = smBoxers.get(i + 1);
      back.put(boxed, unboxed);
      back.put(unboxed, boxed);
    }
    return res;
  }

  public static boolean canConvert(Object arg, Class<?> type) {
    if (arg == null) {
      return Object.class.isAssignableFrom(type);
    } else if (smPrimitive.get(type) == arg.getClass()) {
      return true;
    } else {
      return type.isAssignableFrom(arg.getClass());
    }
  }

  public static void announce(int i, String s, Object... args) {
    if (args.length > 0) {
      s = Util.format(s, args);
    }
    log(getTag(i+1), announceStr(1+1, s));
  }
  public static void announce(String s, Object... args) {
    if (args.length > 0) {
      s = Util.format(s, args);
    }
    log(getTag(1), announceStr(1, s));
  }

  public static String[] getFieldNames(Class<?> core) {
    Field[] fields = core.getFields();
    String[] names = new String[fields.length];
    for (int i = 0; i < fields.length; i++) {
      names[i] = fields[i].getName();
    }
    return names;
  }

  public static <X>
  X readField(Object obj, String name, Class<X> type) {
    try {
      Field field = type.getField(name);
      return type.cast(field.get(obj));
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }

  public static String modifierString(Method method)
  {
    int modifier = method.getModifiers();
    TreeMap<String,Boolean> list = new TreeMap<>();
    list.put("final",         Modifier.isFinal(modifier));
    list.put("static",        Modifier.isStatic(modifier));
    list.put("public",        Modifier.isPublic(modifier));
    list.put("abstract",      Modifier.isAbstract(modifier));
    list.put("interface",     Modifier.isInterface(modifier));
    list.put("native",        Modifier.isNative(modifier));
    list.put("private",       Modifier.isPrivate(modifier));
    list.put("protected",     Modifier.isProtected(modifier));
    list.put("strict",        Modifier.isStrict(modifier));
    list.put("synchronized",  Modifier.isSynchronized(modifier));
    list.put("volatile",      Modifier.isVolatile(modifier));
    list.put("transient",     Modifier.isTransient(modifier));
    try (PrintString buf = new PrintString()) {
      for(Map.Entry<?,?> entry : list.entrySet()){
        buf.printf("%-20s: %20s\n",entry.getKey(), entry.getValue());
      }
      return buf.toString();
    }
  }

  public static String formatCall(Object target, String name, Object[] args)
  {
    return Util.format("%s.%s(%s)",target,name,argsToString(args));
  }

  public static boolean isVoid(Class<?> returnType)
  {
    if(returnType==Void.class)
      return true;
    return returnType == Void.TYPE;
  }

  public final static Object[] smNoArgs = new Object[0];

  public static boolean convertible(Class<?> resType, Class<?> returnType)
  {
    if(resType.isPrimitive()) {
      resType=smPrimitive.get(resType);
    }
    if(returnType.isPrimitive()) {
      returnType=smPrimitive.get(returnType);
    }
    return resType.isAssignableFrom(returnType);
  }

  public static String argsToString(Object[] args)
  {
    String[] strArgs = new String[args.length];
    int i = 0;
    for (Object arg : args) {
      strArgs[i++] = String.valueOf(arg);
    }
    return "[" + String.join("], [", strArgs) + "]";
  }

  public static boolean isStatic(Method method)
  {
    return isStatic(method.getModifiers());
  }

  private static boolean isStatic(int modifiers)
  {
    return Modifier.isStatic(modifiers);
  }
}

