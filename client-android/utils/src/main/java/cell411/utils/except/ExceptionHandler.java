package cell411.utils.except;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.OnCompletionListener;
import cell411.utils.Util;

public interface ExceptionHandler {
  default void showYesNoDialog(String title, String text,
                               OnCompletionListener listener)
  {
    TheWolf.showYesNoDialog(title, text, listener);
  }


  default void showToast(String message) {
    System.out.println("showToast(" + message + ")");
    TheWolf.showToast(message);
  }

  default void handleException(@Nonnull String activity, @Nonnull Throwable pe,
                               @Nullable OnCompletionListener listener, final boolean dialog) {
    TheWolf.handleException(activity, pe, listener, dialog);
  }

  // Anything which calls overloads on this should be below this comment.
  // Anything which calls out to app() should be above it.

  default OnCompletionListener getListener() {
    return success ->
    {
    };
  }

  default void handleException(@Nonnull String activity, @Nonnull Throwable pe) {
    pe.printStackTrace();
    handleException(activity, pe, getListener(), true);
  }

  default void showToast(String format, Object... args) {
    showToast(Util.format(format, args));
  }

  default void showToast(int format, Object... args) {
    showToast(Util.format(format, args));
  }

  default void showAlertDialog(String title, String message,
                               OnCompletionListener listener) {
    TheWolf.showAlertDialog(title,message, listener);
  }
  default void showAlertDialog(String title, String format, Object... args) {
    showAlertDialog(title, Util.format(format, args), getListener());
  }

  default void showAlertDialog(String title, int format, Object... args) {
    showAlertDialog(title, Util.format(format, args));
  }

}
