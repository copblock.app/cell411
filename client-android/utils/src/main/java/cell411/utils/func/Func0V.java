package cell411.utils.func;

public interface Func0V
  extends Runnable
{
  void apply();

  default void run() {
    apply();
  }
  default <R> Func0<R> returning(R r){
    return Func0.create(this,r);
  }
}
