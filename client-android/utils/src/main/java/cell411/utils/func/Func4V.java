package cell411.utils.func;

public interface Func4V<A1, A2, A3, A4>
{
  void apply(A1 a1, A2 a2, A3 a3, A4 a4);

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func3V<B2, B3, B4> getClosure1( B1 b1) {
    return (b2, b3, b4) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func3V<B1, B3, B4> getClosure2( B2 b2) {
    return (b1, b3, b4) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func3V<B1, B2, B4> getClosure3( B3 b3) {
    return (b1, b2, b4) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func3V<B1, B2, B3> getClosure4( B4 b4) {
    return (b1, b2, b3) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func2V<B2, B3> getClosure14( B1 b1, B4 b4) {
    return (b2, b3) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func2V<B1, B3> getClosure24( B2 b2, B4 b4) {
    return (b1, b3) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func2V<B1, B2> getClosure34( B3 b3, B4 b4) {
    return (b1, b2) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func1V<B3> getClosure124( B1 b1, B2 b2, B4 b4) {
    return (b3) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func1V<B2> getClosure134( B1 b1, B3 b3, B4 b4) {
    return (b2) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func1V<B1> getClosure234( B2 b2, B3 b3, B4 b4) {
    return (b1) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func0V getClosure( B1 b1, B2 b2, B3 b3, B4 b4)
  {
    return () -> apply(b1, b2, b3, b4);
  }
}
