package cell411.utils.collect;

import javax.annotation.Nullable;

public class ObservableValueRW<X>
  extends ObservableValue<X>
{
  public ObservableValueRW()
  {
    this(null,null);
  }
  public <T extends X> ObservableValueRW(T value)
  {
    this(null,null);
  }

  public <T extends X> ObservableValueRW(Class<X> type, T t)
  {
    super(type,t);
  }

  public boolean set(X newValue) {
    if(mAlwaysFire){
      X oldValue=mValue;
      mValue=newValue;
      fireStateChange(oldValue);
      return true;
    }
    if (mValue == newValue) {
      return false;
    }
    if (mValue != null && mValue.equals(newValue)) {
      return false;
    }
    X oldValue = mValue;
    mValue = newValue;
    fireStateChange(oldValue);
    return true;
  }

  public ValueObserver<X> getObserver() {
    return (newValue, oldValue) -> set(newValue);
  }

  @Override
  public void onChange(@Nullable X newValue, @Nullable X oldValue)
  {
    set(newValue);
    super.onChange(newValue, oldValue);
  }
}
