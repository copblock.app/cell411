package cell411.utils.collect;

import javax.annotation.Nullable;

public interface ValueObserver<X> {
  void onChange(@Nullable X newValue, @Nullable X oldValue);

  default boolean done() {
    return false;
  }
}
