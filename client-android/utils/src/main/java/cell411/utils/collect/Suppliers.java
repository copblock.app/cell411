package cell411.utils.collect;

import cell411.utils.Util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.TreeMap;
import java.util.function.Supplier;

public class Suppliers
{
  public static <X> Supplier<X> supplier(Class<X> type)
  {
    return new NewInst<>(type);
  }

  public static class NewInst<T>
    implements Supplier<T>
  {
    private final Class<T> mParseUserClass;

    public NewInst(Class<T> parseUserClass)
    {
      mParseUserClass = parseUserClass;
    }

    @Override
    public T get()
    {
      try {
        return mParseUserClass.newInstance();
      } catch (IllegalAccessException | InstantiationException e) {
        throw Util.rethrow(e);
      }
    }
  }
}
