package cell411.utils;

public class Pair<T1, T2>
{
  public final T1 mVal1;
  public final T2 mVal2;

  public Pair(T1 v1, T2 v2)
  {
    mVal1 = v1;
    mVal2 = v2;
  }
}
