package cell411.ui.base;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.OnCompletionListener;

public interface RealMessageForwarder
{
  void realShowToast(String msg);

  void realHandleException(@Nonnull String whileDoing, @Nonnull Throwable pe,
                           @Nullable OnCompletionListener listener,
                           boolean dialog);


  void realShowYesNoDialog(String title, String text, OnCompletionListener listener);
}
