package cell411.config;

import java.io.File;

import okhttp3.OkHttpClient;

public interface Config
{
  int                           getVersionCode();
  OkHttpClient.Builder getHttpClientBuilder();
  String                        getAppId();
  String                        getAppName();
  String                        getAppVersion();
  String                        getClientKey();
  String                        getFlavor();
  String                        getPackageName();
  String                        getParseUrl();
  String                        getString(int            resId);
  String                        getVersionName();

  File getExtDir(String sub);

  File getParseDir();

  boolean isConnected();

  File getAvatarDir();

  File getPictureDir();

  File getDocDir();

  File getPicDir();

  File getTempDir();

  File getCacheDir(String keyval);
}
