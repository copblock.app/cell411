package cell411.config;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;


import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.GZIPOutputStream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.Pair;
import cell411.utils.Util;
import okhttp3.OkHttpClient;

public class ConfigDepot
{
  static final AtomicReference<Config> smConfig =
    new AtomicReference<>();

  private ConfigDepot()
  {
    assert false;
  }
  public static void init(Config config){
    if(!smConfig.compareAndSet(null,config))
      throw new IllegalStateException("ConfigDepot.init() called twice");
  }
  @Nullable
  static public Config opt() {
    return smConfig.get();
  }
  @Nonnull
  static public Config req() {
    return Util.req(opt());
  }

  public static boolean isConnected()
  {
    return req().isConnected();
  }

  public static File getPictureDir()
  {
    return req().getPictureDir();
  }


  public static int getVersionCode()
  {
    return req().getVersionCode();
  }
  static public File getCurrentStoreDir()
  {
    return new File(getParseDir(), "config");
  }


  public static File getFlavorDocDir()
  {
    return mkdirs(new File(getDocDir(), getFlavor()));
  }

  public static String getFlavor()
  {
    return req().getFlavor();
  }

  public static File mkdirs(File file, String... subs)
  {
    if (file == null)
      return null;
    if (subs != null && subs.length > 0)
      return mkdirs(mkname(file, subs));
    else if (file.exists())
      return file;
    else if (file.mkdirs())
      return file;
    else
      return null;
  }

  public static File mkname(File file, String... subs)
  {
    return new File(file, String.join("/", subs));
  }

  public static File getDocDir()
  {
    return req().getDocDir();
  }
  public static File getPicDir()
  {
    return req().getPicDir();
  }

  public static String getVersionName()
  {
    return req().getVersionName();
  }
  public static String getString ( int resId)
  {
    return req().getString(resId);
  }

  public static String getClientKey () {
    return req().getClientKey();
  }

  public static String getAppId () {
    return req().getAppId();
  }
  public static String getParseUrl () {
    return req().getParseUrl();
  }
  public static OkHttpClient getHttpClient () {
    return getHttpClientBuilder().build();
  }


  public static OkHttpClient.Builder getHttpClientBuilder () {
    return opt().getHttpClientBuilder();
  }

  public static String getPackageName () {
    return opt().getPackageName();
  }

  public static String getAppName () {
    return opt().getAppName();
  }

  public static String getAppVersion () {
    return opt().getAppVersion();
  }
  public static File getJsonCacheFile (String name){
    if (!name.endsWith(".json"))
      name = name + ".json";
    File file = getFlavorDocDir();
    file = new File(file, "data");
    return new File(file, name);
  }
  public static File getLogDir() {
    return mkdirs(new File(getFlavorDocDir(), "logs"));
  }
  @Nullable
  public static Pair<File,OutputStream> getNextLogFile(String prefix,
                                                         boolean gz)
  {
    File logDir = getLogDir();
    for(int serial = 10000; serial<99999; serial++) {
      String name = prefix+"-" + serial + ".log";
      String gzip = name+".gz";
      File nFile = new File(logDir,name);
      File gFile = new File(logDir,gzip);
      if(nFile.exists() || gFile.exists())
        continue;
      try {
        if(gz)
          nFile=gFile;
        OutputStream fos= Files.newOutputStream(nFile.toPath(),CREATE, APPEND);
        if(gz)
          fos=new GZIPOutputStream(fos);
        fos.write("\n\n\nFile Opened\n".getBytes());
        return new Pair<>(nFile,fos);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
    throw new RuntimeException("All filee names used");
  }

  public static File getAvatarDir() {
    return mkdirs(req().getAvatarDir());
  }
  public static File getParseDir()
  {
    return mkdirs(req().getParseDir());
  }

  public static File getTempDir() {
    return req().getTempDir();
  }

  public static File getCacheDir(String keyval) {
    return req().getCacheDir(keyval);
  }
}
//
//
//
//
//
//
//
//
//    public static File getMediaOutputDir (String type){
//    return getConfig().getMediaOutputDir(type);
//  }
//
//    public static File getPictureDir () {
//    return mkdirs(getExtDir(DIRECTORY_PICTURES));
//  }
//
//   public static File getKeyValDir () {
//      return mkdirs(getFilesDir(), "keyValue");
//
//}
//}
//
///  File getMediaOutputDir(String type)
//  {
//    return new File(getExtDir(type), "cell411/" + getFlavor());
//  }
//
//  public abstract String getFlavor();
//
//
//  File getFilesFlavoredDir()
//  {
//    return new File(getFilesDir(), getFlavor());
//  }
//
//
//  public static String getFlavor()
//  {
//    return getConfig().getFlavor();
//  }
//
//  public static File mkname(File file, String... subs)
//  {
//    return new File(file, String.join("/", subs));
//  }
//
//
//  public static Bitmap createPlaceholder()
//  {
//    Bitmap placeHolder = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888);
//    Canvas canvas = new Canvas(placeHolder);
//
//    int id = R.drawable.ic_placeholder_user;
//    Paint whitePaint = new Paint();
//    whitePaint.setColor(0xff000000);
//    whitePaint.setStyle(Paint.Style.FILL);
//    canvas.drawRect(0, 0, 300, 300, whitePaint);
//
//    OvalShape ovalShape = new OvalShape();
//    final Drawable background = new ShapeDrawable(ovalShape);
//    final int primaryColor = BaseApp.get().getPrimaryColor();
//
//    background.setTint(0xff000000);
//    background.setBounds(0, 0, 300, 300);
//    background.draw(canvas);
//
//    background.setTint(primaryColor);
//    background.setBounds(20, 20, 280, 280);
//    background.draw(canvas);
//
//    final Drawable drawable =
//      AppCompatResources.getDrawable(ConfigDepot.getContext(), id);
//    int extra = 300 - 262;
//    if (drawable != null) {
//      drawable.setBounds(extra / 2, extra / 2, 300 - extra / 2, 300 - extra / 2);
//      drawable.draw(canvas);
//    }
//    return placeHolder;
//
//
//    public STPExecutorPlus getExec () {
//    return getExecutor();
//  }
//    public static STPExecutorPlus getExecutor () {
//    return getConfig().getExecutor();
//  }
//    public static int getVersionCode () {
//    return getConfig().getVersionCode();
//  }
