package cell411.config;

import android.app.Application;
import android.content.Context;

import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.Util;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.except.ExceptionHandler;
import cell411.utils.io.XLog;
import cell411.utils.reflect.Reflect;
import cell411.utils.reflect.XTAG;

public class UtilApp
  extends Application
  implements ExceptionHandler {
  @Nonnull
  protected static final AtomicReference<UtilApp> smRef = new AtomicReference<>();
  private final static XTAG TAG = new XTAG();

  static {
    XLog.w(TAG, "App is starting");
  }

  protected static boolean ready() {
    return smRef.get()!=null;
  }
//  static void announce(String str) {
//    XLog.i(TAG, Reflect.announceStr(1,str));
//  }
//  private final OutputStream mStream;
  // private final File file;
//  {
//    try {
//
//      String fname="/data/user/0/com.safearx.cell411/files/avatars/logfile";
//      fname=fname+ Instant.now();
//      fname=fname+".log";
//      file = new File(fname);
//      IOUtil.mkdirs(file.getParentFile());
//      mStream= new FileOutputStream(file);
//      mLog = new TimeLog(mStream);
//    } catch (FileNotFoundException e) {
//      throw new RuntimeException(e);
//    }

  //  }
//  private final PrintStream mInitLog = System.out;
//  public String getLogText() {
//    return IOUtil.fileToString(file);
//  }
//  public TimeLog getLog() {
//    return mLog;
//  }
//  File filename;
  //private final TimeLog mLog=null;// FIXME = new TimeLog(System.out);
  public UtilApp() {
    super();
    Reflect.announce("Building UtilApp");
  }

  @Nonnull
  public static UtilApp req() {
    synchronized(smRef) {
      return Util.req(opt());
    }
  }
  @Nonnull
  public static Future<UtilApp> fut() {
    synchronized(smRef) {
      ThreadUtil.waitUntil(smRef,UtilApp::ready,1000);
      return ThreadUtil.submit
        (
          ()-> Util.req(opt())
        );
    }
  }


  @Nullable
  public static UtilApp opt() {
    synchronized(smRef) {
      return Util.getRef(smRef);
    }
  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
//    System.out.println("About to set pointer!");
    synchronized (smRef) {
      ThreadUtil.sleep(3000);
      if (!smRef.compareAndSet(null, this)) {
        throw new RuntimeException("There can be only one!");
      }
    }
  }

  @Override
  public void onCreate() {
    Reflect.announce();
    super.onCreate();
  }

}
