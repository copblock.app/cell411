package cell411.model;

import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.model.ParseObject;

@ParseClassName("Update")
public class XUpdate
  extends ParseObject
{

  public static ParseQuery<XUpdate> q()
  {
    return ParseQuery.getQuery(XUpdate.class);
  }
}
