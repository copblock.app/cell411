package cell411.model;

import javax.annotation.Nonnull;

public class CountryInfo
{
  public String name;
  public String dialingCode;
  public String shortCode;
  public int flagId;
  public boolean selected;

  public CountryInfo(String name, String dialingCode, String shortCode)
  {
    this.name = name;
    this.dialingCode = dialingCode;
    this.shortCode = shortCode;
  }

  @Override
  @Nonnull
  public String toString()
  {
    return name;
  }
}

