package cell411.model;

import cell411.json.JSONObject;

public interface JSONCallback
{
  void done(boolean success, JSONObject result);
}
