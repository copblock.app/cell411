package cell411.model;

import com.parse.decoder.ParseDecoder;

import cell411.enums.EntityType;
import cell411.json.JSONObject;

public abstract class XEntity
  extends XObject
{
  public abstract EntityType getType();

  public XChatRoom getChatRoom()
  {
    return (XChatRoom) getParseObject("chatRoom");
  }

  public void setChatRoom(XChatRoom chatRoom)
  {
    put("chatRoom", chatRoom);
  }

  @Override
  public State mergeFromServer(State state, JSONObject json,
                               ParseDecoder decoder, boolean completeData)
  {
    State res = super.mergeFromServer(state, json, decoder, completeData);
    Object chatRoom = res.get("chatRoom");
    if (chatRoom instanceof XChatRoom) {
      throw new RuntimeException("FIXME!");
      //setEntity((XChatRoom) chatRoom, this);
    }
    return res;
  }

  abstract public String getEntityName();
}
