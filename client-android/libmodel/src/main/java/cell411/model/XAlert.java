package cell411.model;

import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.model.ParseGeoPoint;

import java.net.URL;

import javax.annotation.Nullable;

import cell411.enums.EntityType;
import cell411.enums.ProblemType;
import cell411.utils.UrlUtils;
import cell411.utils.Util;

@ParseClassName("Alert")
public class XAlert
  extends XEntity
{
  private String mFormatCreatedAt;

  public static ParseQuery<XAlert> q()
  {
    return ParseQuery.getQuery(XAlert.class);
  }

  public URL getPhoto()
  {
    return UrlUtils.toURL(getString("photoDownloadURL"));
  }

  public URL getVideoLink()
  {
    return UrlUtils.toURL(getString("videoDownloadURL"));
  }

  public URL getVideoStreamLink()
  {
    return UrlUtils.toURL(getString("videoStreamURL"));
  }

  public XUser getForwardedBy()
  {
    return (XUser) getParseUser("forwardedBy");
  }

  @Nullable
  public String getNote()
  {
    return getString("note");
  }

  @Nullable
  public String getFormatCreatedAt()
  {
    if (mFormatCreatedAt == null) {
      mFormatCreatedAt = Util.formatDateTime(getCreatedAt());
    }
    return mFormatCreatedAt;
  }

  public String getStatus()
  {
    return getString("status");
  }

  public ParseGeoPoint getLocation()
  {
    return getParseGeoPoint("location");
  }

  public boolean isSelfAlert()
  {
    XUser owner = getOwner();
    if (owner == null) {
      return false;
    }
    XUser currentUser = (XUser) Parse.getCurrentUser();
    if (currentUser == null) {
      return false;
    }
    return owner.hasSameId(currentUser);
  }

  public boolean isGlobal()
  {
    return getBoolean("isGlobal");
  }

  @Override
  public EntityType getType()
  {
    return EntityType.ALERT;
  }

  @Override
  public String getEntityName()
  {
    return "Chat for " + getProblemType() + " alert from " +
      getOwner().getName();
  }

  public ProblemType getProblemType()
  {
    return ProblemType.fromString(getString("problemType"));
  }

  public void setProblemType(final ProblemType arrested)
  {
    if (arrested == null) {
      remove("problemType");
    } else {
      String value = arrested.toString();
      value = String.valueOf(ProblemType.fromString(value));
      put("problemType", value);
    }
  }

}

