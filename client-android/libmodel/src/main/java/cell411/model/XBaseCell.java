package cell411.model;

import static cell411.model.XPublicCell.mApp;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import cell411.enums.CellStatus;

public abstract class XBaseCell
  extends XEntity
{
  protected final static ModelApp mApp = ModelApp.req();
  private final String mMemberKey;
  CellStatus mStatus = CellStatus.INITIALIZING;

  protected XBaseCell()
  {
    mMemberKey=getClassName()+":members:_User:"+getObjectId();
  }

  final public CellStatus getStatus()
  {
    if (mStatus == CellStatus.INITIALIZING) {
      Collection<String> memberIds =
        mApp.getMemberIds(this);
      if (memberIds == null) {
        return CellStatus.INITIALIZING;
      }
      XUser user = XUser.reqCurrentUser();
      if (getOwner().hasSameId(user)) {
        mStatus = CellStatus.OWNER;
      } else if (memberIds.contains(user.getObjectId())) {
        mStatus = CellStatus.JOINED;
      } else {
        mStatus = CellStatus.NOT_JOINED;
      }
    }
    return mStatus;
  }

  final public void setStatus(CellStatus status)
  {
    mStatus = status;
  }

  public int nameCompare(XBaseCell other)
  {
    return getName().compareTo(other.getName());
  }

  final public String getName()
  {
    return getString("name");
  }

  // if the member list is null, we do not know if we have
  // members or not.  When we load the list, it will be here,
  // even if it is empty, so we know it has been loaded.
  final public void setName(String name)
  {
    put("name", name);
  }

  public abstract boolean isPublic();


  public String getMemberKey() {
    return mMemberKey;
  }

  public Collection<String> getMemberIds() {
    return mApp.getMemberIds(this);
  };
}

