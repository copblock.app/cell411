package com.parse.controller;

import com.parse.http.ParseSyncUtils;
import com.parse.model.ParseObject;

import java.util.function.Supplier;

import javax.annotation.Nonnull;

import cell411.utils.collect.Suppliers;
import cell411.utils.collect.Suppliers.NewInst;
import cell411.utils.Util;

public class ClassTool<T extends ParseObject> {
  private final Class<T> mType;
  private final Supplier<T> mSupplier;
  private final String mName;

  public ClassTool(Class<T> type)
  {
    mType = type;
    mName = ParseSyncUtils.getClassName(type);
    mSupplier = Suppliers.supplier(type);
  };

  public T newInstance() {
    return getSupplier().get();
  }

  public Class<T> getType()
  {
    return mType;
  }

  public Supplier<T> getSupplier()
  {
    return mSupplier;
  }

  @Nonnull
  public String getName()
  {
    return mName;
  }
}
