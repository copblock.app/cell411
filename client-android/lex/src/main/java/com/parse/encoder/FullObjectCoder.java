package com.parse.encoder;

import static com.parse.ParseException.OTHER_CAUSE;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.decoder.ParseDecoder;
import com.parse.http.ParseSyncUtils;
import com.parse.model.ParseObject;
import com.parse.model.ParseUser;
import com.parse.utils.ParseDateFormat;
import com.parse.utils.ParseIOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cell411.json.JSONArray;
import cell411.json.JSONObject;
import cell411.utils.Util;
import cell411.utils.reflect.Reflect;

/**
 * Handles encoding/decoding ParseObjects to/from REST JSON.
 */
public class FullObjectCoder
  extends AbstractParseEncoder
{
  static final ParseDateFormat smFormat = ParseDateFormat.get();
  private static final String KEY_OBJECT_ID = "objectId";
  private static final String KEY_CLASS_NAME = "className";
  private static final String KEY_ACL = "ACL";
  private static final String KEY_CREATED_AT = "createdAt";
  private static final String KEY_UPDATED_AT = "updatedAt";
  private static FullObjectCoder smInstance;
  private final HashMap<String, JSONObject> mNext = new HashMap<>();
  HashMap<String, JSONObject> mText = new HashMap<>();

  public FullObjectCoder()
  {
  }

  public static FullObjectCoder get()
  {
    if (smInstance == null) {

      smInstance = new FullObjectCoder();
    }
    return smInstance;
  }

  public boolean mkdirs(File parentFile, boolean includeLast)
  {
    if (!includeLast) {
      return mkdirs(parentFile.getParentFile(), true);
    }
    if (parentFile.isDirectory()) {
      return true;
    }
    if (parentFile.exists()) {
      return false;
    }
    if (!mkdirs(parentFile.getParentFile(), true)) {
      return false;
    }
    return parentFile.mkdir();
  }

  public <X extends ParseObject> Date loadData(String text, Map<String, X> data)
  {
    try {
      int i = 0;
      JSONArray array;
      try {
        array = new JSONArray(text);
      } catch (Exception e) {
        throw Util.rethrow("parsing json", e);
      }
      Date batchDate = new Date((long) array.get(i++));
      ParseDecoder decoder = ParseDecoder.get();
      ArrayList<X> objects = new ArrayList<>();
      int objCount = array.getInt(i++);
      int changes = 0;
      Reflect.announce("changes=" + changes);
      for (int obj = 0; obj < objCount; obj++) {
        if (i >= array.length()) {
          System.out.printf("Warning: expected %d, got %d\n", objCount, obj);
          break;
        }
        String string = array.getString(i++);
        JSONObject jsonObject = new JSONObject(string);
        X parseObject = ParseSyncUtils.fromJSON(jsonObject, null, decoder);
        objects.add(parseObject);
        if (++changes % 100 != 0)
          continue;
        Reflect.announce("changes=" + changes);
      }
      for (X object : objects) {
        data.put(object.getObjectId(), object);
        if (++changes % 100 != 0)
          continue;
        Reflect.announce("changes=" + changes);
      }
      while (i < array.length()) {
        String string = array.getString(i++);
        JSONObject jsonObject = new JSONObject(string);
        ParseObject parseObject =
          ParseSyncUtils.fromJSON(jsonObject, null, decoder);
        if (++changes % 100 != 0)
          continue;
        Reflect.announce("changes=" + changes);
      }

      Reflect.announce("changes=" + changes);
      return batchDate;
    } catch (ParseException pe) {
      throw pe;
    } catch (Exception e) {
      throw new ParseException(OTHER_CAUSE, "Exception", e);
    }
  }

  public JSONObject encodeRelatedObject(ParseObject object)
  {
    String objectId = object.getObjectId();
    JSONObject pointer = new JSONObject();
    pointer.put("__type", "Pointer");
    pointer.put("className", object.getClassName());
    pointer.put("objectId", objectId);
    if (mNext.get(objectId) == null) {
      mNext.put(objectId, fullEncode(object));
    }
    return pointer;
  }

  public <X extends ParseObject> JSONObject fullEncode(final X x)
  {
    JSONObject json = encode(x);
    JSONObject copy = new JSONObject();
    String className;
    className = json.optString("__className", null);
    if (className == null) {
      className = json.optString("className", null);
    }
    if (className == null) {
      throw new NullPointerException("className not specified");
    }
    copy.put("className", className);
    copy.put("objectId", x.getObjectId());
    copy.put("createdAt", format(x.getCreatedAt()));
    copy.put("updatedAt", format(x.getUpdatedAt()));
    for (String key : json.keySet()) {
      if (key.equals("sessionToken") || key.equals("className") ||
        key.equals("__className"))
      {
        continue;
      }
      if (!copy.has(key)) {
        copy.put(key, json.get(key));
      }
    }
    return copy;
  }

  public <T extends ParseObject> JSONObject encode(T parseObject)
  {
    JSONObject jsonObject = new JSONObject();

    // Serialize the data
    ParseObject.State state = parseObject.getState();
    jsonObject.put("__type", "Object");
    jsonObject.put("__className", parseObject.getClassName());
    jsonObject.put("objectId", parseObject.getObjectId());
    try {
      Date date = parseObject.getCreatedAt();
      if(date!=null) {
        Object encoded = encodeDate(date);
        jsonObject.put("createdAt", encoded);
      }
    } catch ( Exception e ) {
      Util.printStackTrace(e);
    }
    try {
      Date date = parseObject.getUpdatedAt();
      if(date!=null) {
        Object encoded = encodeDate(date);
        jsonObject.put("updatedAt", encoded);
      }
    } catch ( Exception e ) {
      Util.printStackTrace(e);
    }

    for (String key : state.availableKeys()) {
      Object val = state.get(key);
      Object encoded = this.encode(val);
      jsonObject.put(key, encoded);
    }

    return jsonObject;
  }

  static String format(Date date)
  {
    if (date == null) {
      return null;
    } else {
      return smFormat.format(date);
    }
  }

  /**
   * Converts REST JSON response to {@link ParseObject.State.Init}.
   * <p>
   * This returns Builder instead of a State since we'll probably want to set
   * some additional
   * properties on it after decoding such as
   * {@link ParseObject.State.Init#isComplete()}, etc.
   *
   * @param builder A {@link ParseObject.State.Init} instance that will have
   *                the server JSON applied (mutated) to it. This will
   *                generally be an instance created by clearing a mutable
   *                copy of a {@link ParseObject.State} to ensure it's an
   *                instance of the correct subclass: {@code state.newBuilder
   *                ().clear()}
   * @param json    JSON response in REST format from the server.
   * @param decoder Decoder instance that will be used to decodeObject the
   *                server response.
   * @return The same Builder instance passed in after the JSON is applied.
   */
  public <T extends ParseObject.State.Init<?>> T decode(T builder,
                                                        JSONObject json,
                                                        ParseDecoder decoder)
  {
    return ParseObjectCoder.get().decode(builder, json, decoder);
  }

  public <X extends ParseObject>
  void saveData(File file, Map<String, X> data, Date batch)
  {
    mNext.clear();
    mText.clear();
    ParseUser currentUser = Parse.getCurrentUser();
    if (currentUser == null)
      return;
    for (String key : data.keySet()) {
      X x = data.get(key);
      if (x == null || x == currentUser)
        continue;
      mText.put(key, fullEncode(x));
    }
    String currentKey = currentUser.getObjectId();
    mNext.remove(currentKey);
    FileOutputStream stream = null;
    JSONArray array = new JSONArray();
    array.put(batch.getTime());
    array.put(mText.size());
    for (JSONObject json : mText.values()) {
      array.put(json);
    }
    for (JSONObject json : mNext.values()) {
      array.put(json.toString());
    }
    try {
      mkdirs(file, false);
      stream = new FileOutputStream(file);
      String arrayStr = array.toString(2);
      stream.write(arrayStr.getBytes());
    } catch (RuntimeException re) {
      throw re;
    } catch (Exception e) {
      throw new RuntimeException("Failed to write " + file, e);
    } finally {
      ParseIOUtils.closeQuietly(stream);
    }
  }
}


