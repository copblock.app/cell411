/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.encoder;

import static com.parse.model.ParseUser.State;

import com.parse.decoder.ParseDecoder;
import com.parse.model.ParseObject;
import com.parse.operation.ParseOperationSet;

import java.util.Map;

import cell411.json.JSONException;
import cell411.json.JSONObject;

/**
 * Handles encoding/decoding ParseUser to/from /2 format JSON. /2 format json
 * is only used for
 * persisting current ParseUser and ParseInstallation to disk when LDS is not
 * enabled.
 */
public class ParseUserCurrentCoder
  extends ParseObjectCurrentCoder
{

  private static final String KEY_AUTH_DATA = "auth_data";
  private static final String KEY_SESSION_TOKEN = "session_token";

  private static final ParseUserCurrentCoder INSTANCE =
    new ParseUserCurrentCoder();

  public ParseUserCurrentCoder()
  {
    // do nothing
  }

  public static ParseUserCurrentCoder get()
  {
    return INSTANCE;
  }

  /**
   * Converts a ParseUser state to /2/ JSON representation suitable for
   * saving to disk.
   * <p>
   * <pre>
   * {
   *   data: {
   *     // data fields, including objectId, createdAt, updatedAt
   *   },
   *   classname: class name for the object,
   *   operations: { } // operations per field
   * }
   * </pre>
   * <p>
   * All keys are included, regardless of whether they are dirty.
   * We also add sessionToken and authData to the json.
   *
   * @see #decode(ParseObject.State.Init, JSONObject, ParseDecoder)
   */
  @Override
  public <T extends ParseObject.State> JSONObject encode(T state,
                                                         ParseOperationSet operations,
                                                         ParseEncoder encoder)
  {

    // FYI we'll be double writing sessionToken and authData for now...
    // This is important. super.encode() has no notion of sessionToken and
    // authData, so it treats
    // them
    // like objects (simply passed to the encoder). This means that a null
    // sessionToken will become
    // JSONObject.NULL. This must be accounted in #decodeObject().
    JSONObject objectJSON = super.encode(state, operations, encoder);

    Map<String, Map<String, String>> authData = ((State) state).authData();
    if (authData.size() > 0) {
      try {
        objectJSON.put(KEY_AUTH_DATA, encoder.encode(authData));
      } catch (JSONException e) {
        throw new RuntimeException("could not attach key: auth_data");
      }
    }

    return objectJSON;
  }

}

