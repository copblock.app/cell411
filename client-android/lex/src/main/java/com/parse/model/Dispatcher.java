package com.parse.model;

import java.util.Collection;

public class Dispatcher<T extends ParseObject> implements Runnable {
  final StackTraceElement[] mStackTraceElements =
    new Exception().getStackTrace();

  final Collection<? extends ObjectEventsCallback<T>> mCallbacks;
  final ObjectEvent<T> mEvent;

  public Dispatcher(final ObjectEvent<T> event,
                    final Collection<? extends ObjectEventsCallback<T>> callbacks) {
    mCallbacks = callbacks;
    mEvent = event;
  }

  public void run() {
    for (ObjectEventsCallback<T> callback : mCallbacks) {
      callback.onEvents(mEvent);
    }
  }
}
