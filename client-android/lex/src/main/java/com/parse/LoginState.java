package com.parse;

public enum LoginState
{
  Initializing, NoUserLoggedIn, LoginInProgress, UserLoggedIn, BadSession
}
