/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.offline;

import com.parse.decoder.ParseDecoder;
import com.parse.encoder.ParseObjectCurrentCoder;
import com.parse.encoder.PointerEncoder;
import com.parse.http.ParseSyncUtils;
import com.parse.model.ParseObject;
import com.parse.model.ParseUser;
import com.parse.utils.ParseFileUtils;

import java.io.File;
import java.util.Objects;

import cell411.json.JSONObject;
import cell411.utils.Util;
import cell411.utils.collect.ObservableValueRW;
import cell411.utils.collect.ValueObserver;
import cell411.utils.except.ExceptionHandler;
import cell411.utils.io.IOUtil;
import cell411.utils.reflect.XTAG;

public class FileObjectStore<T extends ParseObject>
  implements ExceptionHandler
{
  static final XTAG TAG = new XTAG();
  private final File mFile;
  private final ParseObjectCurrentCoder mCoder;
  private final ObservableValueRW<T> mValue;
  private final Class<T> mType;
  private final String mName;

  public FileObjectStore(Class<T> type, File file,
                         ParseObjectCurrentCoder coder)
  {
    this(type, file, coder, ParseSyncUtils.getTableForClass(type));
  }

  private FileObjectStore(Class<T> type, File file,
                          ParseObjectCurrentCoder coder, String name)
  {
    mType = Objects.requireNonNull(type);
    mName = Objects.requireNonNull(name);
    mFile = Objects.requireNonNull(file);
    mCoder = Objects.requireNonNull(coder);
    mValue = new ObservableValueRW<>(mType, null);
  }


  public void save()
  {
    saveToDisk(mCoder, get(), mFile);
    load();
  }

  /**
   * Saves the {@code ParseObject} to a file on disk as JSON in /2/ format.
   *
   * @param coder   Current coder to encode the ParseObject.
   * @param current ParseObject which needs to be saved to disk.
   * @param file    The file to save the object to.
   * @see #getFromDisk(ParseObjectCurrentCoder, File, Class, ParseObject.State.Init)
   */
  private boolean saveToDisk(ParseObjectCurrentCoder coder, ParseObject current,
                             File file)
  {
    if (current == null) {
      if (mType == ParseUser.class)
        ParseSyncUtils.sessionToken = null;
      return file.delete();
    }

    JSONObject json =
      coder.encode(current.getState(), null, PointerEncoder.get());
    if (mType == ParseUser.class) {
      json.put(ParseUser.KEY_SESSION_TOKEN, ParseSyncUtils.sessionToken);
    }
    try {
      ParseFileUtils.writeJSONObjectToFile(file, json);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public T get()
  {
    return mValue.get();
  }

  public void load()
  {
    ParseObject.State.Init<?> builder = ParseObject.State.newBuilder(mName);

    mValue.set(getFromDisk(mCoder, mFile, mType, builder));
  }

  /**
   * Retrieves a {@code ParseObject} from a file on disk in /2/ format.
   *
   * @param coder   Current coder to decodeObject the ParseObject.
   * @param file    The file to retrieve the object from.
   * @param builder An empty builder which is used to generate an empty state
   *                and rebuild a
   *                ParseObject.
   * @return The {@code ParseObject} that was retrieved. If the file wasn't
   * found, or the contents
   * of the file is an invalid {@code ParseObject}, returns {@code null}.
   * @see #saveToDisk(ParseObjectCurrentCoder, ParseObject, File)
   */
  private static <T extends ParseObject> T getFromDisk(
    ParseObjectCurrentCoder coder, File file, Class<T> type,
    ParseObject.State.Init<?> builder)
  {
    JSONObject json;
    if (!file.exists()) {
      return null;
    }
    try {
      json = ParseFileUtils.readFileToJSONObject(file);
      if (type == ParseUser.class) {
        String token = json.optString("sessionToken");
        if (token != null)
          ParseSyncUtils.sessionToken = token;
      }
    } catch (Throwable e) {
      ParseFileUtils.deleteQuietly(file);
      return null;
    }
    try {
      ParseObject.State newState =
        coder.decode(builder, json, ParseDecoder.get()).isComplete(true)
          .build();
      ParseObject object = ParseObject.from(newState);
      return type.cast(object);
    } catch (Throwable t) {
      Util.throwThreadDeath(t);
      t.printStackTrace();
      IOUtil.delete(file);
      throw t;
    }
  }

  public void set(T t)
  {
    saveToDisk(mCoder, t, mFile);
    load();
    assert t != null || get() == null;
    assert t == null || t.equals(get());
  }

  public void addObserver(ValueObserver<? super T> observer)
  {
    mValue.addObserver(observer);
  }

  public void removeObserver(ValueObserver<T> observer)
  {
    mValue.removeObserver(observer);
  }

  public boolean exists()
  {
    return mFile.exists();
  }

  public void delete()
  {
    if (mFile.exists() && !ParseFileUtils.deleteQuietly(mFile) &&
      mFile.exists())
    {
      throw new RuntimeException("Unable to delete");
    }
  }

  public void setAlwaysFire(boolean value)
  {
    mValue.setAlwaysFire(value);
  }
}

