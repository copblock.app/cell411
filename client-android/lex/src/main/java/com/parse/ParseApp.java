package com.parse;

import static cell411.utils.reflect.Reflect.announce;

import android.content.Context;

import com.parse.model.ParseGeoPoint;

import cell411.config.UtilApp;
import cell411.utils.concurrent.ThreadUtil;
import cell411.utils.reflect.XTAG;

/**
 * The {@code Parse} class contains static functions that handle global
 * configuration for the Parse
 * library.
 */
public abstract class ParseApp
  extends UtilApp
{
  private final static XTAG TAG = new XTAG();
  static {
    announce( "Loading Class");
  }
  public ParseApp() {
    super();
    announce("Building instance");
  }

  public abstract void initParse();

  @Override
  protected void attachBaseContext(final Context base)
  {
    super.attachBaseContext(base);
    ThreadUtil.onExec(this::initParse,1000);
  }

  public abstract ParseGeoPoint getCurrentLocation();
}
