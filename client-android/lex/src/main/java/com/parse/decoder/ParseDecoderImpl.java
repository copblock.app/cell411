/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.decoder;

import android.util.Base64;

import com.parse.ParsePolygon;
import com.parse.encoder.AbstractParseEncoder;
import com.parse.http.ParseSyncUtils;
import com.parse.model.ParseFile;
import com.parse.model.ParseGeoPoint;
import com.parse.model.ParseObject;
import com.parse.model.ParseRelation;
import com.parse.operation.ParseFieldOperations;
import com.parse.utils.ParseDateFormat;
import com.parse.utils.ParseJSON;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import cell411.json.JSONArray;
import cell411.json.JSONException;
import cell411.json.JSONObject;
import cell411.utils.Util;

/**
 * A {@code ParseDecoder} can be used to transform JSON data structures into
 * actual objects, such as
 * {@link ParseObject}s.
 *
 * @see AbstractParseEncoder
 */
public class ParseDecoderImpl
  implements ParseDecoder
{

  // This class isn't really a Singleton, but since it has no state, it's
  // more efficient to get the
  // default instance.
  private static final ParseDecoder INSTANCE = new ParseDecoderImpl();

  protected ParseDecoderImpl()
  {
    // do nothing
  }

  /**
   * Gets the <code>ParseObject</code> another object points to. By default a
   * new
   * object will be created.
   */
  @Override
  public ParseObject decodePointer(String className, String objectId)
  {
    return ParseObject.createWithoutData(className, objectId);
  }

  @Override
  public Object decode(@Nullable Object object)
  {
    if (object == null)
      return null;

    if (object instanceof JSONArray) {
      return ParseJSON.convertJSONArrayToList((JSONArray) object);
    }

    if (object == JSONObject.NULL) {
      return null;
    }

    if (!(object instanceof JSONObject)) {
      return object;
    }

    JSONObject jsonObject = (JSONObject) object;

    String opString = jsonObject.optString("__op", null);
    if (opString != null) {
      try {
        return ParseFieldOperations.decode(jsonObject, this);
      } catch (JSONException e) {
        throw new RuntimeException(e);
      }
    }

    String typeString = jsonObject.optString("__type", null);
    if (typeString == null) {
      return ParseJSON.convertJSONObjectToMap(jsonObject);
    }

    if (typeString.equals("Date")) {
      String iso = jsonObject.optString("iso");
      return ParseDateFormat.get().parse(iso);
    }

    if (typeString.equals("Bytes")) {
      String base64 = jsonObject.optString("base64");
      return Base64.decode(base64, Base64.NO_WRAP);
    }

    if (typeString.equals("Pointer")) {
      return decodePointer(jsonObject.optString("className"),
        jsonObject.optString("objectId"));
    }

    if (typeString.equals("File")) {
      return new ParseFile(jsonObject, this);
    }

    if (typeString.equals("GeoPoint")) {
      double latitude, longitude;
      try {
        latitude = jsonObject.getDouble("latitude");
        longitude = jsonObject.getDouble("longitude");
      } catch (JSONException e) {
        throw new RuntimeException(e);
      }
      return new ParseGeoPoint(latitude, longitude);
    }

    if (typeString.equals("Polygon")) {
      List<ParseGeoPoint> coordinates = new ArrayList<>();
      try {
        JSONArray array = jsonObject.getJSONArray("coordinates");
        for (int i = 0; i < array.length(); ++i) {
          JSONArray point = array.getJSONArray(i);
          coordinates.add(
            new ParseGeoPoint(point.getDouble(0), point.getDouble(1)));
        }
      } catch (JSONException e) {
        throw new RuntimeException(e);
      }
      return new ParsePolygon(coordinates);
    }

    if (typeString.equals("Object")) {
      try {
        return ParseSyncUtils.fromJSON(jsonObject, null, this);
      } catch ( Throwable t ) {
        t.printStackTrace();
        throw Util.rethrow(t);
      }
    }

    if (typeString.equals("Relation")) {
      return new ParseRelation<>(jsonObject, this);
    }

    if (typeString.equals("OfflineObject")) {
      throw new RuntimeException(
        "An unexpected offline pointer was encountered.");
    }

    return null;
  }
}

