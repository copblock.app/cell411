package com.parse.livequery;

import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseObject;

public interface SubscriptionHandler<T extends ParseObject> {

  /**
   * Register a callback for when an event occurs.
   *
   * @param callback The callback to register.
   * @return The same SubscriptionHandling, for easy chaining.
   */
  Subscription<T> handleEvents(ObjectEventsCallback<T> callback);

  //    /**
  //     * Register a callback for when an event occurs.
  //     *
  //     * @param event    The event type to handle. You should pass one of the enum cases in Event
  //     * @param callback The callback to register.
  //     * @return The same SubscriptionHandling, for easy chaining.
  //     */
  //    SubscriptionHandler<T> handleEvent(Subscription.Event event, Subscription
  //    .HandleEventCallback<T> callback);

  /**
   * Register a callback for when an event occurs.
   *
   * @param callback The callback to register.
   * @return The same SubscriptionHandling, for easy chaining.
   */
  SubscriptionHandler<T> handleError(ErrorCallback<T> callback);

  /**
   * Register a callback for when a client succesfully subscribes to a query.
   *
   * @param callback The callback to register.
   * @return The same SubscriptionHandling, for easy chaining.
   */
  SubscriptionHandler<T> handleSubscribe(HandleSubscribeCallback<T> callback);

  /**
   * Register a callback for when a query has been unsubscribed.
   *
   * @param callback The callback to register.
   * @return The same SubscriptionHandling, for easy chaining.
   */
  SubscriptionHandler<T> handleUnsubscribe(HandleUnsubscribeCallback<T> callback);

  int getRequestId();

}
