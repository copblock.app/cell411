package com.parse.livequery;

import com.parse.ParseQuery;
import com.parse.model.ParseObject;

public interface HandleUnsubscribeCallback<T extends ParseObject> {
  void onUnsubscribe(ParseQuery<T> query);
}
