package cell411.logic;

import com.parse.model.ParseObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class ObjectCache
  extends HashMap<String, ParseObject>
{
  public ObjectCache()
  {
  }

  static protected <X extends ParseObject> ArrayList<X> getValues(
    Iterable<String> it, Class<X> ty)
  {
    return getValues(new ArrayList<>(), it, ty);
  }

  static protected <X extends ParseObject, C extends Collection<X>> C getValues(
    C co, Iterable<String> it, Class<X> ty)
  {
    for (String id : it)
      co.add(ParseObject.createWithoutData(ty, id));
    return co;
  }



  //  static public <X extends ParseObject>
  //  X requireObject(String objectId) {
  //    return Objects.requireNonNull(getObject(objectId));
  //    X x = getObject(objectId);
  //    if (x == null) {
  //      throw new NoSuchElementException("No object found for key '" +
  //      objectId + "'");
  //    }
  //    return x;
  //  }

}
